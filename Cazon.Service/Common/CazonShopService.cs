﻿using Cazon.Repository;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public class CazonShopService : CazonBaseService, ICazonShopService
    {
        CazonShopRepo _repo;

        protected CazonShopRepo Repo
        {
            get
            {
                _repo = _repo ?? new CazonShopRepo();
                return _repo;
            }
            set
            {
                _repo = value;
            }
        }

        public CazonShopService()
        {
            Repo = new CazonShopRepo();
            DisposableObjects.Add(Repo);
        }

        public override async Task<bool> IsEntityExists(object id)
        {
            return await Repo.IsEntityExists(s => s.Id == (int)id);
        }

        public async Task<bool> IsShopExists(string shopName)
        {
            return await Repo.IsShopExists(shopName);
        }

        public async Task<bool> DeleteShop(DeleteShopDTO model)
        {
            var shop = await Repo.FindById(new object[] { model.ShopId }, s => s.Products);
            
            if (shop != null)
            {
                Repo.SetEntityState(EntityState.Deleted, shop.Products);

                return (await Repo.DeleteEntity(shop)) != null;
            }

            return false;
        }

        public async Task<bool> CheckShopOwnership(int shopId, string ownerId)
        {
            return await Repo.CheckShopOwnership(shopId, ownerId);
        }
    }
}
