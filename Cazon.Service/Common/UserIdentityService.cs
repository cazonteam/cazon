﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.Common.CazonAuth;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public class UserIdentityService : CazonBaseService, IUserIdentityService
    {
        CazonUserRepo Repo;
        IOwinContext OwinContext;
        CazonUserManager _userManager;

        protected CazonUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = OwinContext.GetUserManager<CazonUserManager>();
                return _userManager;
            }
        }


        public UserIdentityService(IOwinContext owinContext)
        {
            Repo = new CazonUserRepo();
            OwinContext = owinContext;
            DisposableObjects.Add(Repo);
            DisposableObjects.Add(UserManager);
        }

        public async Task<SignInStatus> PasswordSignIn(LoginViewModel model)
        {
            var signInManager = OwinContext.Get<CazonSignInManager>();

            return await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
        }

        public void SignOut()
        {
            OwinContext.Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public Task<string> GeneratePasswordResetToken(string userId)
        {
            return UserManager.GeneratePasswordResetTokenAsync(userId);
        }

        public async Task<string> GetUserIdByEmail(string email)
        {
            var user = await UserManager.FindByEmailAsync(email);

            return user != null ? user.Id : null;
        }

        public Task<IdentityResult> ResetPassword(string userId, ResetPasswordViewModel model)
        {
            return UserManager.ResetPasswordAsync(userId, model.Code, model.Password);
        }

        public async Task<IdentityResult> Register(RegisterViewModel model)
        {
            var user = new CazonUser { FullName = model.FullName, UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, SecurityStamp = Guid.NewGuid().ToString() };
            var result = await UserManager.CreateAsync(user, model.Password);

            return result;
        }

        public string GetCurrentUserId()
        {
            return OwinContext.Get<CazonSignInManager>().AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId();
        }

        public override async Task<bool> IsEntityExists(object id)
        {
            return await Repo.IsEntityExists(u => u.Id == (string)id);
        }
    }
}
