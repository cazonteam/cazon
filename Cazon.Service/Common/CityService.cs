﻿using Cazon.Repository;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public class CityService : CazonBaseService
    {
        protected CityRepo Repo;

        public CityService()
        {
            Repo = new CityRepo();
            DisposableObjects.Add(Repo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(c => c.Id == (int)id);
        }
    }
}
