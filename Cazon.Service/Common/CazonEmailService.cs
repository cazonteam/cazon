﻿using Cazon.Service.Common.Contract;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System;

namespace Cazon.Service.Common
{
    public class CazonEmailService : CazonBaseService, IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            throw new NotImplementedException();
        }
    }

}
