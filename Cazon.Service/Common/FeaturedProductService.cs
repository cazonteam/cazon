﻿using Cazon.Repository;
using Cazon.Service.Common.Contract;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public class FeaturedProductService : CazonBaseService, IFeaturedProductService
    {
        protected FeaturedProductRepo Repo;

        public FeaturedProductService()
        {
            Repo = new FeaturedProductRepo();
            DisposableObjects.Add(Repo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(p => p.Id == (int)id);
        }

        public Task<bool> IsProductFeatured(int productId)
        {
            return Repo.IsProductFeatured(productId);
        }

        public Task<bool> IsExpiredFeaturedProduct(int productId)
        {
            return Repo.IsExpiredFeaturedProduct(productId);
        }
    }
}
