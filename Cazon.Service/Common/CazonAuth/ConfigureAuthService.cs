﻿using Cazon.Models;
using Cazon.Models.DbContexts;
using Cazon.Service.Common.Contract.CazonAuth;
using Cazon.Service.Infrastructure.Manager;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Threading.Tasks;

namespace Cazon.Service.Common.CazonAuth
{
    public class ConfigureAuthService : CazonBaseService, IConfigureAuthService
    {
        public ConfigureAuthService()
        {

        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(CazonDbContext.Create);
            app.CreatePerOwinContext<CazonUserManager>(CazonUserManager.Create);
            app.CreatePerOwinContext<CazonSignInManager>(CazonSignInManager.Create);
        }

        public void ConfigureCookieAuthentication(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/LogIn"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<CazonUserManager, CazonUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie))
                },
                CookieName = CazonIdentityManager.ApplicationCookieName
            });
        }

        public override Task<bool> IsEntityExists(object id)
        {
            throw new NotImplementedException();
        }
    }
}
