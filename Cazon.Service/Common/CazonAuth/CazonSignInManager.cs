﻿using Cazon.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cazon.Service.Common.CazonAuth
{
    // Configure the application sign-in manager which is used in this application.
    public class CazonSignInManager : SignInManager<CazonUser, string>
    {
        public CazonSignInManager(CazonUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(CazonUser user)
        {
            return user.GenerateUserIdentityAsync((CazonUserManager)UserManager, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public static CazonSignInManager Create(IdentityFactoryOptions<CazonSignInManager> options, IOwinContext context)
        {
            return new CazonSignInManager(context.GetUserManager<CazonUserManager>(), context.Authentication);
        }
    }
}
