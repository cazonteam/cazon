﻿using Cazon.Repository;
using Cazon.Service.Common.Contract.Search;
using Cazon.Service.Infrastructure;
using Cazon.ViewModel.Common.Search;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Search
{
    public class SearchService : CazonBaseService, ISearchService
    {
        CazonProductRepo ProductRepo;

        public SearchService()
        {
            ProductRepo = new CazonProductRepo();

            DisposableObjects.Add(ProductRepo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            throw new NotImplementedException();
        }

        public async Task<CazonResult<SearchProductResultDTO>> SearchProducts(SearchProductDTO model)
        {
            var products = await ProductRepo.FindProducts(model.SearchText, model);

            var result = new CazonResult<SearchProductResultDTO>
            {
                Result = new SearchProductResultDTO
                {
                    SearchedProducts = products
                    .Select(p => new GetProductForSearchDTO
                    {
                        ProductId = p.Id,
                        ProductName = p.Name,
                        ProductPrice = p.Price,
                        Discount = p.Discount.HasValue ? p.Discount.Value : 0.0,
                        DiscountEndOn = p.DiscountEndOn,
                        DefaultImageName = p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                        ProductCategoryId = p.ProductCategoryId,
                        ProductCategoryName = p.ProductCategory.CategoryName,
                        ShopId = p.CazonShopId,
                        ShopName = p.CazonShop.Name
                    })
                    .ToList()
                },
                Success = true
            };

            return result;
        }
    }
}
