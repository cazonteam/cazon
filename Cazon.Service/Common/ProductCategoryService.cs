﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.Common.Contract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cazon.ViewModel.Common;

namespace Cazon.Service.Common
{
    public class ProductCategoryService : CazonBaseService, IProductCategoryService
    {
        CategoryRepo _repo;

        protected CategoryRepo Repo
        {
            set
            {
                _repo = value;
            }

            get
            {
                _repo = _repo ?? new CategoryRepo();

                return _repo;
            }
        }

        public ProductCategoryService()
        {
            DisposableObjects.Add(_repo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(c => c.Id == (int)id);
        }

        public async Task<List<GetCategoryWithSubCategoriesDTO>> GetCategoriesWithSubCategories()
        {
            var categories = await Repo.GetCategoriesWithSubCategories();

            return PopulateGetCategoryWithSubCategoriesDTO(categories);
        }

        protected List<GetCategoryWithSubCategoriesDTO> PopulateGetCategoryWithSubCategoriesDTO(List<ProductCategory> categories)
        {
            return categories.Where(c => c.ParentCategoryId == null)
                .Select(p => new GetCategoryWithSubCategoriesDTO
                {
                    CategoryId = p.Id,
                    CategoryName = p.CategoryName,
                    IconName = p.IconName,
                    OrderNumber = p.OrderNumber,
                    SubCategories = GetSubCategories(p.Id, categories)
                })
                .ToList();
        }

        protected List<GetCategoryWithSubCategoriesDTO> GetSubCategories(int parentCategoryId, List<ProductCategory> categories)
        {
            // base case.
            if (categories == null || categories.Count == 0)
                return null;

            // if there is no category whose parentId is passed parentCategoryId, then it has no sub category then return that category.
            if (categories.Count(sc => sc.ParentCategoryId == parentCategoryId) == 0)
                return categories.Where(sc => sc.ParentCategoryId == parentCategoryId)
                    .Select(sc => new GetCategoryWithSubCategoriesDTO
                    {
                        CategoryId = sc.Id,
                        CategoryName = sc.CategoryName,
                        IconName = sc.IconName,
                        OrderNumber = sc.OrderNumber
                    })
                    .ToList();

            // category has child categories, so construct the dto with child categories.
            return categories.Where(sc => sc.ParentCategoryId == parentCategoryId)
                .Select(sc => new GetCategoryWithSubCategoriesDTO
                {
                    CategoryId = sc.Id,
                    CategoryName = sc.CategoryName,
                    IconName = sc.IconName,
                    OrderNumber = sc.OrderNumber,
                    SubCategories = GetSubCategories(sc.Id, categories).OrderBy(o => o.OrderNumber).ToList()
                })
                .ToList();
        }

        public async Task<bool> Delete(object entityId)
        {
            return await Repo.DeleteParentCategory((int)entityId);
        }
    }
}
