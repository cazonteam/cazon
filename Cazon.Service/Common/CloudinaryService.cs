﻿using System;
using System.Threading.Tasks;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System.Collections.Generic;
using System.Linq;

namespace Cazon.Service.Common
{
    public class CloudinaryService : CazonBaseService, ICloudinaryService
    {
        public string CloudinaryApiKey { set; get; }
        public string CloudinaryApiSecret { get; set; }
        public string CloudinaryCloudName { get; set; }
        public string CloudinaryUploadFolder { get; set; }

        public async Task<List<string>> UploadFiles(List<UploadFileModel> uploadFileModel)
        {
            CheckForValidKeys();
            var fileUploadStatus = new List<string>();
            var account = GetAccount();

            var cloudinary = new Cloudinary(account);

            foreach (var file in uploadFileModel)
            {
                var imageUploadParams = new ImageUploadParams
                {
                    PublicId = file.UploadedOn.ToTimeStamp(),
                    UseFilename = true,
                    File = new FileDescription(file.TempUploadPath),
                    Folder = CloudinaryUploadFolder
                };

                if (await UploadSingleFile(cloudinary, imageUploadParams))
                {
                    fileUploadStatus.Add($"Error Occurred While Uploading File {file.FileName}");
                }
                else
                {
                    fileUploadStatus.Add($"Successfully Uploading File {file.FileName}");
                }
            }

            return fileUploadStatus;
        }

        public async Task<List<string>> UploadFiles(List<CazonHttpPostedFileBase> uploadFileModel)
        {
            CheckForValidKeys();
            var fileUploadStatus = new List<string>();
            var account = GetAccount();

            var cloudinary = new Cloudinary(account);

            foreach (var file in uploadFileModel)
            {
                var imageUploadParams = new ImageUploadParams
                {
                    PublicId = file.UploadedOn.ToTimeStamp(),
                    UseFilename = true,
                    File = new FileDescription(file.UploadedOn.ToTimeStamp(), file.File.InputStream),
                    Folder = CloudinaryUploadFolder
                };

                if (await UploadSingleFile(cloudinary, imageUploadParams))
                {
                    fileUploadStatus.Add($"Error Occurred While Uploading File {file.File.FileName}");
                }
                else
                {
                    fileUploadStatus.Add($"Successfully Uploading File {file.File.FileName}");
                }
            }

            return fileUploadStatus;
        }

        public async Task<bool> DeleteFiles(List<string> fileNames)
        {
            CheckForValidKeys();
            var fileUploadStatus = new List<string>();
            var account = GetAccount();

            var cloudinary = new Cloudinary(account);

            try
            {
                var result = await cloudinary.DeleteResourcesAsync(new DelResParams
                {
                    PublicIds = fileNames.Select(file => $"{CloudinaryUploadFolder}/{file}").ToList(),
                    Invalidate = true,
                    All = true
                });

                if (result.Error != null && !string.IsNullOrEmpty(result.Error.Message))
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Cloudinary Delete Error." + result.Error.Message));
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return false;
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Task.FromResult(false);
        }

        protected virtual async Task<bool> UploadSingleFile(Cloudinary cloudinary, ImageUploadParams uploadParams)
        {
            var result = false;

            try
            {
                var imageUploadResult = await cloudinary.UploadAsync(uploadParams);

                if (imageUploadResult.Error != null && !string.IsNullOrEmpty(imageUploadResult.Error.Message))
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Cloudinary Upload Error." + imageUploadResult.Error.Message));
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = false;
            }

            return result;
        }

        protected void CheckForValidKeys()
        {
            if(string.IsNullOrEmpty(CloudinaryApiKey))
                throw new NullReferenceException("CloudinaryApiKey Can't Be Empty Or Null...");

            if (string.IsNullOrEmpty(CloudinaryApiSecret))
                throw new NullReferenceException("CloudinaryApiSecret Can't Be Empty Or Null...");

            if (string.IsNullOrEmpty(CloudinaryCloudName))
                throw new NullReferenceException("CloudinaryCloudName Can't Be Empty Or Null...");

            if (string.IsNullOrEmpty(CloudinaryUploadFolder))
                throw new NullReferenceException("CloudinaryUploadFolder Can't Be Empty Or Null...");
        }

        protected Account GetAccount()
        {
            return new Account
            {
                ApiKey = CloudinaryApiKey,
                ApiSecret = CloudinaryApiSecret,
                Cloud = CloudinaryCloudName
            };
        }
    }
}
