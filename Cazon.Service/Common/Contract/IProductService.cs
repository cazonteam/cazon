﻿using Cazon.Models.Products;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using Cazon.ViewModel.Common.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface IProductService : ICazonBaseService
    {
        Task<List<FindProductDTO>> FindProducts(FindProductModel model);

        Task<bool> CheckProductOwnerShip(string ownerId, int productId);

        Task<bool> DeleteProduct(object entityId);

        Task<Product> UpdateProduct(UpdateProductBaseDTO model);

        Task<List<KeyValuePair<int, string>>> GetPoductsForSelect(string query);
    }
}
