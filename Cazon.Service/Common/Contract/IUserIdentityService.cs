﻿using Cazon.ViewModel.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface IUserIdentityService : ICazonBaseService
    {
        Task<SignInStatus> PasswordSignIn(LoginViewModel model);

        void SignOut();

        Task<string> GeneratePasswordResetToken(string userId);

        Task<string> GetUserIdByEmail(string email);

        Task<IdentityResult> ResetPassword(string userId, ResetPasswordViewModel model);

        Task<IdentityResult> Register(RegisterViewModel model);

        string GetCurrentUserId();
    }
}
