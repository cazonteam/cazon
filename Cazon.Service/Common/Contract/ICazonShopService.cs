﻿using Cazon.ViewModel.Common;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface ICazonShopService : ICazonBaseService
    {
        Task<bool> IsShopExists(string shopName);

        Task<bool> CheckShopOwnership(int shopId, string ownerId);

        Task<bool> DeleteShop(DeleteShopDTO model);
    }
}
