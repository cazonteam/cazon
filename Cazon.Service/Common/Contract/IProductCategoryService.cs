﻿using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface IProductCategoryService : ICazonBaseService
    {
        Task<List<GetCategoryWithSubCategoriesDTO>> GetCategoriesWithSubCategories();

        Task<bool> Delete(object entityId);
    }
}
