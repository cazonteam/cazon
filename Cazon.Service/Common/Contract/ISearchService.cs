﻿using Cazon.Service.Infrastructure;
using Cazon.ViewModel.Common.Search;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract.Search
{
    public interface ISearchService : ICazonBaseService
    {
        Task<CazonResult<SearchProductResultDTO>> SearchProducts(SearchProductDTO model);
    }
}
