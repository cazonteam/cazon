﻿using System;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface ICazonBaseService : IDisposable
    {
        Task<bool> IsEntityExists(object id);
    }
}
