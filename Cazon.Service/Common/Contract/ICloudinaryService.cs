﻿using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface ICloudinaryService : ICazonBaseService
    {
        string CloudinaryApiKey { set; get; }
        string CloudinaryApiSecret { get; set; }
        string CloudinaryCloudName { get; set; }
        string CloudinaryUploadFolder { get; set; }

        Task<List<string>> UploadFiles(List<UploadFileModel> uploadFileModel);

        Task<List<string>> UploadFiles(List<CazonHttpPostedFileBase> uploadFileModel);

        Task<bool> DeleteFiles(List<string> fileNames);
    }
}