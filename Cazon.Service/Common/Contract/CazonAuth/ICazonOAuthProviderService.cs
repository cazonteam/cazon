﻿using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract.CazonAuth
{
    public interface ICazonOAuthProviderService : ICazonBaseService
    {
        string PublicClientId { set; get; }

        Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context);

        Task TokenEndpoint(OAuthTokenEndpointContext context);

        Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context);

        Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context);
    }
}
