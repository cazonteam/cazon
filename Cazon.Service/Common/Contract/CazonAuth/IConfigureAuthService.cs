﻿using Owin;

namespace Cazon.Service.Common.Contract.CazonAuth
{
    public interface IConfigureAuthService: ICazonBaseService
    {
        void ConfigureAuth(IAppBuilder app);

        void ConfigureCookieAuthentication(IAppBuilder app);
    }
}
