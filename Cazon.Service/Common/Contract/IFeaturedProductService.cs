﻿using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract
{
    public interface IFeaturedProductService : ICazonBaseService
    {
        Task<bool> IsProductFeatured(int productId);

        Task<bool> IsExpiredFeaturedProduct(int productId);
    }
}
