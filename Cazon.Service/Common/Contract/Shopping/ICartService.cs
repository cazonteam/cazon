﻿using Cazon.Service.Infrastructure;
using Cazon.ViewModel.Common.Shopping.Cart;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Contract.Shopping
{
    public interface ICartService : ICazonBaseService
    {
        Task<CazonResult<bool>> AddProductToCart(AddProductToCartDTO model);

        Task<CazonResult<bool>> UpdateCartItem(UpdateCartItemDTO model);

        Task<CazonResult<bool>> UpdateCart(UpdateCartDTO model);

        Task<CazonResult<bool>> DeleteCartItem(DeleteCartItemDTO model);

        Task<CazonResult<ViewCartDTO>> GetCurrentCartDetails();
    }
}
