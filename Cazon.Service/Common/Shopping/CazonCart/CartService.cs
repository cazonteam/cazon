﻿using Cazon.Models.Shopping.CazonCart;
using Cazon.Repository;
using Cazon.Repository.Shopping.CazonCart;
using System.Threading.Tasks;

namespace Cazon.Service.Common.Shopping.CazonCart
{
    public abstract class CartService : CazonBaseService
    {
        protected CartRepo Repo;
        protected CazonProductRepo ProductRepo;
        protected CartItemRepo CartItemRepo;

        public CartService()
        {
            Repo = new CartRepo();
            ProductRepo = new CazonProductRepo();
            CartItemRepo = new CartItemRepo();

            DisposableObjects.Add(Repo);
            DisposableObjects.Add(ProductRepo);
            DisposableObjects.Add(CartItemRepo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(f => f.CartId == (int)id);
        }
    }
}
