﻿using Cazon.Models.Products;
using Cazon.Repository;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using Cazon.ViewModel.Common.Product;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public class ProductService : CazonBaseService, IProductService
    {
        ICloudinaryService CloudinaryService;

        CazonProductRepo _repo;
        protected CazonProductRepo Repo
        {
            get
            {
                _repo = _repo ?? new CazonProductRepo();

                return _repo;
            }
            set
            {
                _repo = value;
            }
        }

        public ProductService(ICloudinaryService cloudinaryService)
        {
            Repo = new CazonProductRepo();
            CloudinaryService = cloudinaryService;
            DisposableObjects.Add(_repo);
            DisposableObjects.Add(CloudinaryService);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(p => p.Id == (int)id);
        }

        public async Task<List<FindProductDTO>> FindProducts(FindProductModel model)
        {
            var products = await Repo.FindProducts(model);

            var dto = products.Select(p => new FindProductDTO
            {
                ProductId = p.Id,
                ProductName = p.Name,
                ProductPrice = p.Price,
                ProductDescription = p.Description,
                CreatedOn = p.CreatedOn,
                ShopName = p.CazonShop.Name,
                DefaultImageName = p.Images.Count > 0 ? p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName : string.Empty,
                ProductImages = p.Images.Where(i => i.IsDefault == false).Select(i => i.ImgName).ToList(),
                ProductCategory = new ProductCategoryForProductDTO
                {
                    ProductCategoryId = p.ProductCategoryId,
                    CategoryName = p.ProductCategory.CategoryName
                }
            })
            .ToList();

            return dto;
        }

        public Task<bool> CheckProductOwnerShip(string ownerId, int productId)
        {
            return Repo.CheckProductOwnerShip(ownerId, productId);
        }

        public async Task<bool> DeleteProduct(object productId)
        {
            var product = await Repo.FindById(productId);

            if (product != null)
            {
                return await Repo.DeleteProductWithChild(product);
            }

            return false;
        }

        public async Task<Product> UpdateProduct(UpdateProductBaseDTO model)
        {
            var product = await Repo.FindById(new object[] { model.ProductId }, includePath: p => p.Images);

            if (product != null)
            {
                if (!string.IsNullOrEmpty(model.ProductName))
                    product.Name = model.ProductName;
                if (!string.IsNullOrEmpty(model.ProductDescription))
                    product.Description = model.ProductDescription;

                if (model.ShopId > 0)
                    product.CazonShopId = model.ShopId;
                if (model.ProductCategoryId > 0)
                    product.ProductCategoryId = model.ProductCategoryId;
                if (model.ProductPrice > 0)
                    product.Price = model.ProductPrice;

                if(model.Discount > 0)
                {
                    product.Discount = model.Discount;
                    product.DiscountStartFrom = model.DiscountStartFrom.HasValue ? model.DiscountStartFrom : DateTime.Now;
                    product.DiscountEndOn = model.DiscountEndOn.HasValue ? model.DiscountEndOn : null;
                }

                await CloudinaryService.DeleteFiles(product.Images
                    .Where(i => model.DeletedImages.Contains(i.ImgName))
                    .Select(i => i.ImgName)
                    .ToList());

                foreach (var image in model.DeletedImages)
                {
                    var imageToDelete = product.Images.FirstOrDefault(i => i.ImgName == image);

                    if (imageToDelete != null)
                        Repo.SetEntityState(EntityState.Deleted, imageToDelete);
                }
            }

            return product;
        }

        public Task<List<KeyValuePair<int, string>>> GetPoductsForSelect(string query)
        {
            return Repo.GetProductsForSelect(query.ToLower());
        }

        protected Product AddProduct(AddProductBaseDTO model)
        {
            var product = new Product
            {
                Name = model.ProductName,
                Description = model.ProductDescription,
                CreatedOn = DateTime.Now,
                Price = model.ProductPrice,
                QuantityInStock = model.QuantityInStock,
                ProductCategoryId = model.ProductCategoryId,
                CazonShopId = model.ShopId
            };

            return product;
        }
    }
}
