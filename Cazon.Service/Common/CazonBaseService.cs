﻿using Cazon.Service.Common.Contract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Common
{
    public abstract class CazonBaseService : ICazonBaseService
    {
        protected ICollection<IDisposable> DisposableObjects { set; get; }

        protected CazonBaseService()
        {
            DisposableObjects = new HashSet<IDisposable>();
        }

        public abstract Task<bool> IsEntityExists(object id);

        protected void LogException(Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                foreach (var obj in DisposableObjects)
                    obj?.Dispose();
        }
    }
}
