﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.API;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class CityServiceAPI : CityService, ICityServiceAPI
    {
        public async Task<GetCityDTO> FindById(object key)
        {
            var city = await Repo.FindById(key);
            GetCityDTO entitiy = null;

            if (city != null)
            {
                entitiy = new GetCityDTO
                {
                    CityId = city.Id,
                    CityFullName = city.FullName,
                    CityPostalCode = city.PostalCode,
                    CityShortName = city.ShortName
                };
            }

            return entitiy;
        }

        public async Task<GetCityDTO> GetById(object key)
        {
            return await FindById(key);
        }

        public async Task<List<GetCityDTO>> GetAll()
        {
            var citites = await Repo.GetAll();
            var entities = citites.Select(c => new GetCityDTO
            {
                CityId = c.Id,
                CityFullName = c.FullName,
                CityPostalCode = c.PostalCode,
                CityShortName = c.ShortName
            })
            .ToList();

            return entities;
        }

        public async Task<GetCityDTO> Add(AddCityDTO model)
        {
            var entity = new City { FullName = model.CityFullName, ShortName = model.CityShortName, PostalCode = model.CityPostalCode };
            var updatedEntity = await Repo.AddEntity(entity);

            GetCityDTO dto = null;
            if (updatedEntity != null)
            {
                dto = new GetCityDTO
                {
                    CityId = updatedEntity.Id,
                    CityFullName = updatedEntity.FullName,
                    CityShortName = updatedEntity.ShortName,
                    CityPostalCode = updatedEntity.PostalCode
                };
            }

            return dto;
        }

        public async Task<bool> Update(UpdateCityDTO model)
        {
            var entity = await Repo.FindById(model.CityId);

            if (entity != null)
            {
                if (!string.IsNullOrEmpty(model.CityFullName))
                    entity.FullName = model.CityFullName;
                if (!string.IsNullOrEmpty(model.CityShortName))
                    entity.ShortName = model.CityShortName;
                if (model.CityPostalCode > 0)
                    entity.PostalCode = model.CityPostalCode;

                return (await Repo.UpdateEntity(entity) != null);
            }

            return false;
        }

        public async Task<bool> Delete(object entityId)
        {
            var entity = await Repo.FindById(entityId);
            var result = await Repo.DeleteEntity(entity);

            return (result != null);
        }
    }
}
