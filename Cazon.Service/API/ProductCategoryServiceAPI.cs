﻿using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class ProductCategoryServiceAPI : ProductCategoryService, IProductCategoryServiceAPI
    {
        public async Task<List<GetCategoryWithSubCategoriesDTO>> GetAll()
        {
            var entities = await Repo.GetAll();

            return PopulateGetCategoryWithSubCategoriesDTO(entities);
        }

        public async Task<List<GetCategoryWithSubCategoriesDTO>> GetSubCategories(int parentCategoryId)
        {
            var subCategories = await Repo.GetSubCategories(parentCategoryId);

            return base.GetSubCategories(parentCategoryId, subCategories);
        }

        public async Task<List<GetCategoryWithSubCategoriesDTO>> GetCategoriesByName(string categoryName)
        {
            var subCategories = await Repo.GetCategoriesByName(categoryName);

            return PopulateGetCategoryWithSubCategoriesDTO(subCategories);
        }
    }
}