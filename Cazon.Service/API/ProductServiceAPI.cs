﻿using Cazon.Models;
using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using Cazon.ViewModel.Common.Product;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class ProductServiceAPI : ProductService, IProductServiceAPI
    {
        public ProductServiceAPI(ICloudinaryService cloudinaryService): base(cloudinaryService)
        {
        }

        public async Task<List<GetShopProductsDTO>> GetProductsByShop(int shopId)
        {
            var products = await Repo.GetProductsByShop(shopId);

            var dto = products.Select(p => new GetShopProductsDTO
            {
                ProductId = p.Id,
                ProductName = p.Name,
                ProductPrice = p.Price,
                CreatedOn = p.CreatedOn,
                ProductDescription = p.Description,
                QuantityInStock = p.QuantityInStock,
                ProductCategory = new ProductCategoryForProductDTO
                {
                    CategoryName = p.ProductCategory.CategoryName,
                    ProductCategoryId = p.ProductCategoryId
                },
                CategoryParentCategories = new ProductCategoryForProductDTO[2]
                    {
                        p.ProductCategory.ParentCategory == null ? null : new ProductCategoryForProductDTO
                        {
                            ProductCategoryId = p.ProductCategory.ParentCategoryId.Value,
                            CategoryName = p.ProductCategory.ParentCategory.CategoryName
                        },
                        p.ProductCategory.ParentCategory == null || p.ProductCategory.ParentCategory.ParentCategory == null ? null : new ProductCategoryForProductDTO
                        {
                            ProductCategoryId = p.ProductCategory.ParentCategory.ParentCategoryId.Value,
                            CategoryName = p.ProductCategory.ParentCategory.ParentCategory.CategoryName
                        }
                    },
                DefaultImageName = p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName
            })
            .ToList();

            return dto;
        }

        public async Task<GetProductDTO> GetProductsById(int productId)
        {
            var product = await Repo.FindById(productId);

            var dto = (product == null) ? null : new GetProductDTO
            {
                ProductId = product.Id,
                ProductName = product.Name,
                ProductPrice = product.Price,
                CreatedOn = product.CreatedOn,
                ProductDescription = product.Description,
                DefaultImageName = (product.Images == null || product.Images.Count == 0) ? null : product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                ProductCategory = new ProductCategoryForProductDTO
                {
                    CategoryName = product.ProductCategory.CategoryName,
                    ProductCategoryId = product.ProductCategoryId
                },
                Shop = new CazonShopForProductDTO
                {
                    ShopId = product.CazonShopId,
                    ShopName = product.CazonShop.Name,
                    Description = product.CazonShop.Description,
                    PhoneNumber = product.CazonShop.Owner.PhoneNumber,
                    RegisteredOn = product.CazonShop.RegisteredOn,
                    Address = new AddressForProductDTO
                    {
                        Latitude = product.CazonShop.Address.Latitude,
                        Longitude = product.CazonShop.Address.Longitude,
                        CityFullName = product.CazonShop.Address.City.FullName,
                        CityShortName = product.CazonShop.Address.City.ShortName,
                        CityPostalCode = product.CazonShop.Address.City.PostalCode,
                    }
                },
                ProductImages = product.Images.Select(i => i.ImgName).ToList()
            };

            return dto;
        }

        public async Task<List<GetProductsByCategoryIdDTO>> GetProductsByCategoryId(int categoryId)
        {
            var products = await Repo.GetProductsByCategoryId(categoryId);

            var dto = products.Select(p => new GetProductsByCategoryIdDTO
            {
                ProductId = p.Id,
                ProductName = p.Name,
                ProductPrice = p.Price,
                CreatedOn = p.CreatedOn,
                ProductDescription = p.Description,
                ShopId = p.CazonShop.Id,
                ShopName = p.CazonShop.Name,
                DefaultImageName = (p.Images == null || p.Images.Count == 0) ? null : p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                ProductCategory = new ProductCategoryForProductDTO
                {
                    CategoryName = p.ProductCategory.CategoryName,
                    ProductCategoryId = p.ProductCategoryId
                },
            })
            .ToList();

            return dto;
        }

        public async Task<List<GetProductsByCategoryIdDTO>> GetProductsByCategoryName(string categoryName)
        {
            var products = await Repo.GetProductsByCategoryName(categoryName);

            var dto = products.Select(p => new GetProductsByCategoryIdDTO
            {
                ProductId = p.Id,
                ProductName = p.Name,
                ProductPrice = p.Price,
                CreatedOn = p.CreatedOn,
                ProductDescription = p.Description,
                ShopId = p.CazonShop.Id,
                ShopName = p.CazonShop.Name,
                DefaultImageName = (p.Images == null || p.Images.Count == 0) ? null : p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                ProductCategory = new ProductCategoryForProductDTO
                {
                    CategoryName = p.ProductCategory.CategoryName,
                    ProductCategoryId = p.ProductCategoryId
                },
            }).ToList();

            return dto;
        }

        public async Task<GetProductImageForProductDTO> GetProductImages(int productId)
        {
            var product = await Repo.FindById(productId);

            var dto = new GetProductImageForProductDTO();
            if (product != null)
            {
                dto.ProductImages.AddRange(product.Images.Select(i => i.ImgName).ToList());
                dto.DefaultImageName = product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName;
            }

            return dto;
        }

        public async Task<int> AddProduct(AddProductApiDTO model)
        {
            var product = base.AddProduct(model);

            product.Images = model.UploadedFiles.Select(i => new ProductImage
            {
                ImgName = i.UploadedOn.ToTimeStamp(),
                IsDefault = i.FileName == model.DefaultImageName
            }).ToList();

            if (!product.Images.Any(i => i.IsDefault == true))
            {
                product.Images.ElementAt(0).IsDefault = true;
            }

            await Repo.AddEntity(product);

            return product.Id;
        }


        public async Task<bool> UpdateProduct(UpdateProductAPIDTO model)
        {
            var product = await base.UpdateProduct(model);
            if (product != null)
            {
                foreach (var image in model.UploadedFiles)
                {
                    var imageToAdd = new ProductImage
                    {
                        ImgName = image.UploadedOn.ToTimeStamp(),
                        IsDefault = false,
                        ProductId = product.Id
                    };

                    Repo.SetEntityState(EntityState.Added, imageToAdd);
                }

                if (!string.IsNullOrEmpty(model.DefaultImageName))
                {
                    var defaultFile = model.UploadedFiles.FirstOrDefault(f => f.FileName == model.DefaultImageName);
                    var imageName = defaultFile != null ? defaultFile.UploadedOn.ToTimeStamp() : model.DefaultImageName;

                    foreach (var image in product.Images)
                    {
                        if (!model.DeletedImages.Contains(image.ImgName))
                        {
                            image.IsDefault = !string.IsNullOrEmpty(imageName) && (image.ImgName == imageName);
                        }
                    }
                }

                if (!product.Images.Any(i => i.IsDefault == true))
                {
                    product.Images.ElementAt(0).IsDefault = true;
                }

                return (await Repo.UpdateEntity(product) != null);
            }

            return false;
        }
    }
}
