﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.API;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class ProductImageServiceAPI : CazonBaseService, IProductImageServiceAPI
    {
        ProductImageRepo Repo;

        public ProductImageServiceAPI()
        {
            Repo = new ProductImageRepo();
            DisposableObjects.Add(Repo);
        }
        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(i => i.Id == (int)id);
        }

        public async Task<List<GetProductImageDTO>> GetAll()
        {
            var entities = await Repo.GetAll();
            var dto = entities.Select(e => new GetProductImageDTO
            {
                ImageId = e.Id,
                ProductId = e.ProductId,
                IsDefault = e.IsDefault,
                ImageName = e.ImgName
            })
            .ToList();

            return dto;
        }

        public async Task<GetProductImageDTO> FindById(object key)
        {
            var entity = await Repo.FindById(key);
            GetProductImageDTO dto = null;

            if (entity != null)
            {
                dto = new GetProductImageDTO
                {
                    ImageId = entity.Id,
                    ProductId = entity.ProductId,
                    ImageName = entity.ImgName,
                    IsDefault = entity.IsDefault
                };
            }

            return dto;
        }


        public async Task<GetProductImageDTO> GetById(object key)
        {
            return await FindById(key);
        }

        public async Task<GetProductImageDTO> Add(AddProductImageDTO model)
        {
            var entity = new ProductImage { ImgName = model.ImageName, IsDefault = model.IsDefault, ProductId = model.ProductId };
            GetProductImageDTO dto = null;

            if (await Repo.AddEntity(entity) != null)
            {
                dto = new GetProductImageDTO
                {
                    ImageId = entity.Id
                };
            }

            return dto;
        }

        public async Task<bool> Delete(object entityId)
        {
            var entity = await Repo.FindById(entityId);
            var isDeleted = false;

            if (entity != null)
            {
                await Repo.DeleteEntity(entity);

                isDeleted = true;
            }

            return isDeleted;
        }

        public async Task<bool> Update(EditProductImageDTO model)
        {
            var entity = await Repo.FindById(model.ImageId);
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(model.ImageName))
                    entity.ImgName = model.ImageName;
                if (model.IsDefault.HasValue)
                    entity.IsDefault = model.IsDefault.Value;
                if (model.ProductId > 0)
                    entity.ProductId = model.ProductId;

                return (await Repo.UpdateEntity(entity) != null);
            }


            return false;
        }
    }
}