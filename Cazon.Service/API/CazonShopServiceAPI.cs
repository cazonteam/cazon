﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class CazonShopServiceAPI : CazonShopService, ICazonShopServiceAPI
    {
        public CazonShopServiceAPI()
        {
            Repo = new CazonShopRepo();
            DisposableObjects.Add(Repo);
        }

        public async Task<GetShopDTO> GetShopById(int shopId)
        {
            var shop = await Repo.FindById(shopId);

            var dto = GetShopDTOFromShop(shop);

            return dto;
        }

        public async Task<GetShopDTO> GetShopByIdAndOwner(int shopId, string ownerId)
        {
            var shop = await Repo.GetShopByIdAndOwner(shopId, ownerId);
            var dto = GetShopDTOFromShop(shop);

            return dto;
        }

        public async Task<List<GetShopDTO>> GetAllShopsByOwner(string ownerId)
        {
            var shops = await Repo.GetAllShopsByOwner(ownerId);

            var dto = shops.Select(s => GetShopDTOFromShop(s)).ToList();

            return dto;
        }

        public async Task<GetShopDTO> AddShop(AddShopApiDTO model)
        {
            var shop = new CazonShop
            {
                Name = model.ShopName,
                Description = model.ShopDescription,
                ShopPhoneNumber = model.ShopPhoneNumber,
                RegisteredOn = DateTime.Now,
                Address = new Address
                {
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    CityId = model.CityId
                },
                OwnerId = model.OwnerId
            };

            if (await Repo.AddEntity(shop) != null)
            {
                return GetShopDTOFromShop(shop);
            }

            return null;
        }

        public async Task<bool> EditShop(EditShopApiDTO model)
        {
            var shop = await Repo.FindById(model.ShopId);
            if (shop != null)
            {
                if (!string.IsNullOrEmpty(model.ShopName)) shop.Name = model.ShopName;
                if (!string.IsNullOrEmpty(model.ShopDescription)) shop.Description = model.ShopDescription;
                if (!string.IsNullOrEmpty(model.ShopPhoneNumber)) shop.ShopPhoneNumber = model.ShopPhoneNumber;
                if (!string.IsNullOrEmpty(model.Latitude)) shop.Address.Latitude = model.Latitude;
                if (!string.IsNullOrEmpty(model.Longitude)) shop.Address.Longitude = model.Longitude;
                if (model.CityId > 0) shop.Address.CityId = model.CityId;

                return (await Repo.UpdateEntity(shop) != null);
            }

            return false;
        }

        private static GetShopDTO GetShopDTOFromShop(CazonShop shop)
        {
            var dto = (shop == null) ? null : new GetShopDTO
            {
                ShopId = shop.Id,
                ShopDescription = shop.Description,
                RegisteredOn = shop.RegisteredOn.ToString(),
                ShopName = shop.Name
            };

            if (shop != null && shop.Owner != null)
            {
                if (shop.Owner != null)
                {
                    dto.ShopPhoneNumber = shop.ShopPhoneNumber;
                }

                if (shop.Address != null)
                {
                    dto.Latitude = shop.Address.Latitude;
                    dto.Longitude = shop.Address.Longitude;
                }

                if (shop.Address.City != null)
                {
                    dto.CityName = shop.Address.City.FullName;
                }
            }

            return dto;
        }
    }
}