﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface ICityServiceAPI : ICityService
    {
        // Find and get the entitiy along with all it's child elements.
        Task<GetCityDTO> GetById(object key);

        // Find and get the entitiy without fetching it's child elements.
        Task<GetCityDTO> FindById(object key);

        Task<GetCityDTO> Add(AddCityDTO entity);

        Task<bool> Update(UpdateCityDTO entity);

        Task<bool> Delete(object entityId);

        Task<List<GetCityDTO>> GetAll();
    }
}
