﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface ICazonUserServiceAPI : ICazonBaseService
    {
        Task<string> RegisterUser(RegisterViewModel model);

        Task<CazonUserBasicInfoDTO> GetBasicInfo(object userId);
    }
}