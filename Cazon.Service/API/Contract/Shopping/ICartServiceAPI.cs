﻿using Cazon.Service.Common.Contract.Shopping;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract.Shopping
{
    public interface ICartServiceAPI : ICartService
    {
        Task<int> GetCurrentCartId();
    }
}
