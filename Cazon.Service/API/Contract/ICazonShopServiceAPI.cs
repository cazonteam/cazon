﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface ICazonShopServiceAPI : ICazonShopService
    {
        Task<GetShopDTO> GetShopById(int shopId);

        Task<GetShopDTO> GetShopByIdAndOwner(int shopId, string ownerId);

        Task<List<GetShopDTO>> GetAllShopsByOwner(string ownerId);

        Task<GetShopDTO> AddShop(AddShopApiDTO model);

        Task<bool> EditShop(EditShopApiDTO model);
    }
}