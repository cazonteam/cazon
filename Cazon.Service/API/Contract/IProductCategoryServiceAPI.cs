﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface IProductCategoryServiceAPI : IProductCategoryService
    {
        Task<List<GetCategoryWithSubCategoriesDTO>> GetAll();

        Task<List<GetCategoryWithSubCategoriesDTO>> GetSubCategories(int parentCategoryId);

        Task<List<GetCategoryWithSubCategoriesDTO>> GetCategoriesByName(string categoryName);
    }
}