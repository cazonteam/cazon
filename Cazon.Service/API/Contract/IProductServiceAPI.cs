﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface IProductServiceAPI : IProductService
    {
        Task<List<GetShopProductsDTO>> GetProductsByShop(int shopId);

        Task<GetProductDTO> GetProductsById(int productId);

        Task<List<GetProductsByCategoryIdDTO>> GetProductsByCategoryId(int categoryId);

        Task<List<GetProductsByCategoryIdDTO>> GetProductsByCategoryName(string categoryName);

        Task<GetProductImageForProductDTO> GetProductImages(int productId);

        Task<int> AddProduct(AddProductApiDTO model);

        Task<bool> UpdateProduct(UpdateProductAPIDTO model);        
    }
}
