﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.API.Contract
{
    public interface IProductImageServiceAPI : ICazonBaseService
    {
        // Find and get the entity along with all it's child elements.
        Task<GetProductImageDTO> GetById(object key);

        // Find and get the entity without fetching it's child elements.
        Task<GetProductImageDTO> FindById(object key);

        Task<List<GetProductImageDTO>> GetAll();

        Task<GetProductImageDTO> Add(AddProductImageDTO model);

        Task<bool> Update(EditProductImageDTO entity);

        Task<bool> Delete(object entityId);        
    }
}