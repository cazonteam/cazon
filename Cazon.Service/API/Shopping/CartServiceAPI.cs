﻿using Cazon.Models.Shopping.CazonCart;
using Cazon.Service.API.Contract.Shopping;
using Cazon.Service.Common.Shopping.CazonCart;
using Cazon.Service.Infrastructure;
using Cazon.Service.Infrastructure.Manager;
using Cazon.ViewModel.Common.Shopping.Cart;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.API.Shopping
{
    public class CartServiceAPI : CartService, ICartServiceAPI
    {
        public async Task<CazonResult<ViewCartDTO>> GetCurrentCartDetails()
        {
            var result = new CazonResult<ViewCartDTO>()
            {
                Result = new ViewCartDTO()
            };

            var cart = await Repo.GetCartByUserId(CazonIdentityManager.CurrentUserId);

            if (cart != null)
            {
                result.Result.CartId = cart.CartId;
                result.Result.CartItems = cart.CartItems.Select(c => new ViewCartItemDTO
                {
                    CartItemId = c.CartItemId,
                    ProductId = c.ProductId,
                    AddedOn = c.AddedOn,
                    ProductName = c.Product.Name,
                    ProductPrice = c.Product.Price,
                    Quantity = c.Quantity,
                    CartItemPrice = (c.Quantity * c.Product.Price),
                    Description = c.Product.Description,
                    QuantityInStock = c.Product.QuantityInStock,
                    ProductImageName = c.Product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                    CazonShopId = c.Product.CazonShopId,
                    CazonShopName = c.Product.CazonShop.Name,
                    ProductCategoryId = c.Product.ProductCategoryId,
                    ProductCategoryName = c.Product.ProductCategory.CategoryName
                })
                .ToList();

                result.Success = true;
            }
            else
            {
                result.Errors.Add("There is no cart for the user.");
            }

            return result;
        }

        public async Task<CazonResult<bool>> AddProductToCart(AddProductToCartDTO model)
        {
            var result = new CazonResult<bool>();
            if (!await ProductRepo.IsEntityExists(model.ProductId))
            {
                result.Errors.Add("This product is not exists at our site.");
                result.Result = false;
            }
            else
            {
                var currentCart = await GetCurrentCart();
                if (!await CartItemRepo.IsProductInTheCart(currentCart.CartId, model.ProductId))
                {
                    currentCart.CartItems.Add(new CartItem
                    {
                        ProductId = model.ProductId,
                        Quantity = 1
                    });

                    await Repo.UpdateEntity(currentCart);
                    result.Success = true;
                    result.Result = true;
                }
                else
                {
                    result.Errors.Add("This product is already in the cart.");
                    result.Result = false;
                }
            }
            return result;
        }

        public async Task<CazonResult<bool>> UpdateCartItem(UpdateCartItemDTO model)
        {
            var result = new CazonResult<bool>();
            var currentCartId = await GetCurrentCartId();
            var cartItem = await CartItemRepo.GetCartItemByCartId(currentCartId, model.CartItemId);
            if (cartItem != null)
            {
                cartItem.Quantity = model.Quantity;
                await CartItemRepo.UpdateEntity(cartItem);
                result.Success = true;
                result.Result = true;
            }
            else
            {
                result.Errors.Add("This product is not in the cart.");
                result.Result = false;
            }

            return result;
        }

        public async Task<CazonResult<bool>> UpdateCart(UpdateCartDTO model)
        {
            var result = new CazonResult<bool>();

            var currentCartId = await GetCurrentCartId();
            var cartItems = await CartItemRepo.GetAllCartItemsByCartId(currentCartId);

            foreach (var item in model.CartItems)
            {
                var updatedItem = cartItems.FirstOrDefault(i => i.CartItemId == item.CartItemId);
                if (updatedItem != null)
                    updatedItem.Quantity = item.Quantity;
            }

            if (await CartItemRepo.UpdateEntities(cartItems) != null)
            {
                result.Success = true;
                result.Result = true;
            }
            else
            {
                result.Success = false;
                result.Result = false;
            }

            return result;
        }

        public async Task<CazonResult<bool>> DeleteCartItem(DeleteCartItemDTO model)
        {
            var result = new CazonResult<bool>();
            var currentCartId = await GetCurrentCartId();
            var cartItem = await CartItemRepo.GetCartItemByCartId(currentCartId, model.CartItemId);
            if (cartItem != null)
            {
                await CartItemRepo.DeleteEntity(cartItem);
                result.Success = true;
                result.Result = true;
            }
            else
            {
                result.Errors.Add("This item is not in your cart.");
                result.Result = false;
            }

            return result;
        }

        public async Task<int> GetCurrentCartId()
        {
            var currentCart = await Repo.GetCartByUserId(CazonIdentityManager.CurrentUserId);

            return (currentCart == null) ? -1 : currentCart.CartId;
        }

        protected async Task<Cart> GetCurrentCart()
        {
            var currentCart = await Repo.GetCartByUserId(CazonIdentityManager.CurrentUserId);

            if (currentCart == null)
            {
                currentCart = new Cart
                {
                    SessionId = string.Empty,
                    UserId = CazonIdentityManager.CurrentUserId
                };

                await Repo.AddEntity(currentCart);
            }

            return currentCart;
        }
    }
}
