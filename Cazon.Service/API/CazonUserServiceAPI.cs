﻿using Cazon.Models;
using Cazon.Repository;
using Cazon.Service.API.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace Cazon.Service.API
{
    public class CazonUserServiceAPI : CazonBaseService, ICazonUserServiceAPI
    {
        CazonUserRepo Repo;

        public CazonUserServiceAPI()
        {
            Repo = new CazonUserRepo();
            DisposableObjects.Add(Repo);
        }

        public override Task<bool> IsEntityExists(object id)
        {
            return Repo.IsEntityExists(c => c.Id == (string)id);
        }

        public async Task<string> RegisterUser(RegisterViewModel model)
        {
            var user = new CazonUser
            {
                FullName = model.FullName,
                Email = model.Email,
                UserName = model.Email,
                PhoneNumber = model.PhoneNumber,
                PasswordHash = new PasswordHasher().HashPassword(model.Password),
                SecurityStamp = Guid.NewGuid().ToString()
            };

            if (await Repo.AddEntity(user) != null)
            {
                return user.Id;
            }

            return null;
        }

        public async Task<CazonUserBasicInfoDTO> GetBasicInfo(object userId)
        {
            var user = await Repo.FindById(userId);

            var dto = (user == null) ? null : new CazonUserBasicInfoDTO
            {
                UserId = user.Id,
                FullName = user.FullName,
                UserName = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            return dto;
        }
    }
}
