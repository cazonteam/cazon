﻿using System.Collections.Generic;

namespace Cazon.Service.Infrastructure
{
    public class CazonResult<TResult>
    {
        public CazonResult()
        {
            Errors = new List<string>();
        }

        public bool Success { get; set; } = false;

        public TResult Result { get; set; }

        public List<string> Errors { get; set; }
    }
}
