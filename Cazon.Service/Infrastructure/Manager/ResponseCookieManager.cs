﻿using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{

    public class ResponseCookieManager : CookieManagerBase
    {
        static ResponseCookieManager()
        {
            CookieCollecttion = HttpContext.Current.Response.Cookies;
        }
    }
}