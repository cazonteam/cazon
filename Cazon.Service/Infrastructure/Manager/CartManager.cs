﻿using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{
    public abstract class CartManagerBase
    {

    }

    public abstract class WebCartManager
    {
        public static string SessionCartIdKey { get; } = "CurrentCartId";
        public static string SessionCartItemCountKey { get; } = "NoOfCartItems";

        public static int CurrentCartId
        {
            get
            {
                var cartId = HttpContext.Current.Session[SessionCartIdKey];

                return cartId != null ? int.Parse(cartId.ToString()) : -1;
            }
        }

        public static int NoOfCartItems
        {
            get
            {
                var cartItemCount = HttpContext.Current.Session[SessionCartItemCountKey];

                return cartItemCount != null ? int.Parse(cartItemCount.ToString()) : 0;
            }
        }

        public static bool ClearCurrentCart()
        {
            SessionManager.DeleteKey(SessionCartIdKey);
            SessionManager.DeleteKey(SessionCartItemCountKey);
            SessionManager.DeleteKey(SessionManager.CazonSessionKey);

            return true;
        }
    }
}
