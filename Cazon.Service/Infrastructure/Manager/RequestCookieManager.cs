﻿using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{

    public class RequestCookieManager : CookieManagerBase
    {
        static RequestCookieManager()
        {
            CookieCollecttion = HttpContext.Current.Request.Cookies;
        }
    }
}