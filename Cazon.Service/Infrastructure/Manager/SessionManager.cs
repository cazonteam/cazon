﻿using System;
using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{
    public static class SessionManager
    {
        public static string CazonSessionKey { get; } = "CazonSessionId";

        public static bool AddOrUpdateValue<T>(string key, T value)
        {
            HttpContext.Current.Session[key] = value;

            return true;
        }

        public static object GetValue(string key)
        {
            return HttpContext.Current.Session[key];
        }

        public static void DeleteKey(string key)
        {
            if(HttpContext.Current.Session[key] != null)
            {
                HttpContext.Current.Session[key] = null;
            }
        }

        public static bool AbandonSession()
        {
            try
            {
                ResponseCookieManager.DeleteCookie(CazonSessionKey);
                ResponseCookieManager.DeleteCookie(CazonIdentityManager.ApplicationCookieName);

                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();

                return true;
            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return false;
        }

        public static bool IsKeyExists(string key)
        {
            return HttpContext.Current.Session[key] != null;
        }
    }
}
