﻿using Microsoft.AspNet.Identity;
using System.Security.Principal;
using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{
    public static class CazonIdentityManager
    {
        public static IPrincipal CurrentUser
        {
            get
            {
                return HttpContext.Current.User;
            }
        } 

        public static string CurrentUserName
        {
            get
            {
                return CurrentUser.Identity.GetUserName();
            }
        }

        public static string CurrentUserId
        {
            get
            {
                return CurrentUser.Identity.GetUserId();
            }
        }

        public static string CurrentSessionId
        {
            get
            {
                return HttpContext.Current.Session.SessionID;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                return CurrentUser.Identity.IsAuthenticated;
            }
        }

        public static string ApplicationCookieName { get; } = "CazonAuthCookie";
    }
}