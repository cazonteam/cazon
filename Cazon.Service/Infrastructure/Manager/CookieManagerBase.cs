﻿using System;
using System.Web;

namespace Cazon.Service.Infrastructure.Manager
{

    public abstract class CookieManagerBase
    {
        protected static HttpCookieCollection CookieCollecttion;

        public static void AddOrUpdateCookie(string name, string value, DateTime expiryDate)
        {
            var cookie = CookieCollecttion[name];
            if (cookie == null)
            {
                cookie = new HttpCookie(name)
                {
                    Value = value,
                    Expires = expiryDate,
                };
                CookieCollecttion.Add(cookie);
            }
            else
            {
                cookie.Value = value;
                cookie.Expires = expiryDate;
                CookieCollecttion.Set(cookie);
            }
        }

        public static void DeleteCookie(string name)
        {
            var cookie = CookieCollecttion[name];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-3);
                CookieCollecttion.Set(cookie);
            }
        }
    }
}