﻿namespace System
{
    public static class DateTimeExtensions
    {
        public static string ToTimeStamp(this DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMddHHmmssffff");
        }
    }
}