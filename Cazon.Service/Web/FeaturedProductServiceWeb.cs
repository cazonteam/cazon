﻿using Cazon.Models.Products;
using Cazon.Service.Common;
using Cazon.Service.Web.Contract;
using Cazon.ViewModel.Web.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Web
{
    public class FeaturedProductServiceWeb: FeaturedProductService, IFeaturedProductServiceWeb
    {
        public async Task<List<GetFeaturedProductsByCategoryDTO>> GetFeaturedProducts(string[] categoryNames)
        {
            var featuredProducts = await Repo.GetFeaturedProducts(categoryNames);

            return ConstructFeaturedList(featuredProducts);
        }

        public async Task<List<GetFeaturedProductsByCategoryDTO>> GetFeaturedProducts(int[] categoryIds)
        {
            var featuredProducts = await Repo.GetFeaturedProducts(categoryIds);

            return ConstructFeaturedList(featuredProducts);
        }

        protected List<GetFeaturedProductsByCategoryDTO> ConstructFeaturedList<T>(List<IGrouping<T, FeaturedProduct>> featuredProducts)
        {
            var result = new List<GetFeaturedProductsByCategoryDTO>();
            foreach (var featuredProduct in featuredProducts)
            {
                var catrgoryFeatureProduct = new GetFeaturedProductsByCategoryDTO();

                if (featuredProducts.Count > 0)
                {
                    catrgoryFeatureProduct.CategoryId = featuredProduct.ElementAt(0).Product.ProductCategoryId;
                    catrgoryFeatureProduct.CategoryName = featuredProduct.ElementAt(0).Product.ProductCategory.CategoryName;
                    catrgoryFeatureProduct.CategoryIconName = featuredProduct.ElementAt(0).Product.ProductCategory.IconName;
                    catrgoryFeatureProduct.FeaturedProducts = featuredProduct.Select(p => new GetFeaturedProductsDTO
                    {
                        FeatureProductId = p.Id,
                        ProductId = p.ProductId,
                        ProductName = p.Product.Name,
                        Description = p.Product.Description,
                        ProductPrice = p.Product.Price,
                        DefaultImageName = p.Product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                        Discount = ((p.Product.DiscountStartFrom != null && p.Product.DiscountStartFrom <= DateTime.Now) && (p.Product.DiscountEndOn == null || p.Product.DiscountEndOn >= DateTime.Now)) ? p.Product.Discount.Value : 0.0
                    })
                    .ToList();
                }
                result.Add(catrgoryFeatureProduct);
            }

            return result;
        }
    }
}
