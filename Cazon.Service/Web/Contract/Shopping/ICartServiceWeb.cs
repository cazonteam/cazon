﻿using Cazon.Service.Common.Contract.Shopping;
using System.Threading.Tasks;

namespace Cazon.Service.Web.Contract.Shopping
{
    public interface ICartServiceWeb : ICartService
    {
        Task<bool> MigrateCurrentCart(string userId);
    }
}
