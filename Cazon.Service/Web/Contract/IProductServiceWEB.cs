﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common.Product;
using Cazon.ViewModel.Web.Products;
using System.Threading.Tasks;

namespace Cazon.Service.Web.Contract
{
    public interface IProductServiceWEB : ICazonBaseService
    {
        Task<ViewCategoryProductsDTO> GetCategoyProducts(string categoryName, IProductSearchFilters filters);

        Task<ViewProductDetailsDTO> GetProductDetailsByNameAndId(string productName, int productId);
    }
}
