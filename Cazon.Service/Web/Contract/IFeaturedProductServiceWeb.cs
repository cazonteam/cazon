﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Web.Products;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Web.Contract
{
    public interface IFeaturedProductServiceWeb : ICazonBaseService
    {
        Task<List<GetFeaturedProductsByCategoryDTO>> GetFeaturedProducts(string[] categoryNames);

        Task<List<GetFeaturedProductsByCategoryDTO>> GetFeaturedProducts(int[] categoryIds);
    }
}
