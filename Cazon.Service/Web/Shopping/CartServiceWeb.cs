﻿using Cazon.Models.Shopping.CazonCart;
using Cazon.Service.Common.Shopping.CazonCart;
using Cazon.Service.Infrastructure;
using Cazon.Service.Infrastructure.Manager;
using Cazon.Service.Web.Contract.Shopping;
using Cazon.ViewModel.Common.Shopping.Cart;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Web.Shopping
{
    public class CartServiceWeb : CartService, ICartServiceWeb
    {
        public async Task<CazonResult<ViewCartDTO>> GetCurrentCartDetails()
        {
            var result = new CazonResult<ViewCartDTO>()
            {
                Result = new ViewCartDTO()
            };

            var currentCartId = WebCartManager.CurrentCartId;
            if (currentCartId > 0)
            {
                var cart = await Repo.FindById(new object[] { currentCartId }, c => c.CartItems);

                if (cart != null)
                {
                    result.Result.CartId = cart.CartId;
                    result.Result.CartItems = cart.CartItems.Select(c => new ViewCartItemDTO
                    {
                        CartItemId = c.CartItemId,
                        ProductId = c.ProductId,
                        AddedOn = c.AddedOn,
                        ProductName = c.Product.Name,
                        ProductPrice = c.Product.Price,
                        Quantity = c.Quantity,
                        CartItemPrice = (c.Quantity * c.Product.Price),
                        Description = c.Product.Description,
                        QuantityInStock = c.Product.QuantityInStock,
                        ProductImageName = c.Product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                        CazonShopId = c.Product.CazonShopId,
                        CazonShopName = c.Product.CazonShop.Name,
                        ProductCategoryId = c.Product.ProductCategoryId,
                        ProductCategoryName = c.Product.ProductCategory.CategoryName
                    })
                    .ToList();

                    result.Success = true;
                }
            }
            else
            {
            }

            return result;
        }

        public async Task<CazonResult<bool>> AddProductToCart(AddProductToCartDTO model)
        {
            var result = new CazonResult<bool>();
            if (!await ProductRepo.IsEntityExists(model.ProductId))
            {
                result.Errors.Add("This product is not exists at our site.");
                result.Result = false;
            }
            else
            {
                var currentCart = await GetCurrentCart();
                if (!await CartItemRepo.IsProductInTheCart(WebCartManager.CurrentCartId, model.ProductId))
                {
                    currentCart.CartItems.Add(new CartItem
                    {
                        ProductId = model.ProductId,
                        Quantity = 1
                    });

                    await Repo.UpdateEntity(currentCart);
                    SessionManager.AddOrUpdateValue(WebCartManager.SessionCartItemCountKey, currentCart.CartItems.Count);
                    result.Success = true;
                    result.Result = true;
                }
                else
                {
                    result.Errors.Add("This product is already in the cart.");
                    result.Result = false;
                }
            }
            return result;
        }

        public async Task<CazonResult<bool>> UpdateCartItem(UpdateCartItemDTO model)
        {
            var result = new CazonResult<bool>();
            if (!IsCartExpired(result))
            {
                var cartItem = await CartItemRepo.GetCartItemByCartId(WebCartManager.CurrentCartId, model.CartItemId);
                if (cartItem != null)
                {
                    cartItem.Quantity = model.Quantity;
                    await CartItemRepo.UpdateEntity(cartItem);
                    result.Success = true;
                    result.Result = true;
                }
                else
                {
                    result.Errors.Add("This product is not in the cart.");
                    result.Result = false;
                }
            }

            return result;
        }

        public async Task<CazonResult<bool>> UpdateCart(UpdateCartDTO model)
        {
            var result = new CazonResult<bool>();

            if (!IsCartExpired(result))
            {
                var cartItems = await CartItemRepo.GetAllCartItemsByCartId(WebCartManager.CurrentCartId);

                foreach (var item in model.CartItems)
                {
                    var updatedItem = cartItems.FirstOrDefault(i => i.CartItemId == item.CartItemId);
                    updatedItem.Quantity = item.Quantity;
                }

                if (await CartItemRepo.UpdateEntities(cartItems) != null)
                {
                    result.Success = true;
                    result.Result = true;
                }
                else
                {
                    result.Success = false;
                    result.Result = false;
                }
            }

            return result;
        }

        public async Task<CazonResult<bool>> DeleteCartItem(DeleteCartItemDTO model)
        {
            var result = new CazonResult<bool>();
            if (!IsCartExpired(result))
            {
                var cartItem = await CartItemRepo.GetCartItemByCartId(WebCartManager.CurrentCartId, model.CartItemId);
                if (cartItem != null)
                {
                    await CartItemRepo.DeleteEntity(cartItem);
                    SessionManager.AddOrUpdateValue(WebCartManager.SessionCartItemCountKey, WebCartManager.NoOfCartItems - 1);
                    result.Success = true;
                    result.Result = true;
                }
                else
                {
                    result.Errors.Add("This item is not in your cart.");
                    result.Result = false;
                }
            }

            return result;
        }

        public async Task<bool> MigrateCurrentCart(string userId)
        {
            var currentCartId = WebCartManager.CurrentCartId;
            if(currentCartId > 0)
            {
                var currentCart = await Repo.GetCartBySessionId(CazonIdentityManager.CurrentSessionId);
                currentCart.UserId = userId;

                await Repo.UpdateEntity(currentCart);

                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartIdKey, currentCart.CartId);
                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartItemCountKey, currentCart.CartItems.Count);
            }
            else
            {
                var cartIdAndItemsCountByUser = await Repo.GetCartIdAndItemsCountByUser(userId);

                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartIdKey, cartIdAndItemsCountByUser.Item1);
                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartItemCountKey, cartIdAndItemsCountByUser.Item2);
            }

            return true;
        }

        protected async Task<Cart> GetCurrentCart(bool createNewCart = true)
        {
            var currentCart = await (CazonIdentityManager.IsLoggedIn ? Repo.GetCartByUserId(CazonIdentityManager.CurrentUserId) : Repo.GetCartBySessionId(CazonIdentityManager.CurrentSessionId));

            if (createNewCart && currentCart == null)
            {
                currentCart = new Cart
                {
                    SessionId = CazonIdentityManager.CurrentSessionId
                };

                if (CazonIdentityManager.IsLoggedIn)
                    currentCart.UserId = CazonIdentityManager.CurrentUserId;

                await Repo.AddEntity(currentCart);
            }

            if (currentCart != null)
            {
                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartIdKey, currentCart.CartId);
                SessionManager.AddOrUpdateValue(WebCartManager.SessionCartItemCountKey, currentCart.CartItems.Count);
            }

            return currentCart;
        }

        protected async Task<int> GetCurrentCartId()
        {
            var cartId = WebCartManager.CurrentCartId;

            if (cartId == -1)
            {
                var currentCart = await GetCurrentCart();
                cartId = currentCart.CartId;
            }

            return cartId;
        }

        protected bool IsCartExpired<TResult>(CazonResult<TResult> result)
        {
            var cartId = WebCartManager.CurrentCartId;
            if (cartId == -1)
                result.Errors.Add("Sorry, your cart is expired.");

            return (cartId == -1);
        }
    }
}
