﻿using Cazon.Service.Common;
using Cazon.Service.Common.Contract;
using Cazon.Service.Web.Contract;
using Cazon.ViewModel.Common.Product;
using Cazon.ViewModel.Web;
using Cazon.ViewModel.Web.Products;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Web
{
    public class ProductServiceWEB : ProductService, IProductServiceWEB
    {
        public ProductServiceWEB(ICloudinaryService cloudinaryService): base(cloudinaryService)
        {
        }

        public async Task<ViewCategoryProductsDTO> GetCategoyProducts(string categoryName, IProductSearchFilters filters)
        {
            var products = await Repo.GetProductsByCategoryName(categoryName, filters);

            var categoryProducts = new ViewCategoryProductsDTO
            {
                CategoryName = categoryName,
                Products = products.Select(p => new ProductByCategoryNameDTO
                {
                    ProductId= p.Id,
                    ProductName= p.Name,
                    ProductPrice= p.Price,
                    Discount = ((p.DiscountStartFrom != null && p.DiscountStartFrom <= DateTime.Now) && (p.DiscountEndOn == null || p.DiscountEndOn >= DateTime.Now)) ? p.Discount.Value : 0.0,
                    DefaultImageName= p.Images.FirstOrDefault(i => i.IsDefault == true).ImgName
                }).ToList()
            };

            return categoryProducts;
        }

        public async Task<ViewProductDetailsDTO> GetProductDetailsByNameAndId(string productName, int productId)
        {
            var product = await Repo.GetProductByNameAndId(productName, productId);

            return product == null ? null : new ViewProductDetailsDTO
            {
                ProductId = product.Id,
                ProductName = product.Name,
                ProductPrice = product.Price,
                ShopId = product.CazonShopId,
                ShopName = product.CazonShop.Name,
                Description = product.Description,
                DefaultImageName = product.Images.FirstOrDefault(p => p.IsDefault == true).ImgName,
                ProductImages = product.Images.Select(i => i.ImgName).ToList(),
                ProductCategoryId = product.ProductCategoryId,
                ProductCategoryName = product.ProductCategory.CategoryName,
                Discount = ((product.DiscountStartFrom != null && product.DiscountStartFrom <= DateTime.Now) && (product.DiscountEndOn == null || product.DiscountEndOn >= DateTime.Now)) ? product.Discount.Value : 0.0,
                DiscountEndDate = product.DiscountEndOn,

                Rating = 0.0
            };
        }
    }
}
