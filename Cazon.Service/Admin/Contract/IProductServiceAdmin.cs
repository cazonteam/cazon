﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Admin.Contract
{
    public interface IProductServiceAdmin : IProductService
    {
        Task<GetProductDetailsDTO> GetProductDetails(int productId);

        Task<List<GetProductsBasicInfoIdDTO>> GetProductsBasicDetailsByShopId(int shopId);

        Task<int> AddProduct(AddProductAdminDTO model);

        Task<bool> UpdateProduct(UpdateProductAdminDTO model);
    }
}
