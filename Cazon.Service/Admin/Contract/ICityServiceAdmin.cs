﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Admin.Contract
{
    public interface ICityServiceAdmin : ICityService
    {
        Task<List<KeyValuePair<int, string>>> GetCityForSelect();
    }
}
