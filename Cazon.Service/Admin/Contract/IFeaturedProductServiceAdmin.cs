﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Admin.Contract
{
    public interface IFeaturedProductServiceAdmin : IFeaturedProductService
    {
        Task<List<GetFeaturedProductsDTO>> GetAllFeaturedProducts();

        Task<int> AddFeaturedProduct(AddFeaturedProductDTO model);

        Task<bool> UpdateFeaturedProduct(UpdateFeaturedProductDTO model);

        Task<bool> DeleteFeaturedProduct(int featureProductId);
    }
}
