﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Admin;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Admin.Contract
{
    public interface IProductCategoryServiceAdmin : IProductCategoryService
    {
        // Find and get the entitiy along with all it's child elements.
        Task<GetProductCategorySimpleDTO> GetById(object key);

        // Find and get the entitiy without fetching it's child elements.
        Task<GetProductCategorySimpleDTO> FindById(object key);

        Task<List<GetProductCategoryAdminDTO>> GetAllCategories();

        Task<List<KeyValuePair<int, string>>> GetAllCategoriesWithBasicInfo();

        Task<GetProductCategorySimpleDTO> Add(AddProductCategoryDTO model);

        Task<bool> Update(EditProductCategoryDTO entity);
    }
}