﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Admin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cazon.Service.Admin.Contract
{
    public interface ICazonShopServiceAdmin : ICazonShopService
    {
        Task<List<KeyValuePair<int, string>>> GetAllShopByOwner(string ownerId);

        Task<List<GetShopBasicDetailsDTO>> GetAllShops(string onwerId);

        Task<int> AddShop(AddShopAdminDTO model);

        Task<bool> UpdateShop(EditShopAdminDTO model);
    }
}
