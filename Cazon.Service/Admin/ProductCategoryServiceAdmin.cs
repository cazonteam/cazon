﻿using Cazon.Models;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.Admin;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Admin
{
    public class ProductCategoryServiceAdmin : ProductCategoryService, IProductCategoryServiceAdmin
    {
        public async Task<GetProductCategorySimpleDTO> GetById(object key)
        {
            Repo.ProxyCreationEnabled(false);
            var entity = await Repo.FindById(key);
            Repo.ProxyCreationEnabled(true);
            GetProductCategorySimpleDTO dto = null;

            if (entity != null)
            {
                dto = new GetProductCategorySimpleDTO
                {
                    CategoryId = entity.Id,
                    CategoryName = entity.CategoryName,
                    IconName = entity.IconName,
                    OrderNumber = entity.OrderNumber
                };
            }

            return dto;
        }

        public async Task<GetProductCategorySimpleDTO> FindById(object key)
        {
            return await GetById(key);
        }
        
        public async Task<List<GetProductCategoryAdminDTO>> GetAllCategories()
        {
            Repo.ProxyCreationEnabled(false);
            var categories = await base.Repo.GetAll();
            Repo.ProxyCreationEnabled(true);

            return categories.Select(c => new GetProductCategoryAdminDTO
            {
                CategoryId = c.Id,
                CategoryName = c.CategoryName,
                IconName = c.IconName,
                OrderNumber = c.OrderNumber,
                ParentCategoryId = c.ParentCategoryId
            })
            .ToList();
        }

        public async Task<List<KeyValuePair<int, string>>> GetAllCategoriesWithBasicInfo()
        {
            Repo.ProxyCreationEnabled(false);
            var categories = await base.Repo.GetAll();
            Repo.ProxyCreationEnabled(true);

            return categories.Select(c => new KeyValuePair<int, string>(c.Id, c.CategoryName)).ToList();
        }

        public async Task<GetProductCategorySimpleDTO> Add(AddProductCategoryDTO model)
        {
            var entity = new ProductCategory
            {
                CategoryName = model.CategoryName,
                IconName = model.IconName,
                OrderNumber = model.OrderNumber,
                ParentCategoryId = (model.ParentCategoryId > 0) ? (int?)model.ParentCategoryId : null
            };
            
            GetProductCategorySimpleDTO dto = null;

            if (await Repo.AddEntity(entity) != null)
            {
                dto = new GetProductCategorySimpleDTO
                {
                    CategoryId = entity.Id,
                    CategoryName = entity.CategoryName,
                    IconName = entity.IconName,
                    OrderNumber = entity.OrderNumber
                };
            }

            return dto;
        }

        public async Task<bool> Update(EditProductCategoryDTO model)
        {
            var entity = await Repo.FindById(model.CategoryId);
            if (entity != null)
            {
                if (!string.IsNullOrEmpty(model.CategoryName))
                    entity.CategoryName = model.CategoryName;

                if (!string.IsNullOrEmpty(model.IconName))
                    entity.IconName = model.IconName;

                if (model.OrderNumber > 0)
                    entity.OrderNumber = model.OrderNumber;

                if (model.ParentCategoryId > 0)
                    entity.ParentCategoryId = model.ParentCategoryId;

                return (await Repo.UpdateEntity(entity) != null);
            }

            return false;
        }
    }
}
