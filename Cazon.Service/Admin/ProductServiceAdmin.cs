﻿using Cazon.Models;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Admin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Admin
{
    public class ProductServiceAdmin : ProductService, IProductServiceAdmin
    {
        public ProductServiceAdmin(ICloudinaryService cloudinaryService) : base(cloudinaryService)
        {

        }

        public async Task<GetProductDetailsDTO> GetProductDetails(int productId)
        {
            var product = await Repo.FindById(productId);

            var model = (product == null) ? null : new GetProductDetailsDTO
            {
                ProductId = product.Id,
                ProductName = product.Name,
                ProductPrice = product.Price,
                CreatedOn = product.CreatedOn,
                QuantityInStock = product.QuantityInStock,
                ProductDescription = product.Description,
                Discount = product.Discount,
                DiscountStartFrom = product.DiscountStartFrom,
                DiscountEndOn = product.DiscountEndOn,
                ShopId = product.CazonShopId,
                ProductCategoryId = product.ProductCategoryId,
                DefaultImageName = product.Images.FirstOrDefault(i => i.IsDefault == true).ImgName,
                ProductImages = product.Images.Select(i => i.ImgName).ToList()
            };

            return model;
        }

        public async Task<List<GetProductsBasicInfoIdDTO>> GetProductsBasicDetailsByShopId(int shopId)
        {
            var products = await Repo.GetProductsByShop(shopId);

            return products.Select(p => new GetProductsBasicInfoIdDTO
            {
                ProductId = p.Id,
                ProductCategoryId = p.ProductCategoryId,
                ProductName = p.Name,
                ProductPrice = p.Price,
                QuantityInStock = p.QuantityInStock
            })
            .ToList();
        }

        public async Task<int> AddProduct(AddProductAdminDTO model)
        {
            var product = base.AddProduct(model);
            product.Images = product.Images ?? new List<ProductImage>();

            product.Images = model.UploadedFiles.Select(i => new ProductImage
            {
                ImgName = i.UploadedOn.ToTimeStamp(),
                IsDefault = (i.File.FileName == model.DefaultImageName)
            })
            .ToList();

            if (!product.Images.Any(i => i.IsDefault == true))
            {
                product.Images.ElementAt(0).IsDefault = true;
            }

            await Repo.AddEntity(product);

            return product.Id;
        }

        public async Task<bool> UpdateProduct(UpdateProductAdminDTO model)
        {
            var product = await base.UpdateProduct(model);

            if (product != null)
            {
                foreach (var image in model.UploadedFiles)
                {
                    var imageToAdd = new ProductImage
                    {
                        ImgName = image.UploadedOn.ToTimeStamp(),
                        IsDefault = false,
                        ProductId = product.Id
                    };

                    Repo.SetEntityState(EntityState.Added, imageToAdd);
                }

                if (!string.IsNullOrEmpty(model.DefaultImageName))
                {
                    var defaultFile = model.UploadedFiles.FirstOrDefault(f => f.File.FileName == model.DefaultImageName);
                    var imageName =  defaultFile != null ? defaultFile.UploadedOn.ToTimeStamp() : model.DefaultImageName;

                    foreach (var image in product.Images)
                    {
                        if (!model.DeletedImages.Contains(image.ImgName))
                        {
                            image.IsDefault = !string.IsNullOrEmpty(imageName) && (image.ImgName == imageName);
                        }
                    }
                }

                if (!product.Images.Any(i => i.IsDefault == true))
                {
                    product.Images.ElementAt(0).IsDefault = true;
                }

                return (await Repo.UpdateEntity(product) != null);
            }

            return false;
        }
    }
}
