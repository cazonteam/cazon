﻿using Cazon.Models;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.Admin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Admin
{
    public class CazonShopServiceAdmin : CazonShopService, ICazonShopServiceAdmin
    {
        public async Task<List<KeyValuePair<int, string>>> GetAllShopByOwner(string ownerId)
        {
            var shops = await Repo.GetAllShopsByOwner(ownerId);

            return shops.Select(s => new KeyValuePair<int, string>(s.Id, s.Name)).ToList();
        }

        public async Task<List<GetShopBasicDetailsDTO>> GetAllShops(string ownerId)
        {
            var shops = await Repo.GetAllShopsByOwner(ownerId);

            return shops.Select(s => new GetShopBasicDetailsDTO
            {
                ShopId = s.Id,
                ShopName = s.Name,
                RegisteredOn = s.RegisteredOn,
                ShopPhoneNumber = s.ShopPhoneNumber,
                ShopDescription = s.Description,
                CityId = s.Address.CityId
            })
            .ToList();
        }

        public async Task<int> AddShop(AddShopAdminDTO model)
        {
            var shop = new CazonShop
            {
                Name = model.ShopName,
                RegisteredOn = DateTime.Now,
                Description = model.ShopDescription,
                OwnerId = model.OwnerId,
                ShopPhoneNumber = model.ShopPhoneNumber,
                Address = new Address
                {
                    CityId = model.CityId,
                    Latitude = model.CityId.ToString(),
                    Longitude = model.CityId.ToString()
                }
            };

            await Repo.AddEntity(shop);

            return shop.Id;
        }

        public async Task<bool> UpdateShop(EditShopAdminDTO model)
        {
            var shop = await Repo.FindById(model.ShopId);

            if (!string.IsNullOrEmpty(model.ShopName))
                shop.Name = model.ShopName;
            if (!string.IsNullOrEmpty(model.ShopPhoneNumber))
                shop.ShopPhoneNumber = model.ShopPhoneNumber;
            if (!string.IsNullOrEmpty(model.ShopDescription))
                shop.Description = model.ShopDescription;
            if (model.CityId > 0)
            {
                shop.Address.CityId = model.CityId;
                Repo.SetEntityState(EntityState.Modified, shop.Address);
            }


            return (await Repo.UpdateEntity(shop) != null);
        }
    }
}
