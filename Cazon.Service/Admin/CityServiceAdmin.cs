﻿using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Admin
{
    public class CityServiceAdmin : CityService, ICityServiceAdmin
    {
        public async Task<List<KeyValuePair<int, string>>> GetCityForSelect()
        {
            var cities = await Repo.GetAll();

            return cities.Select(c => new KeyValuePair<int, string>(c.Id, c.FullName)).ToList();
        }
    }
}
