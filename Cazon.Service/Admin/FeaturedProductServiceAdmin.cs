﻿using Cazon.Models.Products;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.ViewModel.Common.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Service.Admin
{
    public class FeaturedProductServiceAdmin : FeaturedProductService, IFeaturedProductServiceAdmin
    {
        public async Task<List<GetFeaturedProductsDTO>> GetAllFeaturedProducts()
        {
            var products = await Repo.GetAll();

            var dto = products.Select(p => new GetFeaturedProductsDTO
            {
                Id = p.Id,
                StartFrom = p.StartFrom,
                EndOn = p.EndOn.Value,
                ProductId = p.ProductId,
                ProductName = p.Product.Name
            })
            .ToList();

            return dto;
        }

        public async Task<int> AddFeaturedProduct(AddFeaturedProductDTO model)
        {
            var entity = await Repo.GetFeaturedProductByProductId(model.ProductId);

            if (entity != null)
            {
                entity.StartFrom = model.StartFrom;
                entity.EndOn = model.EndOn.HasValue ? model.EndOn.Value : DateTime.MaxValue;

                await Repo.UpdateEntity(entity);
            }
            else
            {
                entity = new FeaturedProduct
                {
                    ProductId = model.ProductId,
                    StartFrom = model.StartFrom,
                    EndOn = model.EndOn.HasValue ? model.EndOn.Value : DateTime.MaxValue
                };

                await Repo.AddEntity(entity);
            }

            return entity.Id;
        }

        public async Task<bool> UpdateFeaturedProduct(UpdateFeaturedProductDTO model)
        {
            var entity = await Repo.FindById(model.Id);

            entity.StartFrom = model.StartFrom;
            entity.EndOn = model.EndOn;

            return (await Repo.UpdateEntity(entity) != null);
        }

        public async Task<bool> DeleteFeaturedProduct(int featureProductId)
        {
            return (await Repo.DeleteEntity(new FeaturedProduct { Id = featureProductId }) != null);
        }
    }
}
