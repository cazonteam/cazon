﻿using System;
using System.Configuration;

namespace Cazon.Admin.Infrastructure.Helpers
{
    public static class Config
    {
        public static T GetSettingByKey<T>(this string name)
        {
            var value = ConfigurationManager.AppSettings[name];
            if (value == null)
            {
                return (T)Convert.ChangeType("", typeof(T));
            }

            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}