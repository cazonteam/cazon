using Cazon.Admin.Model;
using Cazon.Service.Admin;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common;
using Cazon.Service.Common.CazonAuth;
using Cazon.Service.Common.Contract;
using Cazon.Service.Common.Contract.CazonAuth;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Web;
using Unity;
using Unity.Injection;

namespace Cazon.Admin
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            container.RegisterType<IProductCategoryServiceAdmin, ProductCategoryServiceAdmin>();
            container.RegisterType<IProductServiceAdmin, ProductServiceAdmin>();
            container.RegisterType<ICazonShopServiceAdmin, CazonShopServiceAdmin>();
            container.RegisterType<ICloudinaryService>(new InjectionFactory(o =>
            {
                return new CloudinaryService
                {
                    CloudinaryApiKey = CazonConstants.Cloudinary.CloudinaryApiKey,
                    CloudinaryApiSecret = CazonConstants.Cloudinary.CloudinaryApiSecret,
                    CloudinaryCloudName = CazonConstants.Cloudinary.CloudinaryCloudName,
                    CloudinaryUploadFolder = CazonConstants.Cloudinary.CloudinaryProductImageFolderName
                };
            }));
            container.RegisterType<ICityServiceAdmin, CityServiceAdmin>();
            container.RegisterType<IFeaturedProductServiceAdmin, FeaturedProductServiceAdmin>();

            container.RegisterType<IConfigureAuthService, ConfigureAuthService>();
            container.RegisterType<IUserIdentityService, UserIdentityService>();
            container.RegisterType<IOwinContext>(new InjectionFactory(o => HttpContext.Current.GetOwinContext()));
        }
    }
}