﻿using Cazon.Admin.Infrastructure.Helpers;
using System.Linq;
using System.Web.Configuration;
using System.Web.Optimization;

namespace Cazon.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/JQuery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/App").Include(
                "~/Scripts/App/fastclick.js",
                "~/Scripts/App/adminlte.js",
                "~/Scripts/App/Sparkline/jquery.sparkline.js",
                "~/Scripts/App/Slimscroll/jquery.slimscroll.js",
                "~/Scripts/App/demo.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/App/Bootstrap/bootstrap.css",
                      "~/Content/App/FontAwesome/font-awesome.css",
                      "~/Content/App/Ionicons/ionicons.css",
                      "~/Content/App/AdminLTE.css",
                      "~/Content/App/Skins/skin-blue.css"));

            BundleTable.EnableOptimizations = "BundleTable.EnableOptimizations".GetSettingByKey<bool>();
        }
    }
}
