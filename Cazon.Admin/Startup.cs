﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup("Cazon.Admin", typeof(Cazon.Admin.Startup))]
namespace Cazon.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
