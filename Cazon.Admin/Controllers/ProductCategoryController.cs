﻿using Cazon.Admin.Model;
using Cazon.Service.Admin.Contract;
using Cazon.ViewModel.Admin;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    public class ProductCategoryController : CazonAdminBaseCRUDController
    {
        IProductCategoryServiceAdmin Service;

        public ProductCategoryController(IProductCategoryServiceAdmin service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetAllCategories()
        {
            await base.GetAllEntitiesService(Service.GetAllCategories);

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddCategory(AddProductCategoryDTO model)
        {
            if (CheckForValidModel())
            {
                await base.AddEntityService(model, Service.Add);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> EditCategory(EditProductCategoryDTO model)
        {
            if (CheckForValidModel())
            {
                var entity = await Service.FindById(model.CategoryId);
                if (entity != null)
                {
                    await base.UpdateEntityService(model, Service.Update);

                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NO_ENTTIY_FOUND;
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteCategory(int productCategoryId)
        {
            if (CheckForNonZeroNonNegative(productCategoryId))
            {
                var entity = await Service.FindById(productCategoryId);

                if (entity != null)
                    await base.DeleteEntityService((object)productCategoryId, Service.Delete);
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE };
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}