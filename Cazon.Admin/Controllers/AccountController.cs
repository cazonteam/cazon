﻿using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Common;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Expressions;

namespace Cazon.Admin.Controllers
{
    [Authorize]
    public class AccountController : CazonAdminBaseCRUDController
    {
        IUserIdentityService UserIdentityService;

        public AccountController(IUserIdentityService userIdentityService)
        {
            UserIdentityService = userIdentityService;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if(User.Identity.IsAuthenticated)
                return this.RedirectToActionPermanent<HomeController>(a => a.Index());

            return View(new LoginViewModel { ReturnUrl = returnUrl ?? string.Empty });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return this.RedirectToActionPermanent<HomeController>(a => a.Index());

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //// This doesn't count login failures towards account lockout
            //// To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await UserIdentityService.PasswordSignIn(model);

            switch (result)
            {
                case SignInStatus.Success:
                    return this.RedirectToActionPermanent<HomeController>(a => a.Index());
                //case SignInStatus.LockedOut:
                //    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    break;
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            //    var result = await UserManager.CreateAsync(user, model.Password);
            //    if (result.Succeeded)
            //    {
            //        await SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);

            //        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            //        // Send an email with this link
            //        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
            //        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            //        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            //        return RedirectToAction("Index", "Home");
            //    }
            //    AddErrors(result);
            //}

            // If we got this far, something failed, redisplay form
            if (ModelState.IsValid)
            {
                var result = await UserIdentityService.Register(model);
                if (result.Succeeded)
                {
                    return this.RedirectToActionPermanent(a => a.Login(""));
                }

                AddErrors(result);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            UserIdentityService.SignOut();

            return this.RedirectToAction<HomeController>(a => a.Index());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ViewBag.CallBackUrl = "";
            return View(new ForgotPasswordViewModel { });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = await UserIdentityService.GetUserIdByEmail(model.Email);
                if (userId == null)
                {
                    //// Don't reveal that the user does not exist or is not confirmed
                    return this.RedirectToAction(a => a.ForgotPasswordConfirmation());
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                var token = await UserIdentityService.GeneratePasswordResetToken(userId);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = userId, code = token }, protocol: Request.Url.Scheme);
                ViewBag.CallBackUrl = callbackUrl;
                TempData["CallBackUrl"] = callbackUrl;
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");

                return this.RedirectToAction(a => a.ForgotPasswordConfirmation());
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            ViewBag.CallBackUrl = (string)TempData["CallBackUrl"];

            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var userId = await UserIdentityService.GetUserIdByEmail(model.Email);
            if(userId == null)
            {
                return this.RedirectToAction(a => a.ResetPasswordConfirmation());
            }

            var result = await UserIdentityService.ResetPassword(userId, model);
            if (result.Succeeded)
            {
                return this.RedirectToAction(a => a.ResetPasswordConfirmation());
            }

            AddErrors(result);

            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [NonAction]
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}