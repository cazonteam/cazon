﻿using Cazon.Admin.Model;
using Cazon.Service.Admin.Contract;
using Cazon.ViewModel.Admin;
using Cazon.ViewModel.Common;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    public class ShopController : CazonAdminBaseCRUDController
    {
        ICazonShopServiceAdmin Service;
        ICityServiceAdmin CityService;

        public ShopController(ICazonShopServiceAdmin service, ICityServiceAdmin cityService)
        {
            Service = service;
            CityService = cityService;
            DisposableObjects.Add(Service);
            DisposableObjects.Add(CityService);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> GetAllShops()
        {
            var shops = await Service.GetAllShops(User.Identity.GetUserId());
            var cities = await CityService.GetCityForSelect(); 
            CazonJsonResult[CazonConstants.KEY_SUCCESS] = new Dictionary<string, object>
            {
                { "Shops", shops },
                { "Cities", cities }
            };

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> GetAllShopForCurrentUser()
        {
            await base.GetEntityByIdService(User.Identity.GetUserId(), Service.GetAllShopByOwner);

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> AddShop(AddShopAdminDTO model)
        {
            if(CheckForValidModel())
            {
                if (await Service.IsShopExists(model.ShopName))
                {
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { $"Shop with name {model.ShopName} already exists." };
                }
                else
                {
                    model.OwnerId = User.Identity.GetUserId();
                    await base.AddEntityService(model, Service.AddShop);
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateShop(EditShopAdminDTO model)
        {
            if(CheckForValidModel() && await base.CheckEntityExists((object)model.ShopId, Service.IsEntityExists))
            {
                await base.UpdateEntityService(model, Service.UpdateShop);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteShop(int shopId)
        {
            if(CheckForNonZeroNonNegative(shopId))
            {
                if(await Service.CheckShopOwnership(shopId, User.Identity.GetUserId()))
                {
                    var model = new DeleteShopDTO{ ShopId = shopId, OwnerId = User.Identity.GetUserId() };
                    await base.DeleteEntityService(model, Service.DeleteShop);
                }
                else
                {
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { "Sorry, you are not the owner of this shop." };
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}
