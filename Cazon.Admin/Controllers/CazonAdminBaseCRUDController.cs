﻿using Cazon.Admin.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    public abstract class CazonAdminBaseCRUDController : CazonAdminBaseController
    {
        /// <summary>
        /// Find the entries from the database by calling the <paramref name="function"/> and add the result of the function to the CazonJsonResult and return true, or add the error/exception message to the response otherwise.
        /// </summary>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return as a list of <typeparamref name="TResult"/>.</typeparam>
        /// <param name="function">The reference to the method that will be called to get the list of entities.</param>
        /// <returns>return true if entities are found/not found without any error or false if any error or exception is occur.</returns>
        [NonAction]
        protected virtual async Task<bool> GetAllEntitiesService<TResult>(Func<Task<List<TResult>>> function)
        {
            try
            {
                var result = await function?.Invoke();
                CazonJsonResult[CazonConstants.KEY_SUCCESS] = result ?? new List<TResult>();
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return CazonJsonResult.ContainsKey(CazonConstants.KEY_SUCCESS);
        }

        /// <summary>
        /// Find the entity from the database by calling the <paramref name="function"/> and add the result of the function to the CazonJsonResult and return true, or add the error/exception message to the response otherwise.
        /// </summary>
        /// <typeparam name="TParam">The type of <paramref name="model"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return.</typeparam>
        /// <param name="entityId">The id of the entity that is being searched.</param>
        /// <param name="function">The reference to the method that will be called by passing the <paramref name="entityId"/>.</param>
        /// <returns>return true if entity is found/not found without any error or false if any error or exception is occur.</returns>
        [NonAction]
        protected virtual async Task<bool> GetEntityByIdService<TParam, TResult>(TParam entityId, Func<TParam, Task<TResult>> function)
        {
            bool isFound = false;

            try
            {
                var result = await function?.Invoke(entityId);
                if (result != null)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = result;
                    isFound = true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NO_ENTTIY_FOUND;
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isFound;
        }

        /// <summary>
        /// Checks if the entity is exists by calling the <paramref name="function"/>.
        /// </summary>
        /// <typeparam name="TParam">The type of <paramref name="entityId"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <param name="entityId">The id of the entity that is being searched.</param>
        /// <param name="function">The reference to the method that will be call to find out the existance of the entity.</param>
        /// <returns>return true if entity is found and add error, exception messages to the CazonJsonResult. </returns>
        [NonAction]
        protected virtual async Task<bool> CheckEntityExists<TParam>(TParam entityId, Func<TParam, Task<bool>> function)
        {
            bool isFound = false;

            try
            {
                if (await function?.Invoke(entityId))
                {
                    isFound = true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NO_ENTTIY_FOUND;
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isFound;
        }

        /// <summary>
        /// Add the entity using the service. If entity is Added successfully then add the result of the <paramref name="function"/> to the CazonJsonResult or add the validation exceptions errors or other exception messages to the CazonJsonResult otherwise.
        /// </summary>
        /// <typeparam name="TModel">The type of <paramref name="model"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return.</typeparam>
        /// <param name="model">The model that will be passed to the <paramref name="function"/>.</param>
        /// <param name="function">The reference to the method that will be called to add the entity.</param>
        /// <returns>return true if entity is successfully added or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> AddEntityService<TModel, TResult>(TModel model, Func<TModel, Task<TResult>> function)
        {
            bool isAdded = false;

            try
            {
                TResult result = await function?.Invoke(model);
                if (result != null)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = new object[] { result };
                    isAdded = true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.KEY_ERROR;
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isAdded;
        }

        /// <summary>
        /// Updates the entity using the service. If entity is updated successfully then add success message to the CazonJsonResult or add the validation exceptions errors or other exception messages to the CazonJsonResult otherwise.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model">The model that will be passed as argument to the function.</param>
        /// <param name="function">The reference to the method that will be called to the update the entity.</param>
        /// <returns>If entity is updated successfully returns true or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> UpdateEntityService<TModel>(TModel model, Func<TModel, Task<bool>> function)
        {
            var isUpdated = false;

            try
            {
                if (await function?.Invoke(model))
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = new string[] { CazonConstants.KEY_SUCCESS };
                    isUpdated = true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.KEY_ERROR };
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isUpdated;
        }

        /// <summary>
        /// Delete the entity using the service. If entity is deleted successfully then add success message to the CazonJsonResult or add the validation exceptions errors or other exception messages to the CazonJsonResult otherwise.
        /// </summary>
        /// <param name="functionParams">The model that will be passed as argument to the function.</param>
        /// <param name="function">The reference to the method that will be called when deleting the entity.</param>
        /// <returns>If entity is deleted successfully returns true or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> DeleteEntityService<TParam>(TParam functionParams, Func<TParam, Task<bool>> function)
        {
            var isDeleted = false;

            try
            {
                if (await function?.Invoke(functionParams))
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = new string[] { CazonConstants.MESSAGE_DELETED };
                    isDeleted = true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isDeleted;
        }

        /// <summary>
        /// Execute The <paramref name="function"/> and add the result to the CazonJsonResult if function executed successfully or add the validation exceptions errors/exception messages to the CazonJsonResult otherwise.
        /// </summary>
        /// <typeparam name="TModel">The type of <paramref name="model"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return.</typeparam>
        /// <param name="model">The model that will be passed to the <paramref name="function"/>.</param>
        /// <param name="function">The reference to the method that will be called to add the entity.</param>
        /// <returns>return true if function is successfully executed or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> ExecuteServiceFunc<TModel, TResult>(TModel model, Func<TModel, Task<TResult>> function)
        {
            try
            {
                var result = await function?.Invoke(model);
                if (result != null)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = new object[] { result };

                    return true;
                }
                else
                    CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.KEY_ERROR;
            }
            catch (DbEntityValidationException validationException)
            {
                LogError(validationException);
                CazonJsonResult[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                LogError(ex);
                CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return false;
        }

    }
}