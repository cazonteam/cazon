﻿using Cazon.Admin.Model;
using Cazon.Service.Admin.Contract;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.Admin;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    public class ProductController : CazonAdminBaseCRUDController
    {
        IProductServiceAdmin Service;
        IProductCategoryServiceAdmin CategoryService;
        ICloudinaryService CloudinaryService;

        public ProductController(IProductServiceAdmin service, IProductCategoryServiceAdmin categoryService, ICloudinaryService cloudinaryService)
        {
            Service = service;
            CategoryService = categoryService;
            CloudinaryService = cloudinaryService;

            DisposableObjects.Add(Service);
            DisposableObjects.Add(CategoryService);
            DisposableObjects.Add(CloudinaryService);
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetAllProducts(int shopId)
        {
            if (CheckForNonZeroNonNegative(shopId))
            {
                try
                {
                    var products = await Service.GetProductsBasicDetailsByShopId(shopId);
                    var allCategories = await CategoryService.GetAllCategoriesWithBasicInfo();

                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = new Dictionary<string, object>
                    {
                        { "Products", products },
                        { "AllCategories", allCategories }
                    };
                }
                catch (Exception ex)
                {
                    LogError(ex);
                    CazonJsonResult[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetProductDetails(int productId)
        {
            if (CheckForNonZeroNonNegative(productId))
            {
                await base.GetEntityByIdService(productId, Service.GetProductDetails);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddProduct(AddProductAdminDTO model)
        {
            if(CheckForValidModel())
            {
                var secondsToAdd = 1;
                foreach (var file in model.UploadedFiles)
                {
                    file.UploadedOn = DateTime.Now.AddSeconds(secondsToAdd++);
                }

                if(await base.AddEntityService(model, Service.AddProduct))
                {
                    await CloudinaryService.UploadFiles(model.UploadedFiles);
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateProduct(UpdateProductAdminDTO model)
        {
            if (CheckForValidModel())
            {
                var secondsToAdd = 1;
                foreach (var file in model.UploadedFiles)
                {
                    file.UploadedOn = DateTime.Now.AddSeconds(secondsToAdd++);
                }
                
                if(CheckForNonZeroNonNegative(model.ProductId) && await base.UpdateEntityService(model, Service.UpdateProduct))
                {
                    await CloudinaryService.UploadFiles(model.UploadedFiles);
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteProduct(int productId)
        {
            if(CheckForNonZeroNonNegative(productId))
            {
                if(await Service.CheckProductOwnerShip(User.Identity.GetUserId(), productId))
                {
                    if(await base.DeleteEntityService((object)productId, Service.DeleteProduct))
                    {

                    }
                }
                else
                {
                    CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { "This product is not belong to any of your shop." };
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}