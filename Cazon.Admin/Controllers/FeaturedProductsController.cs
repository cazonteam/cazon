﻿using Cazon.Admin.Model;
using Cazon.Service.Admin.Contract;
using Cazon.ViewModel.Common.Product;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    public class FeaturedProductsController : CazonAdminBaseCRUDController
    {
        IFeaturedProductServiceAdmin Service;
        IProductServiceAdmin ProductService;

        public FeaturedProductsController(IFeaturedProductServiceAdmin service, IProductServiceAdmin productService)
        {
            Service = service;
            ProductService = productService;

            DisposableObjects.Add(Service);
            DisposableObjects.Add(ProductService);
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetProductsForSelect(string searchTerm)
        {
            if(CheckForNonEmptyNull(searchTerm))
            {
                await base.ExecuteServiceFunc(searchTerm, ProductService.GetPoductsForSelect);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetFeaturedProducts()
        {
            await base.GetAllEntitiesService(Service.GetAllFeaturedProducts);

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddFeaturedProduct(AddFeaturedProductDTO model)
        {
            if(CheckForValidModel())
            {
                if (await base.CheckEntityExists((object)model.ProductId, ProductService.IsEntityExists))
                {
                    if (!await Service.IsProductFeatured(model.ProductId))
                    {
                        await base.AddEntityService(model, Service.AddFeaturedProduct);
                    }
                    else
                        CazonJsonResult[CazonConstants.KEY_ERROR] = new string[] { "This product is already in the featured list" };
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> UpdateFeaturedProduct(UpdateFeaturedProductDTO model)
        {
            if(CheckForValidModel() && await base.CheckEntityExists((object)model.Id, Service.IsEntityExists))
            {
                await base.UpdateEntityService(model, Service.UpdateFeaturedProduct);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteFeaturedProduct(int featuredProductId)
        {
            if(CheckForNonZeroNonNegative(featuredProductId) && await base.CheckEntityExists((object)featuredProductId, Service.IsEntityExists))
            {
                await base.DeleteEntityService(featuredProductId, Service.DeleteFeaturedProduct);
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}
