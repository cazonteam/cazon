﻿using Cazon.Admin.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Web.Mvc;

namespace Cazon.Admin.Controllers
{
    [Authorize]
    public abstract class CazonAdminBaseController : Controller
    {
        protected ICollection<IDisposable> DisposableObjects { set; get; } = new HashSet<IDisposable>();
        protected Dictionary<string, object> CazonJsonResult = new Dictionary<string, object>();

        protected CazonAdminBaseController()
        {
        }

        [NonAction]
        protected bool CheckForNonZeroNonNegative(int value)
        {
            if (value <= 0)
                CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NON_ZERO_ID;

            return (value > 0);
        }

        [NonAction]
        protected bool CheckForNonEmptyNull(string value)
        {
            if (string.IsNullOrEmpty(value))
                CazonJsonResult[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NON_ZERO_ID;

            return !string.IsNullOrEmpty(value);
        }

        [NonAction]
        protected bool CheckForValidModel()
        {
            if (!ModelState.IsValid)
                CazonJsonResult[CazonConstants.KEY_MODEL_STATE_ERROR] = GetModelStateErrors();

            return ModelState.IsValid;
        }

        [NonAction]
        protected virtual List<string> GetValidationErrors(DbEntityValidationException exception)
        {
            var errorList = new List<string>();

            foreach (var errros in exception.EntityValidationErrors)
                foreach (var error in errros.ValidationErrors)
                    errorList.Add(error.ErrorMessage);

            return errorList;
        }

        [NonAction]
        protected virtual List<string> GetModelStateErrors()
        {
            var errorList = new List<string>();

            foreach (var key in ModelState.Keys)
                foreach (var errors in ModelState[key].Errors)
                    if (!string.IsNullOrEmpty(errors.ErrorMessage))
                        errorList.Add(errors.ErrorMessage);

            return errorList;
        }

        [NonAction]
        protected void LogError(Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (var obj in DisposableObjects)
                    obj?.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}