﻿using Cazon.Admin.Infrastructure.Helpers;

namespace Cazon.Admin.Model
{
    public abstract class CazonConstants
    {
        public static string KEY_ERROR
        {
            get
            {
                return nameof(KEY_ERROR).GetSettingByKey<string>();
            }
        }

        public static string KEY_EXCEPTION
        {
            get
            {
                return nameof(KEY_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string KEY_SUCCESS
        {
            get
            {
                return nameof(KEY_SUCCESS).GetSettingByKey<string>();
            }
        }

        public static string KEY_VALIDATION_EXCEPTION
        {
            get
            {
                return nameof(KEY_VALIDATION_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string KEY_MODEL_STATE_ERROR
        {
            get
            {
                return nameof(KEY_MODEL_STATE_ERROR).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_EXCEPTION
        {
            get
            {
                return nameof(MESSAGE_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NULL_MODEL
        {
            get
            {
                return nameof(MESSAGE_NULL_MODEL).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NO_ENTTIY_FOUND
        {
            get
            {
                return nameof(MESSAGE_NO_ENTTIY_FOUND).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NO_ENTTIY_FOUND_ON_DELETE
        {
            get
            {
               return nameof(MESSAGE_NO_ENTTIY_FOUND_ON_DELETE).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NON_ZERO_ID
        {
            get
            {
                return nameof(MESSAGE_NON_ZERO_ID).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_DELETED
        {
            get
            {
                return nameof(MESSAGE_DELETED).GetSettingByKey<string>();
            }
        }

        public static string HEADERS_TO_REMOVE
        {
            get
            {
                return nameof(HEADERS_TO_REMOVE).GetSettingByKey<string>();
            }
        }

        public static string ElmahErrorLogApplicationName
        {
            get
            {
                return nameof(ElmahErrorLogApplicationName).GetSettingByKey<string>();
            }
        }

        public static class Cloudinary
        {
            public static string CloudinaryCloudName
            {
                get
                {
                    return nameof(CloudinaryCloudName).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryApiKey
            {
                get
                {
                    return nameof(CloudinaryApiKey).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryApiSecret
            {
                get
                {
                    return nameof(CloudinaryApiSecret).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryProductImageFolderName
            {
                get
                {
                    return nameof(CloudinaryProductImageFolderName).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryProductCategoryFolderName
            {
                get
                {
                    return nameof(CloudinaryProductCategoryFolderName).GetSettingByKey<string>();
                }
            }
        }
    }
}