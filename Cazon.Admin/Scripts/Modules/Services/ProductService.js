﻿(function () {
    var Cazon = angular.module("cazon");

    var ProductService = function ($http) {
        var controllerName = "Product";

        var getAllByShop = function (shopId) {
            return $http.get(controllerName + "/GetAllProducts?shopId=" + shopId);
        };

        var getById = function (productId) {            
            return $http.get(controllerName + "/GetProductDetails?productId=" + productId);
        };

        var update = function (product, imagesToUpload) {
            var formData = objectToFormData(product, formData, "model.");

            for (var index = 0; index < imagesToUpload.length; index++) {
                formData.append("model.UploadedFiles[" + index + "].File", imagesToUpload[index].FileData);
            }

            return $http.post(controllerName + "/UpdateProduct", formData, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            });
        };

        var create = function (product, imagesToUpload) {
            var formData = objectToFormData(product, formData, "model.");

            for (var index = 0; index < imagesToUpload.length; index++) {
                formData.append("model.UploadedFiles[" + index + "].File", imagesToUpload[index].FileData);
            }

            return $http.post(controllerName + "/AddProduct", formData, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            });
        };

        var destroy = function (productId) {
            return $http.post(controllerName + "/DeleteProduct?productId=" + productId);
        };

        // Takes a {} object and returns a FormData object.
        // Thanks to https://gist.github.com/ghinda/8442a57f22099bdb2e34
        var objectToFormData = function (obj, form, modelName, namespace) {
            var fd = form || new FormData();
            var formKey;

            for (var property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (namespace) {
                        formKey = namespace + '[' + property + ']';
                    } else {
                        formKey = property;
                    }

                    // if the property is an object, but not a File, use recursivity.
                    if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                        objectToFormData(obj[property], fd, modelName, property);
                    } else {
                        // if it's a string or a File object
                        fd.append(modelName + formKey, obj[property]);
                    }
                }
            }

            return fd;

        };

        return {
            getAllByShop: getAllByShop,
            getById: getById,
            update: update,
            create: create,
            delete: destroy
        };
    };

    ProductService.$inject = ["$http"];
    Cazon.factory("ProductService", ProductService);
})();
