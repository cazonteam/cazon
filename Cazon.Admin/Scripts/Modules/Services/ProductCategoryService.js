﻿(function () {
    var Cazon = angular.module("cazon");

    var ProductCategoryService = function ($http) {
        var controllerName = "ProductCategory";

        var getAll = function () {
            return $http.get(controllerName + "/GetAllCategories");
        };

        var getById = function (id) {
            return $http.post(controllerName + id);
        };

        var update = function (productCategory) {
            return $http.post(controllerName + "/EditCategory", createDTO(productCategory));
        };

        var create = function (productCategory) {
            return $http.post(controllerName + "/AddCategory", createDTO(productCategory));
        };

        var destroy = function (categoryId) {
            return $http.post(controllerName + "/DeleteCategory?productCategoryId=" + categoryId);
        };

        function createDTO(productCategory) {
            var dto = {};
            if (productCategory.CategoryId) dto["CategoryId"] = productCategory.CategoryId;
            if (productCategory.CategoryName) dto["CategoryName"] = productCategory.CategoryName;
            if (productCategory.IconName) dto["IconName"] = productCategory.IconName;
            if (productCategory.OrderNumber) dto["OrderNumber"] = productCategory.OrderNumber;
            if (productCategory.ParentCategoryId) dto["ParentCategoryId"] = productCategory.ParentCategoryId;

            return dto;
        }

        return {
            getAll: getAll,
            getById: getById,
            update: update,
            create: create,
            delete: destroy
        };
    };

    ProductCategoryService.$inject = ["$http"];
    Cazon.factory("ProductCategoryService", ProductCategoryService);
})();
