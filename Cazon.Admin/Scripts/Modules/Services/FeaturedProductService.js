﻿(function () {
    var Cazon = angular.module("cazon");

    var FeaturedProductService = function ($http) {
        var controllerName = "FeaturedProducts/";

        var getAll = function () {
            return $http.get(controllerName + "GetFeaturedProducts");
        };

        var add = function (featuredProduct) {
            return $http.post(controllerName + "AddFeaturedProduct", featuredProduct);
        };

        var update = function (featuredProduct) {
            return $http.post(controllerName + "UpdateFeaturedProduct", featuredProduct);
        };

        var destroy = function (featuredProductId) {
            return $http.post(controllerName + "DeleteFeaturedProduct?featuredProductId=" + featuredProductId);
        }

        return {
            getAllFeaturedProducts: getAll,
            addFeaturedProduct: add,
            updateFeaturedProduct: update,
            deleteFeaturedProduct: destroy
        };
    };

    FeaturedProductService.$inject = ["$http"];
    Cazon.factory("FeaturedProductService", FeaturedProductService);
})();
