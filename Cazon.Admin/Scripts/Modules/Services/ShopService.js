﻿(function () {
    var Cazon = angular.module("cazon");

    var ShopService = function ($http) {
        var controllerName = "Shop";

        var getAll = function () {
            return $http.post(controllerName + "/GetAllShops");
        };

        var getAllShopForCurrentUser = function () {
            return $http.post(controllerName + "/GetAllShopForCurrentUser");
        };

        var getById = function (shopId) {
            return $http.post(controllerName + "/GetShopDetails?shopId=" + shopId);
        };

        var update = function (shop) {
            return $http.post(controllerName + "/UpdateShop", shop);
        };

        var create = function (shop) {
            return $http.post(controllerName + "/AddShop", shop);
        };

        var destroy = function (shopId) {
            var data = angular.toJson({ "shopId": shopId });
            return $http.post(controllerName + "/DeleteShop", data);
        };

        return {
            getAllShops: getAll,
            getAllShopForCurrentUser: getAllShopForCurrentUser,
            getById: getById,
            update: update,
            create: create,
            delete: destroy
        };
    };

    ShopService.$inject = ["$http"];
    Cazon.factory("ShopService", ShopService);
})();
