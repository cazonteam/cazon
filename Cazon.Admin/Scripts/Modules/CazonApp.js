﻿(function() {
    var Cazon = angular.module("cazon", ["ngTable"]);
    var CazonConstants = {
        "BaseApiUrl": "http://api.cazon.local/api/",
        "BaseAdminUrl": "http://admin.cazon.local/"
    };

    Cazon.constant("CazonConstants", CazonConstants);
})();


