﻿/// <reference path="../../Angular/angular.js" />
/// <reference path="../../JQuery/jquery-3.2.1.js" />

(function () {
    var Cazon = angular.module("cazon");

    var ProductController = function ($scope, $filter, NgTableParams, productService, shopService) {

        $scope.Processing = true;
        $scope.IsEditing = false;
        $scope.IsAdding = false;

        $scope.LoadData = function () {
            $scope.Processing = true;
            $scope.IsEditing = false;
            $scope.IsAdding = false;
            $scope.Products = [];

            $scope.TableParams = new NgTableParams({
                count: 15
            }, {
                getData: function (params) {
                    $scope.Processing = true;

                    if ($scope.Shops.AllShops.length == 0) {
                        return GetShops();
                    }
                    else if (!$scope.Products || $scope.Products.length == 0) {
                        return productService.getAllByShop($scope.Shops.SelectedShopForSort)
                            .then(function (result) {
                                $scope.Products = result["data"]["Success"]["Products"];
                                $scope.Categories.AllCategories = result["data"]["Success"]["AllCategories"];

                                $scope.Processing = false;

                                return getDataForBinding();
                            },
                            function (error) {
                                console.log(error);
                                $scope.Processing = false;
                            });
                    }
                    else {
                        $scope.Processing = false;

                        return getDataForBinding();
                    }

                    function getDataForBinding() {
                        params.total($scope.Products.length);

                        var orderedData = params.sorting() ? $filter('orderBy')($scope.Products, params.orderBy()) : $scope.Products;

                        var filter = {};
                        $.each(Object.keys(params.filter()), function (index, elem) {
                            if (params.filter()[elem]) {
                                var keys = elem.split('.');
                                filter[keys[0]] = {};
                                if (keys[1])
                                    filter[keys[0]][keys[1]] = params.filter()[elem];
                                else
                                    filter[keys[0]] = params.filter()[elem];
                            }
                        });

                        orderedData = params.filter() ? $filter('filter')(orderedData, filter) : orderedData;

                        return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    };
                }
            });
        };

        $scope.CreateNew = function () {
            $scope.Processing = true;
            ClearEditModel();
            $scope.IsAdding = true;
            $scope.IsEditing = false;
            $scope.Categories.SelectedProductCategory = $scope.Categories.AllCategories[0].Key.toString();
            $scope.Shops.SelectedShop = $scope.Shops.AllShops[0].Key.toString();
            $scope.Processing = false;
        };

        $scope.EditProduct = function (productId) {
            $scope.Processing = true;
            $scope.IsEditing = true;
            ClearEditModel();

            productService.getById(productId).then(
                function (successResponse) {
                    $scope.EditModel.EditingProduct = successResponse["data"]["Success"];
                    $scope.EditModel.EditingProduct.DiscountStartFrom = $scope.FormatDate($scope.EditModel.EditingProduct.DiscountStartFrom);
                    $scope.EditModel.EditingProduct.DiscountEndOn = $scope.FormatDate($scope.EditModel.EditingProduct.DiscountEndOn);
                    $scope.Categories.SelectedProductCategory = $scope.EditModel.EditingProduct.ProductCategoryId.toString();
                    $scope.Shops.SelectedShop = $scope.EditModel.EditingProduct.ShopId.toString();
                    $scope.Processing = false;
                },
                function (error) {
                    console.log(error);
                    $scope.Processing = false;
                });
        };

        $scope.DeleteProduct = function (product) {
            $scope.Processing = true;

            if (confirm("Do you really want to delete the product? You can't undo once you deleted the product...")) {
                productService.delete(product.ProductId).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.Products.length; index++) {
                                if ($scope.Products[index].ProductId == product.ProductId) {
                                    $scope.Products.splice(index, 1);
                                }
                            }
                            $scope.TableParams.reload();
                        }
                    },
                    function (errorResponse) {
                        console.log(errorResponse);
                    });
            }

            $scope.Processing = false;
        };

        $scope.SaveProduct = function () {
            $scope.Processing = true;
            if (!ValidateProduct($scope.EditModel.EditingProduct)) {
                $scope.Processing = false;
                return;
            }

            var product = angular.copy($scope.EditModel.EditingProduct);
            product.ProductCategoryId = $scope.Categories.SelectedProductCategory;
            product.ShopId = $scope.Shops.SelectedShop;

            product.NewImages = [];
            $.each($scope.EditModel.NewImages, function (index, elem) {
                product.NewImages.push(elem.ImageName);
            });
            product.DeletedImages = $scope.EditModel.DeletedImages;

            if ($scope.IsEditing) {
                productService.update(product, $scope.EditModel.NewImages).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.Products.length; index++) {
                                if ($scope.Products[index].ProductId == product.ProductId) {
                                    $scope.Products[index] = product;
                                    $scope.TableParams.reload();
                                    break;
                                }
                            }
                        }
                        $scope.Processing = false;
                    },
                    function (error) {
                        console.log(error);
                        $scope.Processing = false;
                    });
            }
            else if ($scope.IsAdding) {
                productService.create(product, $scope.EditModel.NewImages).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            product.ProductId = successResponse["data"]["Success"][0];
                            $scope.Products.push(product);
                            $scope.TableParams.reload();
                        }
                        else if (successResponse["data"]["InvalidModelState"]) {
                            $.each(successResponse["data"]["InvalidModelState"], function (index, error) {
                                $scope.ProductForm.$error.push(error);
                            });
                        }
                        else if (successResponse["data"]["Exception"]) {

                        }
                        else if (successResponse["data"]["Error"]) {

                        }

                        $scope.Processing = false;
                    },
                    function (error) {
                        console.log(error);
                        $scope.Processing = false;
                    });
            }
        };

        $scope.CancelChanges = function () {
            $scope.IsAdding = false;
            $scope.IsEditing = false;
            $scope.Processing = false;
        };

        $scope.RemoveImage = function (index) {
            if (confirm("Do you really want to delete the product image?")) {
                $scope.EditModel.DeletedImages.push($scope.EditModel.EditingProduct.ProductImages[index]);
                $scope.EditModel.EditingProduct.ProductImages.splice(index, 1);
                if (!IsDafaultImageExists())
                    $scope.EditModel.EditingProduct.DefaultImageName = "";
            }
        };

        $scope.RemoveNewImage = function (index) {
            $scope.EditModel.NewImages.splice(index, 1);
            if (!IsDafaultImageExists())
                $scope.EditModel.EditingProduct.DefaultImageName = "";

        };

        $scope.AddNewImage = function () {
            $scope.EditModel.NewImages.push({ ImageName: "" });
        };

        $scope.MakeImageDefault = function (imageName) {
            $scope.EditModel.EditingProduct.DefaultImageName = imageName;
        };

        $scope.FileNameChanged = function (elem, index) {
            $scope.EditModel.NewImages[index].ImageName = elem.files[0].name;
            $scope.EditModel.NewImages[index].FileData = elem.files[0];
        };

        $scope.GetProductName = function (productCategoryId) {
            var categoryName = "";

            if (productCategoryId && productCategoryId > 0) {
                $.each($scope.Categories.AllCategories, function (index, elem) {
                    if (elem.Key == productCategoryId) {
                        categoryName = elem.Value;
                        return;
                    }
                });
            }

            return categoryName;
        };

        $scope.FormatDate = function (date) {
            return date ? moment(date).format(dateFormatPattern) : "";
        };

        function ValidateProduct(product) {
            $scope.ProductForm.$error = [];

            if (!product) return false;

            if (product.ProductName.length == 0) {
                $scope.ProductForm.$error.push("Product Name is required");
            }

            if (product.ProductPrice <= 0) {
                $scope.ProductForm.$error.push("Product Price must be greater then 0.");
            }

            if (product.ProductDescription.length == 0) {
                $scope.ProductForm.$error.push("Product description is required.");
            }

            if (product.QuantityInStock <= 0) {
                $scope.ProductForm.$error.push("Quantity in Stock can't be negative.");
            }

            if (product.ProductImages.length == 0 && $scope.EditModel.NewImages.length == 0) {
                $scope.ProductForm.$error.push("At least one image is required.");
            }
            else if (product.DefaultImageName.length == 0) {
                $scope.ProductForm.$error.push("Select a default image.");
            }

            if (product.Discount > 0) {
                var discountStartFrom = moment(product.DiscountStartFrom, dateFormatPattern);
                var discountEndOn = moment(product.DiscountEndOn, dateFormatPattern);

                if (product.DiscountStartFrom.length > 0 && !discountStartFrom) {
                    $scope.ProductForm.$error.push("Invalid discount start DateTime.");
                }
                if (product.DiscountEndOn.length > 0 && !discountEndOn) {
                    $scope.ProductForm.$error.push("Invalid discount end DateTime.");
                }

                if ((product.DiscountStartFrom.length > 0 && discountStartFrom) && (product.DiscountEndOn.length > 0 && discountEndOn)) {
                    if (discountStartFrom.isSame(discountEndOn, "minutes")) {
                        $scope.ProductForm.$error.push("Discount End DateTime and Start DateTime can't be the same.");
                    }

                    if (discountStartFrom.isAfter(discountEndOn, "minutes")) {
                        $scope.ProductForm.$error.push("Discount End DateTime should be after Discount Start DateTime.");
                    }
                }
            }

            return $scope.ProductForm.$error.length == 0;
        };

        function GetShops() {
            return shopService.getAllShopForCurrentUser().then(
                function (successResponse) {
                    $scope.Shops.AllShops = successResponse["data"]["Success"];
                    if ($scope.Shops.AllShops.length > 0)
                        $scope.Shops.SelectedShopForSort = $scope.Shops.AllShops[0].Key.toString();

                    return $scope.LoadData();
                },
                function (errorResponse) {
                    console.log(errorResponse);
                    $scope.Processing = false;
                });
        };

        function ClearEditModel() {
            $scope.EditModel = {};

            if (!$scope.Categories) {
                $scope.Categories = {};
                $scope.Categories.AllCategories = [];
            }
            $scope.Categories.SelectedProductCategory = "";

            if (!$scope.Shops) {
                $scope.Shops = {};
                $scope.Shops.AllShops = [];
                $scope.Shops.SelectedShopForSort = "";
            }
            $scope.Shops.SelectedShop = "";

            $scope.EditModel.EditingProduct = {};
            $scope.EditModel.EditingProduct.ProductId = "";
            $scope.EditModel.EditingProduct.ProductName = "";
            $scope.EditModel.EditingProduct.ProductPrice = "";
            $scope.EditModel.EditingProduct.ProductDescription = "";
            $scope.EditModel.EditingProduct.QuantityInStock = "";
            $scope.EditModel.EditingProduct.DefaultImageName = "";
            $scope.EditModel.EditingProduct.Discount = 0.0;
            $scope.EditModel.EditingProduct.DiscountStartFrom = moment().format(dateFormatPattern);
            $scope.EditModel.EditingProduct.DiscountEndOn = moment().format(dateFormatPattern);
            $scope.EditModel.EditingProduct.ProductImages = [];
            $scope.EditModel.DeletedImages = [];
            $scope.EditModel.NewImages = [];
        };

        function IsDafaultImageExists() {
            if ($scope.EditModel.EditingProduct.ProductImages.length == 0 && $scope.EditModel.NewImages.length == 0)
                return false;

            var defaultImageExists = $scope.EditModel.EditingProduct.ProductImages.indexOf($scope.EditModel.EditingProduct.DefaultImageName) > 0;

            if (!defaultImageExists) {
                $.each($scope.EditModel.NewImages, function (index, image) {
                    if (image.ImageName == $scope.EditModel.EditingProduct.DefaultImageName) {
                        defaultImageExists = true;
                        return;
                    }
                });
            }

            return defaultImageExists;
        };

        ClearEditModel();
        var dateFormatPattern = "MM/DD/YYYY h:mm a";
        $scope.LoadData();
    };

    ProductController.$inject = ['$scope', "$filter", "NgTableParams", 'ProductService', 'ShopService'];
    Cazon.controller("ProductController", ProductController);
})();
