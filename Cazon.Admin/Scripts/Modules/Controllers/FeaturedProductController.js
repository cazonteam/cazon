﻿/// <reference path="../../Angular/angular.js" />
/// <reference path="../../JQuery/jquery-3.2.1.js" />

(function () {
    var Cazon = angular.module("cazon");

    var FeaturedProductController = function ($scope, $filter, NgTableParams, featuredProductService) {

        $scope.Processing = true;
        $scope.IsEditing = false;
        $scope.IsAdding = false;

        $scope.LoadData = function () {
            $scope.Processing = true;
            $scope.IsEditing = false;
            $scope.IsAdding = false;
            $scope.FeaturedProducts = [];

            $scope.TableParams = new NgTableParams({
                count: 15
            }, {
                getData: function (params) {
                    $scope.Processing = true;

                    if (!$scope.FeaturedProducts || $scope.FeaturedProducts.length == 0) {
                        return featuredProductService.getAllFeaturedProducts()
                            .then(function (successResponse) {
                                if (successResponse["data"]["Success"]) {
                                    $scope.FeaturedProducts = successResponse["data"]["Success"];
                                }

                                $scope.Processing = false;

                                return getDataForBinding();
                            },
                            function (error) {
                                console.log(error);
                                $scope.Processing = false;
                            });
                    }
                    else {
                        $scope.Processing = false;

                        return getDataForBinding();
                    }

                    function getDataForBinding() {
                        params.total($scope.FeaturedProducts.length);

                        var orderedData = params.sorting() ? $filter('orderBy')($scope.FeaturedProducts, params.orderBy()) : $scope.FeaturedProducts;

                        var filter = {};
                        $.each(Object.keys(params.filter()), function (index, elem) {
                            if (params.filter()[elem]) {
                                var keys = elem.split('.');
                                filter[keys[0]] = {};
                                if (keys[1])
                                    filter[keys[0]][keys[1]] = params.filter()[elem];
                                else
                                    filter[keys[0]] = params.filter()[elem];
                            }
                        });

                        orderedData = params.filter() ? $filter('filter')(orderedData, filter) : orderedData;

                        return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    };
                }
            });
        };

        $scope.CreateNew = function () {
            $scope.Processing = true;
            $scope.IsAdding = true;
            $scope.IsEditing = false;
            ClearEditModel();
            $scope.Processing = false;
        };

        $scope.EditFeaturedProduct = function (featuredProductId) {
            $scope.Processing = true;
            $scope.IsEditing = true;
            $scope.IsAdding = false;
            ClearEditModel();
            $.each($scope.FeaturedProducts, function (index, product) {
                if (product.Id == featuredProductId) {
                    $scope.EditModel.EditingFeaturedProduct = angular.copy(product);
                    $scope.EditModel.EditingFeaturedProduct.StartFrom = $scope.FormatDate($scope.EditModel.EditingFeaturedProduct.StartFrom);
                    $scope.EditModel.EditingFeaturedProduct.EndOn = $scope.FormatDate($scope.EditModel.EditingFeaturedProduct.EndOn);
                    return;
                }
            });

            $scope.Processing = false;
        };

        $scope.RemoveFromFeaturedProduct = function (featuredProductId, index) {
            $scope.Processing = true;
            if (confirm("do you want to delete the product from the featured product list?")) {
                return featuredProductService.deleteFeaturedProduct(featuredProductId).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            $scope.FeaturedProducts.splice(index, 1);
                            $scope.TableParams.reload();
                        }
                        else {
                            ShowErrorMessages(successResponse['data']);
                        }
                        $scope.Processing = false;
                    },
                    function (errorResponse) {
                        console.log(errorResponse);
                        $scope.Processing = false;
                    });
            }

            $scope.Processing = false;
        };

        $scope.SaveChanges = function () {
            $scope.Processing = true;

            if (!ValidateProduct()) {
                $scope.Processing = false;
                return;
            }

            var featuredProduct = angular.copy($scope.EditModel.EditingFeaturedProduct);

            if ($scope.IsAdding) {
                return featuredProductService.addFeaturedProduct($scope.EditModel.EditingFeaturedProduct)
                    .then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            featuredProduct.Id = successResponse["data"]["Success"][0];
                            $scope.FeaturedProducts.push(featuredProduct);
                            $scope.TableParams.reload();
                        }
                        else {
                            ShowErrorMessages(successResponse['data']);
                        }
                        $scope.Processing = false;
                    },
                    function (errorResponse) {
                        console.log(errorResponse);
                        $scope.Processing = false;
                    });
            }
            else if ($scope.IsEditing) {
                return featuredProductService.updateFeaturedProduct($scope.EditModel.EditingFeaturedProduct)
                    .then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            $.each($scope.FeaturedProducts, function (index, product) {
                                if (product.Id == featuredProduct.Id) {
                                    product.StartFrom = featuredProduct.StartFrom;
                                    product.EndOn = featuredProduct.EndOn;
                                    return;
                                }
                            });
                            $scope.TableParams.reload();
                        }
                        else {
                            ShowErrorMessages(successResponse['data']);
                        }
                        $scope.Processing = false;
                    },
                    function (errorResponse) {
                        console.log(errorResponse);
                        $scope.Processing = false;
                    });
            }
        };

        $scope.CancelChanges = function () {
            $scope.Processing = true;
            ClearEditModel();
            $scope.IsAdding = false;
            $scope.IsEditing = false;
            $scope.Processing = false;
        };

        $scope.FormatDate = function (date) {
            return moment(date).format(dateFormatPattern);
        };

        $scope.SetProduct = function (elem) {
            if (elem.value)
                $scope.EditModel.EditingFeaturedProduct.ProductId = elem.value;
            if (elem.selectedOptions[0])
                $scope.EditModel.EditingFeaturedProduct.ProductName = elem.selectedOptions[0].text;
        };

        function ValidateProduct() {
            $scope.FeaturedProductForm.$error = [];

            if ($scope.IsEditing && $scope.EditModel.EditingFeaturedProduct.Id <= 0) {
                $scope.FeaturedProductForm.$error.push("Invalid featured product to edit.");
            }

            if ($scope.EditModel.EditingFeaturedProduct.ProductId <= 0) {
                $scope.FeaturedProductForm.$error.push("Please, select a product.");
            }

            if ($scope.IsAdding) {
                $.each($scope.FeaturedProducts, function (index, product) {
                    if (product.ProductId == $scope.EditModel.EditingFeaturedProduct.ProductId) {
                        $scope.FeaturedProductForm.$error.push("This product is already in the featured product list.");
                        return;
                    }
                });
            }

            var startFrom = moment($scope.EditModel.EditingFeaturedProduct.StartFrom, dateFormatPattern),
                endOn = moment($scope.EditModel.EditingFeaturedProduct.EndOn, dateFormatPattern);

            if (startFrom.isSame(endOn, "minutes")) {
                $scope.FeaturedProductForm.$error.push("End Date Time and Start Date Time can't be the same.");
            }

            if (startFrom.isAfter(endOn, "minutes")) {
                $scope.FeaturedProductForm.$error.push("End Date Time should be after Start Date Time.");
            }

            return $scope.FeaturedProductForm.$error.length == 0;
        };

        function ClearEditModel() {
            $scope.EditModel = {};
            $scope.EditModel.EditingFeaturedProduct = {};

            $scope.EditModel.EditingFeaturedProduct.ProductId = 0;
            $scope.EditModel.EditingFeaturedProduct.ProductName = "";
            $scope.EditModel.EditingFeaturedProduct.StartFrom = moment().format(dateFormatPattern);
            $scope.EditModel.EditingFeaturedProduct.EndOn = moment().add(7, 'days').format(dateFormatPattern);

            if ($scope.FeaturedProductForm) {
                $scope.FeaturedProductForm.$error = [];
            }
        };

        function ShowErrorMessages(data) {
            $scope.FeaturedProductForm.$error = [];
            if (!data) return;

            var key = "";

            if (data["ValidationException"]) {
                key = "ValidationException";
            }
            else if (data["InvalidModelState"]) {
                key = "InvalidModelState";
            }
            else if (data["Error"]) {
                key = "Error";
            }
            else if (data["Exception"]) {
                $scope.FeaturedProductForm.$error.push("Sorry, an error occured while saving your data...");
            }

            if (key != "") {
                $.each(data[key], function (index, error) {
                    $scope.FeaturedProductForm.$error.push(error);
                });
            }
        };

        ClearEditModel();
        var dateFormatPattern = "MM/DD/YYYY h:mm a";
        $scope.LoadData();
    };

    FeaturedProductController.$inject = ['$scope', "$filter", "NgTableParams", 'FeaturedProductService'];
    Cazon.controller("FeaturedProductController", FeaturedProductController);
})();
