﻿/// <reference path="../../Angular/angular.js" />
/// <reference path="../../JQuery/jquery-3.2.1.js" />

(function () {
    var Cazon = angular.module("cazon");

    var ProductCategoryController = function ($scope, $filter, NgTableParams, productCategoryService) {
        $scope.Processing = true;
        $scope.IsEditing = false;
        $scope.IsAdding = false;

        $scope.LoadData = function () {
            $scope.ProductCategories = [];

            $scope.TableParams = new NgTableParams({
                count: 15
            }, {
                getData: function (params) {
                    $scope.Processing = true;

                    if (!$scope.ProductCategories || $scope.ProductCategories.length == 0) {
                        return productCategoryService.getAll()
                            .then(function (categories) {
                                $scope.ProductCategories = categories["data"]["Success"];
                                $scope.Processing = false;

                                return getDataForBinding();
                            },
                            function (error) {
                                console.log(error);
                                $scope.Processing = false;
                            });
                    }
                    else {
                        $scope.Processing = false;

                        return getDataForBinding();
                    }

                    function getDataForBinding() {
                        params.total($scope.ProductCategories.length);

                        var orderedData = params.sorting() ? $filter('orderBy')($scope.ProductCategories, params.orderBy()) : $scope.ProductCategories;
                        orderedData = params.filter() ? $filter('filter')(orderedData, params.filter()) : orderedData;

                        return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    }
                }
            });
        };

        $scope.CreateNew = function () {
            $scope.IsAdding = true;
            $scope.IsEditing = false;
            ClearEditModel();
            if ($scope.ProductCategories[0].CategoryId != 0)
                $scope.ProductCategories.splice(0, 0, EmptyCategory);
            $scope.EditModel.SelectedParentCategory = { "CategoryId": 0 };
            $scope.Processing = true;
        };

        $scope.Edit = function (categoryId) {
            $scope.IsEditing = true;
            ClearEditModel();

            $.each($scope.ProductCategories, function (index, elem) {
                if (elem.CategoryId == categoryId) {
                    $scope.EditModel.EditingCategory = angular.copy(elem);
                    $scope.EditModel.SelectedParentCategory = { "CategoryId": elem.ParentCategoryId };
                    return;
                }
            });

            if ($scope.ProductCategories[0].CategoryId != 0)
                $scope.ProductCategories.splice(0, 0, EmptyCategory);

            if(!$scope.EditModel.SelectedParentCategory.CategoryId)
                $scope.EditModel.SelectedParentCategory = { "CategoryId": 0 };
        };

        $scope.Delete = function (categoryId) {
            $scope.Processing = true;

            if (confirm("Do you really want to delete the category? this category and all of it's child categories will be deleted...")) {
                productCategoryService.delete(categoryId).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.ProductCategories.length; index++) {
                                if ($scope.ProductCategories[index].CategoryId == categoryId) {
                                    $scope.ProductCategories.splice(index, 1);
                                }
                                if ($scope.ProductCategories[index].ParentCategoryId == categoryId) {
                                    $scope.ProductCategories[index].ParentCategoryId = null;
                                }
                            }
                            $scope.TableParams.reload();
                        }
                    },
                    function (errorResponse) {
                        console.log(errorResponse);
                    });
            }

            $scope.Processing = false;
        };

        $scope.SaveCategory = function () {
            if (!ValidateCategory($scope.EditModel.EditingCategory)) {
                $scope.editProductCategoryForm.$valid = false;
                return;
            }

            var category = angular.copy($scope.EditModel.EditingCategory);
            if ($scope.EditModel.SelectedParentCategory && $scope.EditModel.SelectedParentCategory.CategoryId > 0) {
                category.ParentCategoryId = $scope.EditModel.SelectedParentCategory.CategoryId;
            }

            if ($scope.IsEditing) {
                productCategoryService.update(category).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.ProductCategories.length; index++) {
                                if ($scope.ProductCategories[index].CategoryId == category.CategoryId) {
                                    $scope.ProductCategories[index] = category;
                                    $scope.TableParams.reload();
                                    break;
                                }
                            }
                        }
                    },
                    function (error) {
                        console.log(error);
                    });
            }
            else if ($scope.IsAdding) {
                productCategoryService.create(category).then(
                    function (successResponse) {
                        category.CategoryId = successResponse["data"]["Success"][0].CategoryId;
                        $scope.ProductCategories.push(category);
                        $scope.TableParams.reload();
                    },
                    function (error) {
                        console.log(error);
                    });
            }
        };

        $scope.CancelChanges = function () {
            ClearEditModel();
            //$scope.ProductCategories.push(EmptyCategory);
            $scope.Processing = false;
        };

        function ValidateCategory(category) {
            $scope.editProductCategoryForm.$error = [];

            if (category.CategoryName.length == 0) {
                $scope.editProductCategoryForm.$error.push("Category name is required...");
            }
            else if (!(/^(?:[a-z ]+)(?:[ a-z0-9]*)$/ig.test(category.CategoryName))) {
                $scope.editProductCategoryForm.$error.push("Only number, letters and spaces are allowed in the category name...");
            }

            return $scope.editProductCategoryForm.$error.length == 0;
        };

        function ClearEditModel() {
            $scope.EditModel = {};

            $scope.EditModel.SelectedParentCategory = {};
            $scope.EditModel.SelectedParentCategory.CategoryId = "";

            $scope.EditModel.EditingCategory = {};
            $scope.EditModel.EditingCategory.CategoryName = "";
            $scope.EditModel.EditingCategory.IconName = "";
            $scope.EditModel.EditingCategory.OrderNumber = 0;
            $scope.EditModel.EditingCategory.ParentCategoryId = 0;
        };

        EmptyCategory = { "CategoryId": 0, "CategoryName": "Select the Parent Category." };

        ClearEditModel();
        $scope.LoadData();
    };

    ProductCategoryController.$inject = ['$scope', "$filter", "NgTableParams", 'ProductCategoryService'];
    Cazon.controller("ProductCategoryController", ProductCategoryController);
})();
