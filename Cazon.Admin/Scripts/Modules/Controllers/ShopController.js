﻿/// <reference path="../../Angular/angular.js" />
/// <reference path="../../JQuery/jquery-3.2.1.js" />

(function () {
    var Cazon = angular.module("cazon");

    var ShopController = function ($scope, $filter, NgTableParams, shopService) {

        $scope.Processing = true;
        $scope.IsEditing = false;
        $scope.IsAdding = false;

        $scope.LoadData = function () {
            $scope.Processing = true;
            $scope.IsEditing = false;
            $scope.IsAdding = false;
            $scope.Shops = [];

            $scope.TableParams = new NgTableParams({
                count: 15
            }, {
                getData: function (params) {
                    $scope.Processing = true;

                    if (!$scope.Shops || $scope.Shops.length == 0) {
                        return shopService.getAllShops()
                            .then(function (result) {
                                $scope.Shops = result["data"]["Success"]["Shops"];
                                $scope.City.AllCities = result["data"]["Success"]["Cities"];

                                $.each($scope.Shops, function (shopIndex, shop) {
                                    $.each($scope.City.AllCities, function (cityIndex, city) {
                                        if (city.Key == shop.CityId) {
                                            shop.CityName = city.Value;
                                            return;
                                        }
                                    });
                                });

                                $scope.Processing = false;

                                return getDataForBinding();
                            },
                            function (error) {
                                console.log(error);
                                $scope.Processing = false;
                            });
                    }
                    else {
                        $scope.Processing = false;

                        return getDataForBinding();
                    }

                    function getDataForBinding() {
                        params.total($scope.Shops.length);

                        var orderedData = params.sorting() ? $filter('orderBy')($scope.Shops, params.orderBy()) : $scope.Shops;

                        var filter = {};
                        $.each(Object.keys(params.filter()), function (index, elem) {
                            if (params.filter()[elem]) {
                                var keys = elem.split('.');
                                filter[keys[0]] = {};
                                if (keys[1])
                                    filter[keys[0]][keys[1]] = params.filter()[elem];
                                else
                                    filter[keys[0]] = params.filter()[elem];
                            }
                        });

                        orderedData = params.filter() ? $filter('filter')(orderedData, filter) : orderedData;

                        return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    }
                }
            });
        };

        $scope.CreateNew = function () {
            $scope.Processing = true;
            $scope.IsAdding = true;
            $scope.IsEditing = false;
            ClearEditModel();
            $scope.EditModel.Shop.CityId = $scope.City.AllCities[0].Key.toString();
            $scope.Processing = false;
        };

        $scope.EditShop = function (shopId) {
            $scope.Processing = true;
            $scope.IsEditing = true;
            ClearEditModel();

            $.each($scope.Shops, function (index, shop) {
                if (shop.ShopId == shopId) {
                    $scope.EditModel.Shop = shop;
                    $scope.EditModel.Shop.CityId = shop.CityId.toString();
                    $scope.EditModel.Shop.RegisteredOn = $scope.ParseDate(shop.RegisteredOn);
                    return;
                }
            });

            $scope.Processing = false;
        };

        $scope.DeleteShop = function (shopId) {
            $scope.Processing = true;
            $scope.ShopForm.$error = [];

            if (confirm("Do you really want to delete the shop? This shop and all of its products will be deleted. You can't undo once you deleted the shop...")) {
                shopService.delete(shopId).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.Shops.length; index++) {
                                if ($scope.Shops[index].ShopId == shopId) {
                                    $scope.Shops.splice(index, 1);
                                }
                            }
                            $scope.TableParams.reload();
                        }
                        else {
                            ShowErrorMessages(successResponse["data"]);
                        }

                        $scope.Processing = false;
                    },
                    function (errorResponse) {
                        console.log(errorResponse);

                        $scope.Processing = false;
                    });
            }
        };

        $scope.SaveChanges = function () {
            $scope.Processing = true;
            if (!ValidateShop($scope.EditModel.Shop)) {
                $scope.Processing = false;
                return;
            }

            var shop = angular.copy($scope.EditModel.Shop);

            if ($scope.IsEditing) {
                shopService.update(shop).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            for (var index = 0; index < $scope.Shops.length; index++) {
                                if ($scope.Shops[index].ShopId == shop.ShopId) {
                                    shop.CityName = GetCityName(shop.CityId);
                                    $scope.Shops[index] = shop;
                                    $scope.TableParams.reload();
                                    break;
                                }
                            }
                        }
                        else {
                            ShowErrorMessages(successResponse["data"]);
                        }
                        $scope.Processing = false;
                    },
                    function (error) {
                        console.log(error);
                        $scope.Processing = false;
                    });
            }
            else if ($scope.IsAdding) {
                shopService.create(shop).then(
                    function (successResponse) {
                        if (successResponse["data"]["Success"]) {
                            shop.ShopId = successResponse["data"]["Success"][0];
                            shop.CityName = GetCityName(shop.CityId);
                            $scope.Shops.push(shop);
                            $scope.TableParams.reload();
                        }
                        else {
                            ShowErrorMessages(successResponse["data"]);
                        }

                        $scope.Processing = false;
                    },
                    function (error) {
                        console.log(error);
                        $scope.Processing = false;
                    });
            }
        };

        $scope.CancelChanges = function () {
            $scope.IsAdding = false;
            $scope.IsEditing = false;
            ClearEditModel();
            $scope.Processing = false;
        };

        $scope.ParseDate = function (dateToParse) {
            var parsedDate = Date.parse(dateToParse);

            if (!parsedDate) parsedDate = dateToParse.replace(/\/Date\((\d+)\)\//, '$1');

            return (new Date(+parsedDate)).toString();
        };

        function ValidateShop(shop) {
            $scope.ShopForm.$error = [];

            if ($scope.EditModel.Shop.ShopName == "") {
                $scope.ShopForm.$error.push("Shop Name can't be empty.");
            }

            if ($scope.EditModel.Shop.ShopPhoneNumber == "") {
                $scope.ShopForm.$error.push("Provide a valid shop phone number.");
            }

            if ($scope.EditModel.Shop.ShopDescription == "") {
                $scope.ShopForm.$error.push("Shop description can't be empty.");
            }

            return $scope.ShopForm.$error.length == 0;
        };

        function ShowErrorMessages(data) {
            $scope.ShopForm.$error = [];
            if (!data) return;

            var key = "";

            if (data["ValidationException"]) {
                key = "ValidationException";
            }
            else if (data["InvalidModelState"]) {
                key = "InvalidModelState";
            }
            else if (data["Error"]) {
                key = "Error";
            }
            else if (data["Exception"]) {
                $scope.ShopForm.$error.push("Sorry, an error occured while saving your data...");
            }

            if (key != "") {
                $.each(data[key], function (index, error) {
                    $scope.ShopForm.$error.push(error);
                });
            }
        };

        function GetCityName(cityId) {
            var cityName = "";
            $.each($scope.City.AllCities, function (index, city) {
                if (city.Key == cityId) {
                    cityName = city.Value;
                    return;
                }
            });

            return cityName;
        };

        function ClearEditModel() {
            $scope.EditModel = {};

            if ($scope.ShopForm)
                $scope.ShopForm.$error = [];

            if (!$scope.City) {
                $scope.City = {};
            }

            $scope.EditModel.Shop = {};
            $scope.EditModel.Shop.ShopName = "";
            $scope.EditModel.Shop.RegisteredOn = "";
            $scope.EditModel.Shop.ShopPhoneNumber = "";
            $scope.EditModel.Shop.ShopDescription = "";
            $scope.EditModel.Shop.CityId = "";
        };

        ClearEditModel();
        $scope.LoadData();
    };

    ShopController.$inject = ['$scope', "$filter", "NgTableParams", 'ShopService'];
    Cazon.controller("ShopController", ShopController);
})();
