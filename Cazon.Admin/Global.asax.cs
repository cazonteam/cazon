﻿using Cazon.Admin.Model;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Cazon.Admin
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            foreach (var headerToRemove in CazonConstants.HEADERS_TO_REMOVE.Split(';'))
                HttpContext.Current.Response.Headers.Remove(headerToRemove);
        }
    }
}
