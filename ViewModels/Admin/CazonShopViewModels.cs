﻿using Cazon.ViewModel.Common;
using System;

namespace Cazon.ViewModel.Admin
{
    // This DTO is used by other classes to render shops in selects, editors etc.
    public class GetShopDetailsForSelectDTO
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
    }

    public class GetShopBasicDetailsDTO
    {
        public int ShopId { get; set; }

        public string ShopName { get; set; }

        public string ShopPhoneNumber { get; set; }

        public string ShopDescription { get; set; }

        public DateTime RegisteredOn { get; set; }

        public int CityId { get; set; }
    }

    public class AddShopAdminDTO : AddShopBaseDTO
    {
        public string OwnerId { get; set; }
    }

    public class EditShopAdminDTO : EditShopBaseDTO
    {
    }
}
