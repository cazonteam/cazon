﻿using Cazon.ViewModel.Common;
using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Admin
{
    public class GetProductCategoryAdminDTO : GetProductCategorySimpleDTO
    {
        public int? ParentCategoryId { set; get; }
    }

    public class AddProductCategoryDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string CategoryName { get; set; }

        public string IconName { get; set; }

        public int OrderNumber { get; set; }

        public int ParentCategoryId { get; set; }
    }

    public class EditProductCategoryDTO
    {
        [NonZero]
        [Required]
        public int CategoryId { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string CategoryName { get; set; }

        public string IconName { get; set; }

        public int OrderNumber { set; get; }

        public int ParentCategoryId { get; set; }
    }
}
