﻿using Cazon.ViewModel.Common;
using Cazon.ViewModel.Common.Product;
using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System;
using System.Collections.Generic;

namespace Cazon.ViewModel.Admin
{
    public class GetProductDetailsDTO : GetProductsBasicInfoIdDTO
    {
        public DateTime CreatedOn { get; set; }

        public string ProductDescription { get; set; }

        public string DefaultImageName { set; get; }

        public double? Discount { get; set; }

        public DateTime? DiscountStartFrom { get; set; }

        public DateTime? DiscountEndOn { get; set; }

        public List<string> ProductImages { get; set; }

        public int ShopId { get; set; }
    }

    public class GetProductsBasicInfoIdDTO
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public int QuantityInStock { set; get; }

        public int ProductCategoryId { get; set; }
    }

    public class UpdateProductAdminDTO : UpdateProductBaseDTO
    {
        public UpdateProductAdminDTO()
        {
            UploadedFiles = new List<CazonHttpPostedFileBase>();
        }

        public List<CazonHttpPostedFileBase> UploadedFiles { set; get; }
    }

    public class AddProductAdminDTO : AddProductBaseDTO
    {
        public AddProductAdminDTO()
        {
            UploadedFiles = new List<CazonHttpPostedFileBase>();
        }

        [MinLengthCollection(MinLength = 1)]
        public List<CazonHttpPostedFileBase> UploadedFiles { set; get; }

    }
}
