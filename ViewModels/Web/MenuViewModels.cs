﻿namespace Cazon.ViewModel.Web
{
    public class MainMenuCategoryDTO
    {
        public int Id { get; set; }

        public int OrderNumber { set; get; }

        public string CategoryName { get; set; }

        public string IconName { get; set; }
    }
}
