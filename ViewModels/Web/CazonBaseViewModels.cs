﻿using Cazon.ViewModel.Web.Products;
using System.Collections.Generic;

namespace Cazon.ViewModel.Web
{
    public class CazonDefaultDTO : IGetFeaturedProducts
    {
        public CazonDefaultDTO()
        {
            CategoryFeatureProducts = new List<GetFeaturedProductsByCategoryDTO>();
        }

        public string[] FeaturedProductsCategories { set; get; }

        public List<GetFeaturedProductsByCategoryDTO> CategoryFeatureProducts { get; set; }
    }
}
