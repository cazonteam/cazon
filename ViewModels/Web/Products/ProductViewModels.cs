﻿using Cazon.ViewModel.Common.Product;
using System.Collections.Generic;
using Cazon.ViewModel.Common.Enums;
using System;

namespace Cazon.ViewModel.Web.Products
{
    public abstract class ProductBaseDTO
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public string DefaultImageName { get; set; }

        public double Discount { get; set; }

        public double DiscountedPrice
        {
            get
            {
                if (Discount == 0.0)
                    return ProductPrice;

                var discountPercent = (ProductPrice * Discount) / 100.0;
                return ProductPrice - discountPercent;
            }
        }

        public string ProductUrl
        {
            get
            {
                return $"{ProductName.Replace(" ", "-")}-{ProductId}";
            }
        }
    }

    public class ViewCategoryProductsDTO : IGetFeaturedProducts
    {
        public ViewCategoryProductsDTO()
        {
            CategoryFeatureProducts = new List<GetFeaturedProductsByCategoryDTO>();
            Products = new List<ProductByCategoryNameDTO>();
        }

        public string CategoryName { get; set; }

        public List<ProductByCategoryNameDTO> Products { get; set; }

        public string[] FeaturedProductsCategories { set; get; }

        public List<GetFeaturedProductsByCategoryDTO> CategoryFeatureProducts { set; get; }
    }

    public class ProductByCategoryNameDTO : ProductBaseDTO
    {
    }

    public class ViewProductDetailsDTO : ProductBaseDTO, IGetFeaturedProducts
    {
        public ViewProductDetailsDTO()
        {
            CategoryFeatureProducts = new List<GetFeaturedProductsByCategoryDTO>();
        }

        public string Description { get; set; }

        public int ShopId { get; set; }
        public string ShopName { get; set; }

        public double Rating { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductCategoryName { get; set; }

        public DateTime? DiscountEndDate { get; set; }

        public List<string> ProductImages { get; set; }

        public string[] FeaturedProductsCategories { set; get; }

        public List<GetFeaturedProductsByCategoryDTO> CategoryFeatureProducts { get; set; }
    }

    public class FindProductByCategoryDTO : IProductSearchFilters
    {
        public string CategoryName { get; set; }

        public string Color { get; set; }

        public string Discount { get; set; }

        public Tuple<double, double> DiscountRanges
        {
            get
            {
                Tuple<double, double> discountRange = null;

                if (!string.IsNullOrEmpty(Discount))
                {
                    var range = Discount.Split('-');
                    if (range.Length == 2)
                    {
                        discountRange = new Tuple<double, double>(double.Parse(range[0]), double.Parse(range[1]));
                    }
                    else if (range.Length == 1)
                    {
                        discountRange = new Tuple<double, double>(0.0, double.Parse(range[0]));
                    }
                }
                return discountRange;
            }
        }

        public string PriceRange { get; set; }

        public Tuple<double, double> PriceRanges
        {
            get
            {
                Tuple<double, double> priceRanges = null;

                if (!string.IsNullOrEmpty(PriceRange))
                {
                    var range = PriceRange.Split('-');
                    if (range.Length == 2)
                    {
                        priceRanges = new Tuple<double, double>(double.Parse(range[0]), double.Parse(range[1]));
                    }
                    else if (range.Length == 1)
                    {
                        priceRanges = new Tuple<double, double>(0.0, double.Parse(range[0]));
                    }
                }

                return priceRanges;
            }
        }

        public ProductSortBy? SortBy { get; set; }
    }
}
