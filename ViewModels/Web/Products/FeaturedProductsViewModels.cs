﻿using System.Collections.Generic;

namespace Cazon.ViewModel.Web.Products
{
    public class GetFeaturedProductsDTO : ProductBaseDTO
    {
        public int FeatureProductId { get; set; }

        public string Description { get; set; }
    }

    public class GetFeaturedProductsByCategoryDTO
    {
        public GetFeaturedProductsByCategoryDTO()
        {
            FeaturedProducts = new List<GetFeaturedProductsDTO>();
        }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string CategoryIconName { get; set; }

        public List<GetFeaturedProductsDTO> FeaturedProducts { get; set; }
    }

    public interface IGetFeaturedProducts
    {
        string[] FeaturedProductsCategories { set; get; }

        List<GetFeaturedProductsByCategoryDTO> CategoryFeatureProducts { set; get; }
    }
}
