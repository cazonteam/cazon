﻿using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.API
{
    public class GetCityDTO
    {
        public int CityId { get; set; }
        public string CityFullName { get; set; }
        public string CityShortName { get; set; }
        public int CityPostalCode { get; set; }
    }

    public class AddCityDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string CityFullName { get; set; }

        [StringLength(3, ErrorMessage = "The {0} must be equal to {2} characters long.", MinimumLength = 3)]
        [Required(AllowEmptyStrings = false)]
        public string CityShortName { get; set; }

        [NonZero]
        [Required(ErrorMessage = "Postal Code Is Required.")]
        public int CityPostalCode { get; set; }

    }

    public class UpdateCityDTO
    {
        [NonZero]
        [Required(ErrorMessage = "The CityId Is Required...")]
        public int CityId { get; set; }

        public string CityFullName { get; set; }
        
        public string CityShortName { get; set; }

        public int CityPostalCode { get; set; }
    }
}