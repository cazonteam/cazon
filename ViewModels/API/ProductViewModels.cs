﻿using Cazon.ViewModel.Common;
using Cazon.ViewModel.Common.Enums;
using Cazon.ViewModel.Common.Product;
using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System;
using System.Collections.Generic;

namespace Cazon.ViewModel.API
{
    public abstract class ProductBaseDTO
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ProductDescription { get; set; }

        public int QuantityInStock { get; set; }

        public ProductCategoryForProductDTO ProductCategory { set; get; }
    }

    public class GetProductDTO : GetShopProductsDTO
    {
        public List<string> ProductImages { get; set; }
        public CazonShopForProductDTO Shop { get; set; }
    }

    public class GetShopProductsDTO : ProductBaseDTO
    {
        public string DefaultImageName { get; set; }

        public ProductCategoryForProductDTO[] CategoryParentCategories { set; get; }
    }

    public class GetProductsByCategoryIdDTO : GetShopProductsDTO
    {
        public int ShopId { set; get; }
        public string ShopName { set; get; }
    }

    public class AddProductApiDTO : AddProductBaseDTO, IUploadFile
    {
        [MinLengthCollection(MinLength = 1, ErrorMessage = "Product must have at lest one image.")]
        public List<UploadFileModel> UploadedFiles { set; get; }
    }

    public class UpdateProductAPIDTO : UpdateProductBaseDTO, IUploadFile
    {
        public UpdateProductAPIDTO()
        {
        }

        public List<UploadFileModel> UploadedFiles { set; get; }
    }

    public class ProductCategoryForProductDTO
    {
        public int ProductCategoryId { get; set; }

        public string CategoryName { get; set; }
    }

    public class CazonShopForProductDTO
    {
        public int ShopId { get; set; }

        public string ShopName { set; get; }

        public string Description { get; set; }

        public DateTime RegisteredOn { set; get; }

        public string PhoneNumber { get; set; }
        
        public virtual AddressForProductDTO Address { get; set; }
    }

    public class AddressForProductDTO
    {
        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string CityFullName { get; set; }
        public string CityShortName { get; set; }
        public int CityPostalCode { get; set; }
    }

    public class FindProductDTO : ProductBaseDTO
    {
        public string ShopName { get; set; }

        public string DefaultImageName { get; set; }

        public List<string> ProductImages { get; set; }
    }

    public class FindProductModel
    {
        public string SearchText { get; set; }
        public ProductSortBy? SortBy { get; set; }
        public int? NoOfRecords { get; set; }
        public int? PageNumber { get; set; }
    }
}
