﻿using Cazon.ViewModel.Common;
using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.API
{
    public class GetProductImageDTO
    {
        public int ImageId { get; set; }
        public int ProductId { get; set; }

        public bool IsDefault { get; set; }

        public string ImageName { get; set; }
    }

    public class AddProductImageDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string ImageName { get; set; }

        public bool IsDefault { get; set; }

        public string DefaultImageName { set; get; }

        [Required]
        [NonZero]
        public int ProductId { get; set; }
    }

    public class EditProductImageDTO
    {
        [Required]
        [NonZero]
        public int ImageId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string ImageName { get; set; }

        public bool? IsDefault { get; set; }

        [Required]
        [NonZero]
        public int ProductId { get; set; }
    }

    public class GetProductImageForProductDTO
    {
        public GetProductImageForProductDTO()
        {
            ProductImages = new List<string>();
        }

        public List<string> ProductImages { set; get; }
        public string DefaultImageName { set; get; }
    }

    public class AddProductImageTestDTO : IUploadFile
    {

        public List<UploadFileModel> UploadedFiles { set; get; }
    }
}