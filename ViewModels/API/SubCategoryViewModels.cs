﻿using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.API
{
    public class AddSubCategoryViewModel
    {
        [Required(AllowEmptyStrings = false)]
        public string SubCategoryName { get; set; }

        public string IconName { get; set; }

        [NonZero]
        [Required]
        public int ParentCategoryId { get; set; }
    }

    public class EditSubCategoryViewModel
    {
        [NonZero]
        [Required]
        public int SubCategoryId { get; set; }


        [Required(AllowEmptyStrings = false)]
        public string SubCategoryName { get; set; }

        public string IconName { get; set; }


        [NonZero]
        [Required]
        public int ParentCategoryId { get; set; }
    }

}