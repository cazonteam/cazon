﻿using Cazon.ViewModel.Common;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.API
{
    public class GetShopDTO
    {
        public int ShopId { get; set; }

        public string ShopName { set; get; }

        public string ShopDescription { get; set; }
        public string RegisteredOn { get; set; }

        public string ShopPhoneNumber { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string CityName { get; set; }
    }

    public class AddShopApiDTO : AddShopBaseDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string Latitude { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Longitude { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string OwnerId { get; set; }
    }

    public class EditShopApiDTO : EditShopBaseDTO
    {
        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}