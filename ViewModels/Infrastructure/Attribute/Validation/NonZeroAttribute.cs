﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Infrastructure.Attribute.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple =false)]
    public class NonZeroAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            try
            {
                if(value.GetType() == typeof(int))
                {
                    var obj = (int)value;
                    return obj > 0;
                }
                else if (value.GetType() == typeof(double))
                {
                    var obj = (double)value;
                    return obj > 0.0;
                }
                else if (value.GetType() == typeof(float))
                {
                    var obj = (float)value;
                    return obj > 0.0f;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.IsNullOrEmpty(ErrorMessage) ? string.Format("The {0} must be greater then 0.", name) : string.Format(ErrorMessage, name);
        }
    }
}