﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Infrastructure.Attribute.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class MinLengthCollectionAttribute : ValidationAttribute
    {
        public int MinLength { get; set; }

        public override bool IsValid(object value)
        {
            try
            {
                var asICollection = value as ICollection;
                if (asICollection != null)
                    return asICollection.Count >= MinLength;
            }
            catch
            {
                throw;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.IsNullOrEmpty(ErrorMessage) ? string.Format("The {0} must have atleast one item.", name) : string.Format(ErrorMessage, name);
        }
    }
}