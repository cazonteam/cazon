﻿using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Common
{
    public class AddShopBaseDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string ShopName { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string ShopDescription { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string ShopPhoneNumber { get; set; }

        [Required]
        [NonZero]
        public int CityId { get; set; }
    }

    public class EditShopBaseDTO
    {
        [Required]
        [NonZero]
        public int ShopId { get; set; }

        public string ShopName { set; get; }

        public string ShopDescription { get; set; }

        public string ShopPhoneNumber { get; set; }

        public int CityId { get; set; }
    }

    public class DeleteShopDTO
    {
        [Required]
        [NonZero]
        public int ShopId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string OwnerId { get; set; }
    }
}
