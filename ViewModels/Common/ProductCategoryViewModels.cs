﻿using System.Collections.Generic;

namespace Cazon.ViewModel.Common
{
    public class GetProductCategorySimpleDTO
    {
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string IconName { get; set; }

        public int OrderNumber { get; set; }
    }

    public class GetCategoryWithSubCategoriesDTO : GetProductCategorySimpleDTO
    {
        public List<GetCategoryWithSubCategoriesDTO> SubCategories { get; set; }
    }
}
