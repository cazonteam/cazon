﻿using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Common.Shopping.Cart
{
    public class AddProductToCartDTO
    {
        [Required]
        [NonZero]
        public int ProductId { get; set; }
    }

    public class DeleteCartItemDTO
    {
        [Required]
        [NonZero]
        public int CartItemId { get; set; }
    }

    public class UpdateCartItemDTO
    {
        [Required]
        [NonZero]
        public int CartItemId { get; set; }

        [Required]
        [NonZero]
        public int Quantity { get; set; }
    }

    public class UpdateCartDTO
    {
        public List<UpdateCartItemDTO> CartItems { get; set; }
    }

    public class ViewCartDTO
    {
        public ViewCartDTO()
        {
            CartItems = new List<ViewCartItemDTO>();
        }

        public int CartId { get; set; }

        public List<ViewCartItemDTO> CartItems { get; set; }

        public double GetCartTotal()
        {
            var cartTotal = 0.0;

            foreach (var item in CartItems)
                cartTotal += item.CartItemPrice;

            return cartTotal;
        }
    }

    public class ViewCartItemDTO
    {
        public int CartItemId { get; set; }

        public int Quantity { get; set; }

        public DateTime AddedOn { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public double CartItemPrice { get; set; }

        public string Description { get; set; }

        public string ProductImageName { get; set; }

        public int QuantityInStock { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductCategoryName { get; set; }

        public int CazonShopId { get; set; }

        public string CazonShopName { get; set; }
    }
}
