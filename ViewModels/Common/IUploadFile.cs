﻿using System.Collections.Generic;

namespace Cazon.ViewModel.Common
{
    public interface IUploadFile
    {
        List<UploadFileModel> UploadedFiles { set; get; }
    }
}
