﻿using Cazon.ViewModel.Common.Enums;
using Cazon.ViewModel.Common.Product;
using Cazon.ViewModel.Web.Products;
using System;
using System.Collections.Generic;

namespace Cazon.ViewModel.Common.Search
{
    public class SearchProductDTO : IProductSearchFilters
    {
        public string SearchText { get; set; }

        public ProductSortBy? SortBy { get; set; }

        public string Discount { get; set; }

        public Tuple<double, double> DiscountRanges
        {
            get
            {
                Tuple<double, double> discountRange = null;

                if (!string.IsNullOrEmpty(Discount))
                {
                    var range = Discount.Split('-');
                    if (range.Length == 2)
                    {
                        discountRange = new Tuple<double, double>(double.Parse(range[0]), double.Parse(range[1]));
                    }
                    else if (range.Length == 1)
                    {
                        discountRange = new Tuple<double, double>(0.0, double.Parse(range[0]));
                    }
                }

                return discountRange;
            }
        }

        public double DiscountEndOnPrice { get; }

        public string Color { get; set; }

        public string PriceRange { get; set; }

        public Tuple<double, double> PriceRanges
        {
            get
            {
                Tuple<double, double> priceRanges = null;

                if (!string.IsNullOrEmpty(PriceRange))
                {
                    var range = PriceRange.Split('-');
                    if (range.Length == 2)
                    {
                        priceRanges = new Tuple<double, double>(double.Parse(range[0]), double.Parse(range[1]));
                    }
                    else if (range.Length == 1)
                    {
                        priceRanges = new Tuple<double, double>(0.0, double.Parse(range[0]));
                    }
                }

                return priceRanges;
            }
        }
    }

    public class SearchProductResultDTO : IGetFeaturedProducts
    {
        public SearchProductResultDTO()
        {
            CategoryFeatureProducts = new List<GetFeaturedProductsByCategoryDTO>();
            SearchedProducts = new List<GetProductForSearchDTO>();
        }

        public List<GetProductForSearchDTO> SearchedProducts { set; get; }

        public List<GetFeaturedProductsByCategoryDTO> CategoryFeatureProducts { get; set; }

        public string[] FeaturedProductsCategories { get; set; }
    }

    public class GetProductForSearchDTO
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public string DefaultImageName { get; set; }

        public double Discount { get; set; }

        public double DiscountedPrice
        {
            get
            {
                if (Discount == 0.0)
                    return ProductPrice;

                var discountPercent = (ProductPrice * Discount) / 100.0;
                return ProductPrice - discountPercent;
            }
        }

        public DateTime? DiscountEndOn { get; set; }

        public string ProductUrl
        {
            get
            {
                return $"{ProductName.Replace(" ", "-")}-{ProductId}";
            }
        }

        public int ShopId { get; set; }
        public string ShopName { get; set; }

        public double Rating { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductCategoryName { get; set; }
    }
}
