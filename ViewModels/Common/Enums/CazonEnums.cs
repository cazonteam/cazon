﻿namespace Cazon.ViewModel.Common.Enums
{
    public enum ProductSortBy
    {
        NameAsc = 1,
        NameDesc,
        PriceAsc,
        PriceDesc,
        Latest,
        Popular
    }
}
