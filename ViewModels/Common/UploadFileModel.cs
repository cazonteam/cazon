﻿using System;
using System.Web;

namespace Cazon.ViewModel.Common
{
    public class UploadFileModel
    {
        public UploadFileModel()
        {
            UploadedOn = DateTime.Now;
        }

        public string FileName { set; get; }

        public string MimeType { set; get; }

        public string TempUploadPath { set; get; }
        public DateTime UploadedOn { get; set; }

        public byte[] FileData { set; get; }

        // Used by NewtonSoft JSON to ignore the FileData.
        public bool ShouldSerializeFileData()
        {
            return false;
        }
    }

    public class CazonHttpPostedFileBase
    {
        public CazonHttpPostedFileBase()
        {
            UploadedOn = DateTime.Now;
        }

        public DateTime UploadedOn { get; set; }
        public HttpPostedFileBase File { set; get; }
    }
}