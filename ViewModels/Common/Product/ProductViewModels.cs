﻿using Cazon.ViewModel.Common.Enums;
using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Common.Product
{
    public class UpdateProductImageDTO
    {
        public string OldImageName { get; set; }
        public string NewImageName { get; set; }
    }


    public class UpdateProductBaseDTO
    {
        public UpdateProductBaseDTO()
        {
            DeletedImages = new List<string>();
        }

        [NonZero]
        [Required]
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public double ProductPrice { get; set; }

        public string ProductDescription { get; set; }

        public int ProductCategoryId { get; set; }

        public int ShopId { get; set; }

        public string DefaultImageName { set; get; }

        public List<string> DeletedImages { set; get; }

        public double Discount { get; set; }

        public DateTime? DiscountStartFrom { get; set; }

        public DateTime? DiscountEndOn { get; set; }
    }

    public class AddProductBaseDTO
    {
        [Required(AllowEmptyStrings = false)]
        public string ProductName { get; set; }

        [Required]
        [NonZero]
        public double ProductPrice { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string ProductDescription { get; set; }

        [Required]
        [NonZero]
        public int QuantityInStock { set; get; }

        [Required]
        [NonZero]
        public int ProductCategoryId { get; set; }

        [Required]
        [NonZero]
        public int ShopId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string DefaultImageName { set; get; }
    }

    public interface IProductSearchFilters
    {
        ProductSortBy? SortBy { get; set; }

        string Discount { get; set; }

        Tuple<double, double> DiscountRanges { get;}

        string Color { get; set; }

        string PriceRange { get; set; }


        Tuple<double, double> PriceRanges { get; }

    }
}
