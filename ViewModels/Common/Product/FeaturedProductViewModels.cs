﻿using Cazon.ViewModel.Infrastructure.Attribute.Validation;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cazon.ViewModel.Common.Product
{
    public class GetFeaturedProductsDTO
    {
        public int Id { get; set; }

        public DateTime StartFrom { get; set; }

        public DateTime EndOn { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }
    }

    public class AddFeaturedProductDTO
    {
        [Required]
        public DateTime StartFrom { get; set; }

        public DateTime? EndOn { get; set; }

        [Required]
        [NonZero]
        public int ProductId { get; set; }
    }

    public class UpdateFeaturedProductDTO
    {
        [Required]
        [NonZero]
        public int Id { get; set; }

        public DateTime StartFrom { get; set; }

        public DateTime EndOn { get; set; }
    }
}
