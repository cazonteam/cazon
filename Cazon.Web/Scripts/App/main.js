/// <reference path="../jquery/jquery-3.2.1.js" />

var Cart = {
    Init: function() {
        $(".cazon-cart select.product-quantity").on('change', function () {
            Utility.ToggleOverLay();
            var $this = $(this);
            var $tr = $this.closest("tr");
            var quantity = parseFloat($this.val());
            var perUnitPrice = parseFloat($tr.find("td.per-unit-price span").text());
            $tr.find('td.remove h4 span').text((quantity * perUnitPrice).toString() + " PKR.");

            Cart.CalculateCartTotal();
            Utility.ToggleOverLay();
        });

        $(".cazon-cart .cart-bottom h1#updateCart").on('click', function () {
            Utility.ToggleOverLay();
            var data = {
                __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val()
            };

            data["CartItems"] = [];
            $(".cazon-cart tr.cart-item").each(function (index, elem) {
                var $tr = $(elem);
                var quantity = $tr.find("td.quantity select.product-quantity").val();
                var cartItemId = $tr.attr("data-cazon-cartItemId");
                data["CartItems"].push({ "CartItemId": cartItemId, "Quantity": quantity });
            });

            var config = {
                url: "/Cart/UpdateCart",
                method: "POST",
                data: data
            };

            $.ajax(config).then(
                function (successResponse) {
                    if (successResponse["Success"]) {
                        Notification.ShowDefaultToast("Cart is updated successfully.", "Updated");
                    }
                    Utility.ToggleOverLay();
                },
                function (errorResponse) {
                    Utility.ToggleOverLay();
                    console.log(errorResponse);
                });
        });

        $(".cazon-cart .cart-item .remove-product").on('click', function () {
            Utility.ToggleOverLay();
            var $tr = $(this).closest("tr");
            var options = {
                url: "/Cart/DeleteCartItem",
                method: "POST",
                data: {
                    CartItemId: $tr.attr("data-cazon-cartItemId"),
                    __RequestVerificationToken: $("input[name='__RequestVerificationToken']").val()
                }
            };

            $.ajax(options).then(
                function (successResponse) {
                    if (successResponse["Success"]) {
                        Cart.UpdateCartItemCount(Cart.GetCartItemCount() - 1);
                        $tr.fadeOut(500, function () {
                            $tr.remove();
                            Notification.ShowDefaultToast("Item is deleted from the cart.", "Deleted");
                            Cart.CalculateCartTotal();
                        });
                    }
                    Utility.ToggleOverLay();
                },
                function (errorResponse) {
                    Utility.ToggleOverLay();
                    console.log(errorResponse);
                });
        });
    },

    CalculateCartTotal: function() {
        var cartTotal = 0.0;
        $(".cazon-cart tr.cart-item").each(function (index, elem) {
            var $tr = $(elem);
            var quantity = parseFloat($tr.find("td.quantity select.product-quantity").val());
            var perUnitPrice = parseFloat($tr.find("td.per-unit-price span").text());
            $tr.find("td.remove span").text(quantity * perUnitPrice);
            cartTotal += (quantity * perUnitPrice);
        });

        $(".cazon-cart .cart-bottom .dis-total h1 span").text(cartTotal);
    },

    UpdateCartItemCount: function (cartItemCount) {
        if (cartItemCount <= 0) {
            $("a.w3view-cart #shoppingCartCount").html("");
        }
        else {
            $("a.w3view-cart #shoppingCartCount").html(cartItemCount);
        }
    },

    GetCartItemCount: function () {
        var cartItemCount = $("a.w3view-cart #shoppingCartCount").text();

        return (cartItemCount == "") ? 0 : parseInt(cartItemCount);
    }
};

var Menu = {
    Init: function() {
        //open/close mega-navigation
        $('.cd-dropdown-trigger').on('click', function (event) {
            event.preventDefault();
            toggleNav();
        });

        //close meganavigation
        $('.cd-dropdown .cd-close').on('click', function (event) {
            event.preventDefault();
            toggleNav();
        });

        //on mobile - open submenu
        $('.has-children').children('a').on('click', function (event) {
            //prevent default clicking on direct children of .has-children 
            event.preventDefault();
            var selected = $(this);
            selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
        });

        //on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
        var submenuDirection = (!$('.cd-dropdown-wrapper').hasClass('open-to-left')) ? 'right' : 'left';
        $('.cd-dropdown-content').menuAim({
            activate: function (row) {
                $(row).children().addClass('is-active').removeClass('fade-out');
                if ($('.cd-dropdown-content .fade-in').length == 0) $(row).children('ul').addClass('fade-in');
            },
            deactivate: function (row) {
                $(row).children().removeClass('is-active');
                if ($('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row))) {
                    $('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
                    $(row).children('ul').addClass('fade-out')
                }
            },
            exitMenu: function () {
                $('.cd-dropdown-content').find('.is-active').removeClass('is-active');
                return true;
            },
            submenuDirection: submenuDirection,
        });

        //submenu items - go back link
        $('.go-back').on('click', function () {
            var selected = $(this),
                visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
            selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
        });

        function toggleNav() {
            var navIsVisible = (!$('.cd-dropdown').hasClass('dropdown-is-active')) ? true : false;
            $('.cd-dropdown').toggleClass('dropdown-is-active', navIsVisible);
            $('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
            if (!navIsVisible) {
                $('.cd-dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $('.has-children ul').addClass('is-hidden');
                    $('.move-out').removeClass('move-out');
                    $('.is-active').removeClass('is-active');
                });
            }
        }
    }
};

var Product = {
    Init: function () {
        $("button.w3ls-cart.cazon-product").on("click", function () {
            Utility.ToggleOverLay();
            var productId = $(this).attr("data-cazon-ProductId");
            var requestVerificationToken = $("input[name='__RequestVerificationToken']").val();
            var options = {
                method: "POST",
                url: "/Cart/AddProductToCart",
                data: {
                    ProductId: productId,
                    __RequestVerificationToken: requestVerificationToken
                }
            };
            $.ajax(options)
                .done(function (successResponse) {
                    if (successResponse["Success"]) {
                        Cart.UpdateCartItemCount(Cart.GetCartItemCount() + 1);
                        Notification.ShowDefaultToast("Item is added to the cart.", "Added");
                    }
                    else if (successResponse["Error"]) {
                        Notification.ShowErorToast("Sorry, This product is already in your cart.");
                    }
                    else if (successResponse["Exception"]) {
                        Notification.ShowErorToast("Sorry, an error occurred on the server while adding product to the cart.");
                    }
                    Utility.ToggleOverLay();
                    console.log(successResponse);
                })
                .fail(function (errorResponse) {
                    Utility.ToggleOverLay();
                    console.log(errorResponse);
                });
        });
    }
};

var Product_ViewProducts = {
    Init: function () {

    }
};

var UserAuth = {
    Init: function () {
        $("a#btnLogOff").on('click', function () {
            $("form#formLogOff").submit();
        });
    }
};

var SearchFilters = {
    Init: function () {
        $("#priceFilter input[type='checkbox']").on('click', function () {
            Utility.UpdateQueryParamter('priceRange', this.checked ? this.value : '');
        });

        $("#discountFilter input[type='checkbox']").on('click', function () {
            Utility.UpdateQueryParamter('discount', this.checked ? this.value : '');
        });

        $("#productColorFilter input[type='checkbox']").on('click', function () {
            Utility.UpdateQueryParamter('color', this.checked ? this.value : '');
        });

        $("#productSortBy .dropdown-menu li > a").on('click', function () {
            Utility.UpdateQueryParamter('sortBy', $(this).attr("data-cazon-sortBy"));
        });
    }
};

var Utility = {
    ToggleOverLay: function() {
        var windowHeigth = $('body').height();
        if ($('#overlay').hasClass('active')) {
            $('#overlay').addClass('hidden').removeClass('active');
        }
        else {
            $('#overlay').height(windowHeigth).removeClass('hidden').addClass('active');
        }
        
    },

    UpdateQueryParamter: function (paramName, updatedValue) {
        /// <signature>
        /// <summary>Updated the specified query string param value.</summary>
        /// <param name="paramName" type="String">Parameter name in the query string whose value need to b updated</param>
        /// <param name="updatedValue" type="String">The update value of the paramName, empty string means value should be removed.</param>
        /// </signature>

        let queryParams = document.location.search.replace('?', '').split("&");

        if (document.location.search.indexOf(paramName) == -1) {
            queryParams.push(paramName + "=" + updatedValue);

        }
        else {
            $.each(queryParams, function (index, param) {
                if (param != '' && param.indexOf(paramName) >= 0) {
                    if (updatedValue == '') {
                        queryParams.splice(index, 1);
                    }
                    else {
                        let paramNameAndValue = param.split('=');
                        if (paramNameAndValue.length == 2) {
                            queryParams[index] = paramNameAndValue[0] + '=' + updatedValue;
                        }
                    }
                    return false;
                }
            });
        }

        $.each(queryParams, function (index, param) {
            if (param == '') {
                queryParams.splice(index, 1);
            }
        });

        switch(queryParams.length) {
            case 0:
                document.location.search = "";
                break;
            case 1:
                document.location.search = "?" + queryParams[0];
                break;
            default:
                document.location.search = "?" + queryParams.join('&');
                break;
        }
    }
};

var Notification = {
    ShowDefaultToast: function (message, title) {
        toastr.info(message, title, Notification.DefaultSettings);
    },

    ShowErorToast: function (message, title) {
        toastr.error(message, title, Notification.DefaultSettings);
    },

    DefaultSettings: {
        "closeButton": true,
        "progressBar": true,
        "showDuration": "300",
        "timeOut": "5000"
    }
};

var CazonLazyLoad = {
    Init: function () {
        $('.lazy-load').lazy({
            enableThrottle: false,
            afterLoad: CazonLazyLoad_AfterLoad
        });

        $('.lazy-load-scroll').lazy({
            threshold: 200,
            afterLoad: CazonLazyLoad_AfterLoad
        });

        function CazonLazyLoad_AfterLoad(element) {
            $(element).removeClass('lazy-load lazy-load-scroll loading');
            if ($(element).attr('data-lazy-class-remove')) {
                $(element).find('.showAfterLoad').removeClass($(element).attr('data-lazy-class-remove') + ' showAfterLoad');
            }
        }
    }
}
