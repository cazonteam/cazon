﻿DECLARE @DatabaseName VarChar(max) = 'CazonDb';

/* ============== DROP ALL Foreign Keys From A Table.============== */
WHILE(EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE [CONSTRAINT_CATALOG] = @DatabaseName AND CONSTRAINT_TYPE = 'FOREIGN KEY'))
BEGIN
    DECLARE @sql_alterTable_fk NVARCHAR(2000)

    SELECT  TOP 1 @sql_alterTable_fk = ('ALTER TABLE ' + TABLE_SCHEMA + '.[' + TABLE_NAME + '] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']')
    FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS
    WHERE   [CONSTRAINT_CATALOG] = @DatabaseName AND CONSTRAINT_TYPE = 'FOREIGN KEY'
    EXEC (@sql_alterTable_fk)
END

/* ============== DROP ALL Tables From the Database. ============== */
DECLARE @dropAllTableSql NVARCHAR(max)=''
SELECT @dropAllTableSql += ' Drop table ' + QUOTENAME(TABLE_SCHEMA) + '.'+ QUOTENAME(TABLE_NAME) + '; '
FROM   INFORMATION_SCHEMA.TABLES
WHERE  TABLE_CATALOG = @DatabaseName
AND TABLE_TYPE = 'BASE TABLE';

Exec Sp_executesql @dropAllTableSql