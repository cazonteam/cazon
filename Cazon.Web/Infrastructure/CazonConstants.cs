﻿using Cazon.Web.Infrastructure.Helpers;

namespace Cazon.Web.Infrastructure
{
    public static class CazonConstants
    {
        public static class Cloudinary
        {
            public static string CloudinaryCloudName
            {
                get
                {
                    return nameof(CloudinaryCloudName).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryApiKey
            {
                get
                {
                    return nameof(CloudinaryApiKey).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryApiSecret
            {
                get
                {
                    return nameof(CloudinaryApiSecret).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryProductCategoryFolderName
            {
                get
                {
                    return nameof(CloudinaryProductCategoryFolderName).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryProductImageBaseUrl
            {
                get
                {
                    return nameof(CloudinaryProductImageBaseUrl).GetSettingByKey<string>();
                }
            }

            public static string CloudinaryProductImageFolder
            {
                get
                {
                    return nameof(CloudinaryProductImageFolder).GetSettingByKey<string>();
                }
            }

            public static string GetProductImageUrl(int heigth = 0, int width = 0)
            {
                var productUrl = CloudinaryProductImageBaseUrl;

                if (heigth > 0 && width > 0)
                {
                    productUrl += $"h_{heigth},w_{width}/";
                }
                else if (heigth > 0)
                {
                    productUrl += $"h_{heigth}/";
                }
                else if (width > 0)
                {
                    productUrl += $"w_{width}/";
                }

                productUrl += CloudinaryProductImageFolder;

                return productUrl;
            }

        }

        public static class FeaturedProducts
        {
            public static string[] FeaturedProductsDefaultCategories
            {
                get
                {
                    return "FeaturedProductsDefaultCategories".GetSettingByKey<string>().Split('|');
                }
            }
        }

        public static string ProductNameIdRegex
        {
            get
            {
                return nameof(ProductNameIdRegex).GetSettingByKey<string>();
            }
        }

        public static string KEY_ERROR
        {
            get
            {
                return nameof(KEY_ERROR).GetSettingByKey<string>();
            }
        }

        public static string KEY_EXCEPTION
        {
            get
            {
                return nameof(KEY_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string KEY_SUCCESS
        {
            get
            {
                return nameof(KEY_SUCCESS).GetSettingByKey<string>();
            }
        }

        public static string KEY_VALIDATION_EXCEPTION
        {
            get
            {
                return nameof(KEY_VALIDATION_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string KEY_MODEL_STATE_ERROR
        {
            get
            {
                return nameof(KEY_MODEL_STATE_ERROR).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_EXCEPTION
        {
            get
            {
                return nameof(MESSAGE_EXCEPTION).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NULL_MODEL
        {
            get
            {
                return nameof(MESSAGE_NULL_MODEL).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NO_ENTTIY_FOUND
        {
            get
            {
                return nameof(MESSAGE_NO_ENTTIY_FOUND).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NO_ENTTIY_FOUND_ON_DELETE
        {
            get
            {
                return nameof(MESSAGE_NO_ENTTIY_FOUND_ON_DELETE).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_NON_ZERO_ID
        {
            get
            {
                return nameof(MESSAGE_NON_ZERO_ID).GetSettingByKey<string>();
            }
        }

        public static string MESSAGE_DELETED
        {
            get
            {
                return nameof(MESSAGE_DELETED).GetSettingByKey<string>();
            }
        }

        public static string HEADERS_TO_REMOVE
        {
            get
            {
                return nameof(HEADERS_TO_REMOVE).GetSettingByKey<string>();
            }
        }

        public static bool EnableGoogleFonts
        {
            get
            {
                return "EnableGoogleFonts".GetSettingByKey<bool>();
            }
        }
    }
}