﻿using Cazon.Service.Infrastructure;
using Cazon.Service.Web.Contract;
using Cazon.ViewModel.Web.Products;
using Hydrogen.Extensions.Mvc5.Async;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Unity.Attributes;

namespace Cazon.Web.Infrastructure.Filters.ActionFilters.ProductFilters
{
    public class FeaturedProductsAttribute : AsyncActionFilterAttribute
    {
        [Dependency]
        public IFeaturedProductServiceWeb FeaturedProductService { set; get; }

        public override async Task OnResultExecutionAsync(ResultExecutingContext filterContext, ResultExecutionDelegate next)
        {
            // Execute code before the result is invoked.
            // You can "short-circuit" the action by setting 'filterContext.Canceled = true'
            // and returning before calling 'next()'.

            using (FeaturedProductService)
            {
                if (!(filterContext.Result is RedirectToRouteResult))
                {
                    var viewResult = filterContext.Result as ViewResult;
                    IGetFeaturedProducts IFeaturedProducts = (viewResult != null) ? viewResult.Model as IGetFeaturedProducts : null;

                    if (IFeaturedProducts != null)
                    {
                        if (IFeaturedProducts.FeaturedProductsCategories != null && IFeaturedProducts.FeaturedProductsCategories.Length > 0)
                        {
                            IFeaturedProducts.CategoryFeatureProducts = await FeaturedProductService.GetFeaturedProducts(IFeaturedProducts.FeaturedProductsCategories);
                        }
                        else
                        {
                            IFeaturedProducts.FeaturedProductsCategories = CazonConstants.FeaturedProducts.FeaturedProductsDefaultCategories;
                            IFeaturedProducts.CategoryFeatureProducts = await FeaturedProductService.GetFeaturedProducts(CazonConstants.FeaturedProducts.FeaturedProductsDefaultCategories);
                        }
                    }
                    else
                    {
                        viewResult.ViewBag.CategoryFeatureProducts = CazonConstants.FeaturedProducts.FeaturedProductsDefaultCategories;
                        viewResult.ViewBag.CategoryFeatureProducts = await FeaturedProductService.GetFeaturedProducts(CazonConstants.FeaturedProducts.FeaturedProductsDefaultCategories);
                    }
                }
            }

            // The implementation is responsible for calling 'next()'.
            var resultExecutedContext = await next().ConfigureAwait(false);

            // Execute code after the result is invoked
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}