﻿using Microsoft.AspNet.Identity;
using System.Security.Principal;
using System.Web;

namespace Cazon.Web.Infrastructure.Helpers.CazonUser
{
    public static class CazonIdentityManager
    {
        public static IPrincipal CurrentUser
        {
            get
            {
                return HttpContext.Current.User;
            }
        } 

        public static string UserName
        {
            get
            {
                return CurrentUser.Identity.GetUserName();
            }
        }

        public static string CurrentSessionId
        {
            get
            {
                return HttpContext.Current.Session.SessionID;
            }
        }
    }
}