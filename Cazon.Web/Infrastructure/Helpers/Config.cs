﻿using System;
using System.Configuration;

namespace Cazon.Web.Infrastructure.Helpers
{
    public static class Config
    {
        public static T GetSettingByKey<T>(this string name)
        {
            string value = ConfigurationManager.AppSettings[name];
            if (value == null)
            {
                return (T)Convert.ChangeType("", typeof(T));
            }

            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}