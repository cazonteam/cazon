using Cazon.Service.Common;
using Cazon.Service.Common.CazonAuth;
using Cazon.Service.Common.Contract;
using Cazon.Service.Common.Contract.CazonAuth;
using Cazon.Service.Common.Contract.Search;
using Cazon.Service.Common.Search;
using Cazon.Service.Web;
using Cazon.Service.Web.Contract;
using Cazon.Service.Web.Contract.Shopping;
using Cazon.Service.Web.Shopping;
using Cazon.Web.Infrastructure;
using Hydrogen.Extensions.Mvc5.Async;
using Microsoft.Owin;
using System;
using System.Web;
using System.Web.Mvc.Async;
using Unity;
using Unity.Injection;

namespace Cazon.Web
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IProductCategoryServiceWEB, ProductCategoryServiceWEB>();
            container.RegisterType<IProductServiceWEB, ProductServiceWEB>();
            container.RegisterType<IFeaturedProductServiceWeb, FeaturedProductServiceWeb>();
            container.RegisterType<ICloudinaryService>(new InjectionFactory(o =>
            {
                return new CloudinaryService
                {
                    CloudinaryApiKey = CazonConstants.Cloudinary.CloudinaryApiKey,
                    CloudinaryApiSecret = CazonConstants.Cloudinary.CloudinaryApiSecret,
                    CloudinaryCloudName = CazonConstants.Cloudinary.CloudinaryCloudName,
                    CloudinaryUploadFolder = CazonConstants.Cloudinary.CloudinaryProductImageFolder
                };
            }));

            container.RegisterType<ICartServiceWeb, CartServiceWeb>();
            container.RegisterType<ISearchService, SearchService>();

            container.RegisterType<IConfigureAuthService, ConfigureAuthService>();
            container.RegisterType<IUserIdentityService, UserIdentityService>();
            container.RegisterType<IOwinContext>(new InjectionFactory(o => HttpContext.Current.GetOwinContext()));
            container.RegisterType<IAsyncActionInvoker, ControllerActionInvokerEx>();
        }
    }
}