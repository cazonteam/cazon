﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Cazon.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("CazonElmahErrorLog.axd");

            routes.MapRoute(
                name: MapRouteNames.SearchRoute,
                url: "Search/{SearchText}",
                defaults: new { controller = "Search", action = "Index", SearchText = UrlParameter.Optional });

            routes.MapRoute(
                name: MapRouteNames.ViewCategoryProductsRoute,
                url: "Products/{CategoryName}",
                defaults: new { controller = "Product", action = "ViewProducts", CategoryName = UrlParameter.Optional });

            routes.MapRoute(
                name: MapRouteNames.ViewProductDetailsRoute,
                url: "Products/Details/{productName}",
                defaults: new { controller = "Product", action = "ProductDetails", productName = UrlParameter.Optional });

            routes.MapRoute(
                name: MapRouteNames.ViewCartRoute,
                url: MapRouteNames.ViewCartRoute,
                defaults: new { controller = "Cart", action = "Index" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        public static class MapRouteNames
        {
            public static string ViewCategoryProductsRoute
            {
                get
                {
                    return "ViewCategoryProducts";
                }
            }

            public static string ViewProductDetailsRoute
            {
                get
                {
                    return "ViewProductDetails";
                }
            }

            public static string ViewCartRoute
            {
                get
                {
                    return "ViewCart";
                }
            }

            public static string SearchRoute
            {
                get
                {
                    return "Search";
                }
            }
        }
    }
}
