﻿using Cazon.Web.Infrastructure.Helpers;
using System.Web.Optimization;

namespace Cazon.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.validate*",
                        "~/Scripts/jquery/jquery.validate.unobtrusive*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/Main").Include(
                "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/bundles/Main").Include(
                "~/Scripts/App/owl.carousel.js",
                "~/Scripts/App/jquery-scrolltofixed-min.js",
                "~/Scripts/App/move-top.js",
                "~/Scripts/App/easing.js",
                "~/Scripts/App/custom.js",
                "~/Scripts/App/jquery.marquee.min.js",
                "~/Scripts/App/jquery.knob.js",
                "~/Scripts/App/jquery.throttle.js",
                "~/Scripts/App/jquery.classycountdown.js",
                "~/Scripts/App/jquery.menu-aim.js",
                "~/Scripts/App/main.js",
                "~/Scripts/App/jquery.flexslider.js",
                "~/Scripts/App/jquery.scrollpane.min.js",
                "~/Scripts/App/jquery.mousewheel.js",
                "~/Scripts/toastr.js",
                "~/Scripts/LazyLoader/jquery.lazy.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                      "~/Content/bootstrap.css"));

            // Bundles for the apps.
            bundles.Add(new StyleBundle("~/Content/App").Include(
                "~/Content/App/style.css",
                "~/Content/App/menu.css",
                "~/Content/App/ken-burns.css",
                "~/Content/App/animate.min.css",
                "~/Content/App/owl.carousel.css",
                "~/Content/App/font-awesome.css",
                "~/Content/App/flexslider.css",
                "~/Content/toastr.css",
                "~/Content/Site.css"));
            BundleTable.EnableOptimizations = "BundleTable.EnableOptimizations".GetSettingByKey<bool>();
        }
    }
}
