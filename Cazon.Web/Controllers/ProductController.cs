﻿using Cazon.Service.Web.Contract;
using Cazon.ViewModel.Common.Enums;
using Cazon.ViewModel.Web;
using Cazon.ViewModel.Web.Products;
using Cazon.Web.Infrastructure;
using Cazon.Web.Infrastructure.Filters.ActionFilters.ProductFilters;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Expressions;

namespace Cazon.Web.Controllers
{
    [FeaturedProducts]
    public class ProductController : CazonWebBaseCRUDController
    {
        IProductServiceWEB Service;
        IFeaturedProductServiceWeb FeaturedProductService;

        public ProductController(IProductServiceWEB service, IFeaturedProductServiceWeb featuredProductService)
        {
            Service = service;
            FeaturedProductService = featuredProductService;

            DisposableObjects.Add(Service);
            DisposableObjects.Add(FeaturedProductService);
        }

        public ActionResult Index()
        {
            return View(new CazonDefaultDTO());
        }

        public async Task<ActionResult> ViewProducts(FindProductByCategoryDTO model)
        {
            if (string.IsNullOrEmpty(model.CategoryName))
                return this.RedirectToAction(a => a.Index());

            var result = await Service.GetCategoyProducts(model.CategoryName, model);
            result.FeaturedProductsCategories = new string[] { model.CategoryName };

            return View(result);
        }

        public async Task<ActionResult> ProductDetails(string productName)
        {
            if (!Regex.IsMatch(productName, CazonConstants.ProductNameIdRegex))
                return this.RedirectToAction(a => a.Index());

            var orignalProductName = Regex.Replace(productName, @"[\d\-]+", " ").Trim();
            var productId = int.Parse(Regex.Match(productName, @"[\d]+").Value);

            var model = await Service.GetProductDetailsByNameAndId(orignalProductName, productId);

            if (model == null) return this.RedirectToActionPermanent<HomeController>(a => a.Index());

            model.FeaturedProductsCategories = new string[] { model.ProductCategoryName };

            return View(model);
        }
    }
}