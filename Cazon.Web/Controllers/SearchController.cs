﻿using Cazon.Service.Common.Contract.Search;
using Cazon.ViewModel.Common.Search;
using Cazon.Web.Infrastructure.Filters.ActionFilters.ProductFilters;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Expressions;

namespace Cazon.Web.Controllers
{
    [FeaturedProducts]
    public class SearchController : CazonWebBaseCRUDController
    {
        ISearchService Service;

        public SearchController(ISearchService service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        public async Task<ActionResult> Index(SearchProductDTO model)
        {
            if (string.IsNullOrEmpty(model.SearchText))
                return this.RedirectToAction<HomeController>(a => a.Index());

            var result = await base.ExecuteServiceFunc(model, Service.SearchProducts);

            return View(result);
        }
    }
}