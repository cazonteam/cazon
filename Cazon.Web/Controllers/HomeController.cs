﻿using Cazon.Service.Web.Contract;
using Cazon.Web.Infrastructure.Filters.ActionFilters.ProductFilters;
using System.Web.Mvc;

namespace Cazon.Web.Controllers
{
    public class HomeController : CazonWebBaseCRUDController
    {
        IProductCategoryServiceWEB ProductCategoryService;
        IFeaturedProductServiceWeb FeaturedProductService;

        public HomeController(IProductCategoryServiceWEB productCategoryService, IFeaturedProductServiceWeb featuredProductService)
        {
            ProductCategoryService = productCategoryService;
            FeaturedProductService = featuredProductService;

            DisposableObjects.Add(ProductCategoryService);
            DisposableObjects.Add(FeaturedProductService);
        }

        [FeaturedProducts]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Faq()
        {
            return View();
        }

        public ActionResult SiteMap()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult Privacy()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}