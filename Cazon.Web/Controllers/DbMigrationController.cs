﻿using Cazon.Models.DbContexts;
using Cazon.Web.Infrastructure.Helpers;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Cazon.Web.Controllers
{
    // /DbMigration/UpdateDatabase
    public class DbMigrationController : CazonWebBaseController
    {
        const string ExecuteScriptKey = "ExecuteScriptCaptcha";
        // DbMigration/UpdateDatabase
        public async Task<ActionResult> UpdateDatabase()
        {
            if ("DbMigrationControllerAllowed".GetSettingByKey<string>().ToLower().Equals("true"))
            {
                var result = await Task.Factory.StartNew<object>(() =>
                {
                    try
                    {
                        var config = new Models.Migrations.Configuration();
                        var dbMigrator = new DbMigrator(config);
                        
                        dbMigrator.Update();
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                        return ex;
                    }

                    return true;
                });

                return Content(result.ToString());
            }

            return Content("Sorry, you don't have sufficient privileges to view this directory...");
        }

        // /DbMigration/DropEntireDatabaseWithRelations
        public async Task<ActionResult> DropEntireDatabaseWithRelations()
        {
            if ("DbMigrationControllerAllowed".GetSettingByKey<string>().ToLower().Equals("true"))
            {
                var result = -1;
                var script = System.IO.File.ReadAllText(Server.MapPath("MapRouteRootString".GetSettingByKey<string>()) +
                    "EfDbScriptPath".GetSettingByKey<string>());
                using (var context = CazonDbContextFactory.GetDbContext("DynamicMigrationConnectionStringName".GetSettingByKey<string>()))
                {
                    result = await context.Database.ExecuteSqlCommandAsync(TransactionalBehavior.EnsureTransaction, script);
                }

                return Content(script);
            }

            return Content("Sorry, you don't have sufficient privileges to view this directory...");
        }

        [HttpGet]
        // /DbMigration/ExecuteScript
        public ActionResult ExecuteScript()
        {
            Session[ExecuteScriptKey] = DateTime.Now.AddMonths(new Random().Next(1000)).ToString("dd/MM/yyyy");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // /DbMigration/ExecuteScript
        public async Task<ActionResult> ExecuteScript(HttpPostedFileBase scriptFile, string captcha)
        {
            var finalResult = string.Empty;

            if (CheckCaptach(captcha))
            {
                if (scriptFile != null && scriptFile.ContentLength > 0 && scriptFile.FileName.EndsWith(".sql"))
                {
                    var scriptToRun = string.Empty;
                    try
                    {
                        using (var reader = new StreamReader(scriptFile.InputStream))
                        {
                            scriptToRun = await reader.ReadToEndAsync();
                        }

                        using (var context = CazonDbContextFactory.GetDbContext("DynamicMigrationConnectionStringName".GetSettingByKey<string>()))
                        {
                            var queryResult = await context.Database.ExecuteSqlCommandAsync(TransactionalBehavior.EnsureTransaction, scriptToRun);

                            if (queryResult > 0)
                                finalResult = $"Success! SQL Commands were executed successfully With Changes Made: {queryResult}.";
                            else
                                finalResult = $"Success! Commands were executed successfully With No Changes.";
                        }
                    }
                    catch (Exception ex)
                    {
                        finalResult = $"Exepction! An Exception Occured While Executing Query.\n{ex.ToString()}\n{ex.StackTrace}\n{ex.Message}";
                    }
                }
                else
                {
                    finalResult = $"Sorry! Invalid File Type.";
                }
            }
            else
                finalResult = $"Sorry! Invalid Captcha.";

            return Content(finalResult);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // /DbMigration/ExecuteScriptForResult
        public async Task<ActionResult> ExecuteScriptForResult(HttpPostedFileBase scriptFile, string captcha)
        {
            var finalResult = string.Empty;
            if (CheckCaptach(captcha))
            {
                if (scriptFile != null && scriptFile.ContentLength > 0 && scriptFile.FileName.EndsWith(".sql"))
                {
                    var scriptToRun = string.Empty;
                    try
                    {
                        using (var reader = new StreamReader(scriptFile.InputStream))
                        {
                            scriptToRun = await reader.ReadToEndAsync();
                        }

                        using (var context = CazonDbContextFactory.GetDbContext("DynamicMigrationConnectionStringName".GetSettingByKey<string>()))
                        {
                            var command = context.Database.Connection.CreateCommand();
                            command.CommandText = scriptToRun;
                            await context.Database.Connection.OpenAsync();
                            using (var reader = await command.ExecuteReaderAsync())
                            {
                                var dataSet = new DataSet();
                                do
                                {
                                    // loads the DataTable (schema will be fetch automatically)
                                    var dataTable = new DataTable();
                                    dataTable.Load(reader);
                                    dataSet.Tables.Add(dataTable);

                                }
                                while (!reader.IsClosed);

                                finalResult = JsonConvert.SerializeObject(dataSet);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        finalResult = $"Exepction! An Exception Occured While Executing Query.\n{ex.ToString()}\n{ex.StackTrace}\n{ex.Message}";
                    }
                }
                else
                {
                    finalResult = $"Sorry! Invalid File Type.";
                }
            }

            else
                finalResult = $"Sorry! Invalid Captcha.";

            return Content(finalResult);
        }

        private bool CheckCaptach(string captcha)
        {
            if (string.IsNullOrEmpty(captcha) || captcha.Length < 6)
                return false;

            var actualCaptcha = Session[ExecuteScriptKey].ToString().Split('/');

            var dateSum = 0;
            actualCaptcha[0].ToList().ForEach(c => { dateSum += int.Parse(c.ToString()); });
            var monthSum = 0;
            actualCaptcha[1].ToList().ForEach(c => { monthSum += int.Parse(c.ToString()); });
            var actualSum = dateSum + monthSum;

            var captchaSum = int.Parse(captcha.Substring(2, 2));

            if (actualSum >= 10)
            {
                var yearSum = 0;
                actualCaptcha[2].ToList().ForEach(c => { yearSum += int.Parse(c.ToString()); });

                if (yearSum % 3 == 0)
                    actualSum += 10;
                else
                    actualSum -= 10;
            }

            return (captchaSum == actualSum);
        }
    }
}