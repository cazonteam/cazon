﻿using Cazon.Service.Web.Contract.Shopping;
using Cazon.ViewModel.Common.Shopping.Cart;
using Cazon.Web.Infrastructure;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Cazon.Web.Controllers
{
    public class CartController : CazonWebBaseCRUDController
    {
        protected ICartServiceWeb Service;

        public CartController(ICartServiceWeb cartService)
        {
            Service = cartService;
            DisposableObjects.Add(Service);
        }

        public async Task<ActionResult> Index()
        {
            var result = await Service.GetCurrentCartDetails();

            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AddProductToCart(AddProductToCartDTO model)
        {
            if (CheckForValidModel())
            {
                var result = await Service.AddProductToCart(model);
                if (result.Success)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = result.Result;
                }
                else
                {
                    CazonJsonResult["Error"] = result.Errors;
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateCartProduct(UpdateCartItemDTO model)
        {
            if (CheckForValidModel())
            {
                var result = await Service.UpdateCartItem(model);
                if (result.Success)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = result.Result;
                }
                else
                {
                    CazonJsonResult["Error"] = result.Errors;
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UpdateCart(UpdateCartDTO model)
        {
            if(CheckForValidModel())
            {
                var result = await Service.UpdateCart(model);
                if(result.Success)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = result.Result;
                }
                else
                {
                    CazonJsonResult["Error"] = result.Errors;
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeleteCartItem(DeleteCartItemDTO model)
        {
            if (CheckForValidModel())
            {
                var result = await Service.DeleteCartItem(model);
                if (result.Success)
                {
                    CazonJsonResult[CazonConstants.KEY_SUCCESS] = result.Result;
                }
                else
                {
                    CazonJsonResult["Error"] = result.Errors;
                }
            }

            return Json(CazonJsonResult, JsonRequestBehavior.AllowGet);
        }
    }
}