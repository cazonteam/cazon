﻿namespace Cazon.Web.DTO
{
    public class ViewProductCategoryDTO
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }

    }
}