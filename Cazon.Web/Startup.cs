﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup("Cazon.Web", typeof(Cazon.Web.Startup))]
namespace Cazon.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
