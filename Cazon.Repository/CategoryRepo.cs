﻿using Cazon.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class CategoryRepo : CazonBaseRepo<ProductCategory>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }

        public List<ProductCategory> GetParentCategories()
        {
            return DbContext.ProductCategory.Where(p => p.ParentCategoryId == null).ToList();
        }

        public async Task<List<ProductCategory>> GetCategoriesWithSubCategories()
        {
            return await DbContext.ProductCategory.ToListAsync();
        }

        public async Task<List<ProductCategory>> GetSubCategories(int parentCategoryId)
        {
            return await DbContext.ProductCategory.Where(p => p.ParentCategoryId == parentCategoryId).ToListAsync();
        }

        public async Task<List<ProductCategory>> GetCategoriesByName(string categoryName)
        {
            return await DbContext.ProductCategory.Where(p => p.CategoryName.ToLower().Contains(categoryName.ToLower()))
                .ToListAsync();
        }

        public async Task<bool> DeleteParentCategory(int categoryId)
        {
            var isDeleted = false;
            using (var dbTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    var categories = await DbContext.ProductCategory
                        .Where(c => c.Id == categoryId || c.ParentCategoryId == categoryId)
                        .ToListAsync();
                    var category = categories.FirstOrDefault(c => c.Id == categoryId);
                    var childCategories = categories.Where(c => c.ParentCategoryId == category.Id).ToList();

                    foreach (var childCategory in childCategories)
                    {
                        childCategory.ParentCategoryId = null;
                        DbContext.Entry(childCategory).State = EntityState.Modified;
                    }

                    DbContext.Entry(category).State = EntityState.Deleted;

                    await DbContext.SaveChangesAsync();
                    dbTransaction.Commit();

                    isDeleted = true;
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
            }

            return isDeleted;
        }
    }
}
