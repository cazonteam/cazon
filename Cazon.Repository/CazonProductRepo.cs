﻿using Cazon.Models.Products;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common.Enums;
using Cazon.ViewModel.Common.Product;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class CazonProductRepo : CazonBaseRepo<Product>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }

        protected override Expression<Func<Product, bool>> GetByIdExpression(params object[] Ids)
        {
            var productId = (int)Ids[0];

            return (p) => p.Id == productId;
        }

        public async Task<List<Product>> GetProductsByShop(int shopId)
        {
            return await DbContext.Product.Where(p => p.CazonShopId == shopId).ToListAsync();
        }

        public async Task<List<Product>> GetProductsByCategoryId(int categoryId)
        {
            return await DbContext.Product.Where(p => p.ProductCategoryId == categoryId).ToListAsync();
        }

        public async Task<List<Product>> GetProductsByCategoryName(string categoryName, IProductSearchFilters filters = null)
        {
            var query = DbContext.Product.Where(p => p.ProductCategory.CategoryName.ToLower().Contains(categoryName.ToLower()));

            query = ApplySearchFilters(query, filters);

            return await query.ToListAsync();
        }

        public async Task<List<Product>> FindProducts(string searchText, IProductSearchFilters filters = null)
        {
            var query = DbContext.Product.Where(p => p.Name.ToLower().Contains(searchText.ToLower()));

            query = ApplySearchFilters(query, filters);

            return await query.ToListAsync();
        }

        public async Task<Product> GetProductByNameAndId(string productName, int productId)
        {
            var lowerProductName = productName.ToLower();

            return await DbContext.Product.FirstOrDefaultAsync(p => p.Name.ToLower() == lowerProductName && p.Id == productId);
        }

        public async Task<bool> DeleteProductWithChild(Product product)
        {
            var isDeleted = false;
            using (var transaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    DbContext.ProductImage.RemoveRange(product.Images);

                    await DbContext.SaveChangesAsync();
                    DbContext.Entry(product).State = EntityState.Deleted;

                    if (await DbContext.SaveChangesAsync() > 0)
                    {
                        transaction.Commit();
                        isDeleted = true;
                    }
                    else
                        transaction.Rollback();
                }
                catch (Exception ex)
                {
                    ex.ToString();
                    transaction.Rollback();
                }
            }

            return isDeleted;
        }

        public async Task<List<Product>> FindProducts(FindProductModel model)
        {
            var query = DbContext.Product.AsQueryable();

            query = query.Where(p => p.Name.ToLower().Contains(model.SearchText.ToLower()));

            switch(model.SortBy)
            {
                case ProductSortBy.NameAsc:
                    query = query.OrderBy(o => o.Name);
                    break;
                case ProductSortBy.NameDesc:
                    query = query.OrderByDescending(o => o.Name);
                    break;
                case ProductSortBy.PriceAsc:
                    query = query.OrderBy(o => o.Price);
                    break;
                case ProductSortBy.PriceDesc:
                    query = query.OrderByDescending(o => o.Price);
                    break;
            }

            if ((model.NoOfRecords.HasValue && model.NoOfRecords.Value > 0) && (model.PageNumber.HasValue && model.PageNumber.Value > 0))
            {
                if (!model.SortBy.HasValue)
                    query = query.OrderBy(o => o.Name);

                query = query.Skip(model.PageNumber.Value * model.NoOfRecords.Value)
                    .Take(model.NoOfRecords.Value);
            }

            return await query.ToListAsync();
        }

        public Task<bool> CheckProductOwnerShip(string ownerId, int productId)
        {
            return DbContext.Product.AnyAsync(p => p.Id == productId && p.CazonShop.OwnerId == ownerId);
        }

        public async Task<List<KeyValuePair<int, string>>> GetProductsForSelect(string query)
        {
            var featuredProducts = await DbContext.Product.Where(p => p.Name.ToLower().Contains(query))
                .Select(p => new { p.Id, p.Name })
                .ToListAsync();

            return featuredProducts.Select(p => new KeyValuePair<int, string>(p.Id, p.Name)).ToList();
        }

        protected IQueryable<Product> ApplySearchFilters(IQueryable<Product> query, IProductSearchFilters filters)
        {
            if (filters != null)
            {
                var priceRange = filters.PriceRanges;
                if (priceRange != null)
                {
                    if (priceRange.Item1 > 0.0 && priceRange.Item2 > 0.0)
                    {
                        query = query.Where(p => p.Price >= priceRange.Item1 && p.Price <= priceRange.Item2);
                    }
                    else if (priceRange.Item1 > 0.0)
                    {
                        query = query.Where(p => p.Price >= priceRange.Item1);
                    }
                }

                var discountRange = filters.DiscountRanges;
                if (discountRange != null)
                {
                    if (discountRange.Item1 > 0.0 && discountRange.Item2 > 0.0)
                    {
                        query = query.Where(p => (p.DiscountStartFrom != null && p.DiscountStartFrom <= DateTime.Now) && (p.DiscountEndOn == null || p.DiscountEndOn >= DateTime.Now) && p.Discount >= discountRange.Item1 && p.Discount <= discountRange.Item2);
                    }
                    else if (discountRange.Item1 > 0.0)
                    {
                        query = query.Where(p => (p.DiscountStartFrom != null && p.DiscountStartFrom <= DateTime.Now) && (p.DiscountEndOn == null || p.DiscountEndOn >= DateTime.Now) && p.Discount >= discountRange.Item1);
                    }
                }

                switch (filters.SortBy)
                {
                    case ProductSortBy.Latest:
                        query = query.OrderBy(o => o.CreatedOn);
                        break;
                    case ProductSortBy.NameAsc:
                        query = query.OrderBy(o => o.Name);
                        break;
                    case ProductSortBy.NameDesc:
                        query = query.OrderByDescending(o => o.Name);
                        break;
                    case ProductSortBy.Popular:
                        break;
                    case ProductSortBy.PriceAsc:
                        query = query.OrderBy(o => o.Price);
                        break;
                    case ProductSortBy.PriceDesc:
                        query = query.OrderByDescending(o => o.Price);
                        break;
                }
            }
            else
            {
                query = query.OrderBy(o => o.CreatedOn);
            }

            return query;
        }
    }
}
