﻿using Cazon.Models;
using System.Threading.Tasks;

namespace Cazon.Repository
{

    public class ProductImageRepo : CazonBaseRepo<ProductImage>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }
    }
}