﻿using Cazon.Models;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class CazonUserRepo : CazonBaseRepo<CazonUser>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (string)id);
        }

    }
}
