﻿using Cazon.Models.Shopping.CazonCart;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Repository.Shopping.CazonCart
{
    public class CartItemRepo : CazonBaseRepo<CartItem>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.CartItemId == (int)id);
        }


        public Task<bool> IsProductInTheCart(int cartId, int productId)
        {
            return DbContext.CartItem.AnyAsync(i => i.CartId == cartId && i.ProductId == productId);
        }

        public Task<CartItem> GetCartItemByCartId(int cartId, int cartItemId)
        {
            return DbContext.CartItem.FirstOrDefaultAsync(i => i.CartId == cartId && i.CartItemId == cartItemId);
        }

        public Task<List<CartItem>> GetAllCartItemsByCartId(int cartId)
        {
            return DbContext.CartItem.Where(i => i.CartId == cartId).ToListAsync();
        }
    }
}
