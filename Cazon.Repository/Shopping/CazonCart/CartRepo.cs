﻿using Cazon.Models.Shopping.CazonCart;
using System.Threading.Tasks;
using System.Data.Entity;
using System;
using System.Linq.Expressions;
using System.Linq;

namespace Cazon.Repository.Shopping.CazonCart
{
    public class CartRepo : CazonBaseRepo<Cart>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.CartId == (int)id);
        }

        public Task<Cart> GetCartBySessionId(string sessionId)
        {
            return DbContext.Cart.FirstOrDefaultAsync(c => c.SessionId == sessionId);
        }

        public Task<Cart> GetCartByUserId(string userId)
        {
            return DbContext.Cart.FirstOrDefaultAsync(c => string.IsNullOrEmpty(c.UserId) == false && c.UserId == userId);
        }

        public async Task<Tuple<int, int>> GetCartIdAndItemsCountByUser(string userId)
        {
            var cartIdAndItemsCounts = (await DbContext.Cart.Where(c => c.UserId == userId).Select(c => new { ItemCount = c.CartItems.Count(), CartId = c.CartId }).FirstOrDefaultAsync());

            Tuple<int, int> result = null;
            if (cartIdAndItemsCounts == null)
                result = new Tuple<int, int>(-1, 0);
            else
                result = new Tuple<int, int>(cartIdAndItemsCounts.CartId, cartIdAndItemsCounts.ItemCount);

            return result;
        }

        protected override Expression<Func<Cart, bool>> GetByIdExpression(params object[] Ids)
        {
            var cartId = int.Parse(Ids[0].ToString());

            return c => c.CartId == cartId;
        }
    }
}
