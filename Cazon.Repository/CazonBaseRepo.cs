﻿using Cazon.Models.DbContexts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public abstract class CazonBaseRepo<T> : IDisposable where T : class
    {
        private CazonDbContext _db;

        protected CazonDbContext DbContext
        {
            get
            {
                if (_db == null)
                {
                    _db = new CazonDbContext();
                    _db.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
                }

                return _db;
            }
            private set
            {
                _db = value;
            }
        }

        public bool ProxyCreationEnabled(bool enabled)
        {
            DbContext.Configuration.ProxyCreationEnabled = enabled;

            return true;
        }

        public virtual async Task<T> AddEntity(T entity)
        {
            AddEntitiesToContext(EntityState.Added, entity);

            return (await SaveChanges() > 0) ? entity : null;
        }

        public virtual async Task<T> UpdateEntity(T entity)
        {
            AddEntitiesToContext(EntityState.Modified, entity);

            return (await SaveChanges() > 0) ? entity : null;
        }

        public virtual async Task<ICollection<T>> UpdateEntities(ICollection<T> entitiesToUpdate)
        {
            AddEntitiesToContext(EntityState.Modified, entitiesToUpdate.ToArray());

            return (await SaveChanges() > 0) ? entitiesToUpdate : null;
        }

        public virtual async Task<T> DeleteEntity(T entity)
        {
            AddEntitiesToContext(EntityState.Deleted, entity);

            return (await SaveChanges() > 0) ? entity : null;
        }

        public virtual async Task<List<T>> GetAll()
        {
            return await DbContext.Set<T>().ToListAsync();
        }

        public virtual async Task<T> FindById(params object[] Id)
        {
            return await DbContext.Set<T>().FindAsync(Id);
        }

        public virtual async Task<T> FindById<TProperty>(object[] Ids, Expression<Func<T, TProperty>> includePath)
        {
            var query = DbContext.Set<T>().AsQueryable();
            var getByIdPredicate = GetByIdExpression(Ids);

            if(getByIdPredicate == null)
                throw new NullReferenceException("getByIdPredicate can't be null");

            if(includePath != null) query = query.Include(includePath);

            return await query.FirstOrDefaultAsync(getByIdPredicate);
        }

        public virtual async Task<bool> IsEntityExists(Expression<Func<T, bool>> filter)
        {
            if (filter == null)
                throw new ArgumentNullException(paramName:"filter", message: "filter can't be null");

            return await DbContext.Set<T>().AnyAsync(filter);
        }

        public abstract Task<bool> IsEntityExists(object id);

        public virtual void SetEntityState<TEntity>(EntityState entityState, params TEntity[] entities) where TEntity : class
        {
            var dbSet = DbContext.Set<TEntity>();

            if (entityState == EntityState.Deleted)
            {
                foreach(var entity in entities)
                dbSet.Remove(entity);
            }
            else
            {
                foreach (var entity in entities)
                {
                    if (DbContext.Entry(entity).State == EntityState.Detached)
                        dbSet.Attach(entity);

                    dbSet.RemoveRange(entities);

                    DbContext.Entry(entity).State = entityState;
                }
            }
        }

        public virtual void SetEntityState<TEntity>(EntityState entityState, ICollection<TEntity> entities) where TEntity : class
        {
            var dbSet = DbContext.Set<TEntity>();

            if (entityState == EntityState.Deleted)
            {
                dbSet.RemoveRange(entities);
            }
            else
            {
                foreach (var entity in entities)
                {
                    if (DbContext.Entry(entity).State == EntityState.Detached)
                        dbSet.Attach(entity);

                    dbSet.RemoveRange(entities);

                    DbContext.Entry(entity).State = entityState;
                }
            }
        }

        protected virtual Expression<Func<T, bool>> GetByIdExpression(params object[] Ids)
        {
            return null;
        }

        protected virtual void AddEntitiesToContext(EntityState entityState, params T[] entities)
        {
            foreach (var entity in entities)
                DbContext.Entry(entity).State = entityState;
        }

        protected virtual async Task<int> SaveChanges()
        {
            return await DbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                    DbContext = null;
                }
            }
        }
    }
}