﻿using Cazon.Models.Products;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class FeaturedProductRepo : CazonBaseRepo<FeaturedProduct>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }

        public override async Task<List<FeaturedProduct>> GetAll()
        {
            var currentDate = DateTime.Now;

            return await DbContext.FeaturedProducts.Where(f => f.EndOn.HasValue == false || currentDate <= f.EndOn.Value).ToListAsync();
        }

        public Task<bool> IsProductFeatured(int productId)
        {
            var currentDate = DateTime.Now;

            return DbContext.FeaturedProducts.AnyAsync(p => p.ProductId == productId && (p.EndOn.HasValue == false || currentDate <= p.EndOn.Value));
        }

        public Task<FeaturedProduct> GetFeaturedProductByProductId(int productId)
        {
            return DbContext.FeaturedProducts.FirstOrDefaultAsync(f => f.ProductId == productId);
        }

        public Task<bool> IsExpiredFeaturedProduct(int productId)
        {
            var currentDate = DateTime.Now;

            return DbContext.FeaturedProducts.AnyAsync(p => p.ProductId == productId && p.EndOn < currentDate);
        }

        public async Task<List<IGrouping<string, FeaturedProduct>>> GetFeaturedProducts(string[] categoryNames)
        {
            var currentDate = DateTime.Now;

            return await DbContext.FeaturedProducts.Where(p => categoryNames.Contains(p.Product.ProductCategory.CategoryName.ToLower()) && currentDate >= p.StartFrom && currentDate <= p.EndOn)
                .Include(i => i.Product)
                .Include(i => i.Product.Images)
                .GroupBy(key => key.Product.ProductCategory.CategoryName)
                .ToListAsync();
        }

        public async Task<List<IGrouping<int, FeaturedProduct>>> GetFeaturedProducts(int[] categoryIds)
        {
            var currentDate = DateTime.Now;

            return await DbContext.FeaturedProducts.Where(p => categoryIds.Contains(p.Product.ProductCategoryId) && currentDate >= p.StartFrom && currentDate <= p.EndOn)
                .Include(i => i.Product)
                .Include(i => i.Product.Images)
                .GroupBy(key => key.Product.ProductCategoryId)
                .ToListAsync();
        }
    }
}