﻿using Cazon.Models;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class CityRepo : CazonBaseRepo<City>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }
    }
}
