﻿using Cazon.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cazon.Repository
{
    public class CazonShopRepo : CazonBaseRepo<CazonShop>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.Id == (int)id);
        }

        public async Task<CazonShop> GetShopByIdAndOwner(int shopId, string ownerId)
        {
            return await DbContext.CazonShop.FirstOrDefaultAsync(s => s.Id == shopId && s.OwnerId == ownerId);
        }

        public async Task<CazonShop> GetShopById(int shopId)
        {
            return (await GetShopsByPredicate(s => s.Id == shopId)).FirstOrDefault();
        }

        public async Task<List<CazonShop>> GetAllShopsByOwner(string ownerId)
        {
            return await GetShopsByPredicate(s => s.OwnerId == ownerId);
        }

        public async Task<bool> IsShopExists(string shopName)
        {
            shopName = shopName.ToLower();

            return await DbContext.CazonShop.AnyAsync(s => s.Name.ToLower() == shopName);
        }

        public async Task<bool> CheckShopOwnership(int shopId, string ownerId)
        {
            return await DbContext.CazonShop.AnyAsync(s => s.Id == shopId && s.OwnerId == ownerId);
        }

        protected override Expression<Func<CazonShop, bool>> GetByIdExpression(params object[] Ids)
        {
            var shopId = (int)Ids[0];

            return s => s.Id == shopId;
        }

        private async Task<List<CazonShop>> GetShopsByPredicate(Expression<Func<CazonShop, bool>> predicate)
        {
            return await DbContext.CazonShop.Where(predicate).ToListAsync();
        }
    }
}