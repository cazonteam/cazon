﻿using Cazon.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Cazon.Repository
{

    public class TokenRepo : CazonBaseRepo<CazonToken>
    {
        public override Task<bool> IsEntityExists(object id)
        {
            return IsEntityExists(f => f.UserId == (string)id);
        }

        public async Task<CazonToken> GetToken(string userId)
        {
            return await DbContext.CazonToken.FirstOrDefaultAsync(u => u.UserId == userId);
        }
    }
}