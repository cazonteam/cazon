﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup("Cazon.API", typeof(Cazon.API.Startup))]
namespace Cazon.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
        }
    }
}
