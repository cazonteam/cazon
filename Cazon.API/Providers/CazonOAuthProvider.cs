﻿using Cazon.Service.Common.Contract.CazonAuth;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Threading.Tasks;

namespace Cazon.API.Providers
{
    public class CazonOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string PublicClientId;
        ICazonOAuthProviderService OAuthProviderService;

        public CazonOAuthProvider()
        {

        }

        public CazonOAuthProvider(string publicClientId, ICazonOAuthProviderService oAuthProviderService)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            if(oAuthProviderService == null)
            {
                throw new ArgumentNullException("oAuthProviderService");
            }

            PublicClientId = publicClientId;
            OAuthProviderService = oAuthProviderService;
            OAuthProviderService.PublicClientId = PublicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            await OAuthProviderService.GrantResourceOwnerCredentials(context);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            return OAuthProviderService.TokenEndpoint(context);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            return OAuthProviderService.ValidateClientAuthentication(context);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            return OAuthProviderService.ValidateClientRedirectUri(context);
        }
    }
}