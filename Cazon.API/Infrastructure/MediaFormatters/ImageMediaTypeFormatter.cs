﻿using Cazon.ViewModel.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using static Cazon.API.Model.CazonConstants;

namespace Cazon.API.Infrastructure.MediaFormatters
{

    public class FileMediaTypeBaseFormatter : MediaTypeFormatter
    {
        public FileMediaTypeBaseFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(MultiPartMediaTypeFormatter.MultiPartMediaHeader));
        }

        public override bool CanReadType(Type type)
        {
            var isImplementsIUploadFile = type.GetInterfaces().Contains(typeof(IUploadFile));

            return isImplementsIUploadFile;
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public async override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var provider = await content.ReadAsMultipartAsync();
            var modelObject = Activator.CreateInstance(type);

            if (provider.Contents.Any(c => c.Headers.ContentDisposition.Name.NormalizeContentDispositionName().Matches(MultiPartMediaTypeFormatter.MutiPartModelContentRegex)))
            {
                var modelContent = await provider.Contents
                    .FirstOrDefault(c => c.Headers.ContentDisposition.Name.NormalizeContentDispositionName().Matches(MultiPartMediaTypeFormatter.MutiPartModelContentRegex))
                    .ReadAsStringAsync();

                if (!string.IsNullOrEmpty(modelContent))
                    modelObject = JsonConvert.DeserializeObject(modelContent, type);
            }

            var fileContents = provider.Contents
                .Where(c => c.Headers.ContentDisposition.Name.NormalizeContentDispositionName().Matches(MultiPartMediaTypeFormatter.MultiPartImageMatchRegex))
                .ToList();

            var secondsToAdd = 1.0;
            var uploadedFiles = new List<UploadFileModel>();
            foreach (var fileContent in fileContents)
            {
                uploadedFiles.Add(new UploadFileModel
                {
                    FileName = fileContent.Headers.ContentDisposition.FileName.NormalizeContentDispositionName(),
                    MimeType = fileContent.Headers.ContentType.MediaType,
                    FileData = await fileContent.ReadAsByteArrayAsync(),
                    TempUploadPath = string.Empty,
                    UploadedOn = DateTime.Now.AddSeconds(secondsToAdd++)
                });
            }

            (modelObject as IUploadFile).UploadedFiles = uploadedFiles;

            return modelObject;
        }
    }
}
