﻿using Cazon.API.Model;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Cazon.API.Infrastructure.Attribute.ActionFilter
{
    public class NullModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionArguments.Count > 0 && actionContext.ActionArguments.Any(kv => kv.Value == null))
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, CazonConstants.MESSAGE_NULL_MODEL);
            else
                base.OnActionExecuting(actionContext);
        }
    }
}