﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Cazon.API.Infrastructure.Attribute.ActionFilter
{
    public class VerifyTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            IEnumerable<string> tokenValues;

            filterContext.Request.Headers.TryGetValues("Authorization", out tokenValues);


            if(tokenValues != null)
            {
                var token = tokenValues.FirstOrDefault();

                if (token != GetCurrentToken())
                {
                    //filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private string GetCurrentToken()
        {
            // get the token from wherever you'd like ...
            return string.Empty;
        }
    }
}