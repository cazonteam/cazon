﻿using System.Text.RegularExpressions;

namespace System
{
    public static class StringExtenstions
    {
        public static string NormalizeContentDispositionName(this string text)
        {
            return text.Replace("\"", "");
        }

        public static bool Matches(this string text, string pattern)
        {
            return Regex.IsMatch(text, pattern, RegexOptions.IgnoreCase);
        }
    }
}