﻿using System;
using System.Linq.Expressions;

namespace Cazon.API.Infrastructure
{
    public static class ReflectionHelper
    {
        public static string PropertyName<T>(Expression<Func<T, object>> property) where T : class
        {
            var body = property.Body as MemberExpression;
            if (body == null)
            {
                var ubody = (UnaryExpression)property.Body;
                body = ubody.Operand as MemberExpression;
            }

            return body.Member.Name;
        }
    }
}
