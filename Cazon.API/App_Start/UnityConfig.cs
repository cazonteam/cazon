using Cazon.API.Model;
using Cazon.Service.API;
using Cazon.Service.API.Contract;
using Cazon.Service.API.Contract.Shopping;
using Cazon.Service.API.Shopping;
using Cazon.Service.Common;
using Cazon.Service.Common.CazonAuth;
using Cazon.Service.Common.Contract;
using Cazon.Service.Common.Contract.CazonAuth;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace Cazon.API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType<ICityServiceAPI, CityServiceAPI>();
            container.RegisterType<IProductImageServiceAPI, ProductImageServiceAPI>();
            container.RegisterType<IProductCategoryServiceAPI, ProductCategoryServiceAPI>();
            container.RegisterType<ICazonShopServiceAPI, CazonShopServiceAPI>();
            container.RegisterType<ICazonUserServiceAPI, CazonUserServiceAPI>();
            container.RegisterType<IProductServiceAPI, ProductServiceAPI>();
            container.RegisterType<ICloudinaryService>(new InjectionFactory(o =>
            {
                return new CloudinaryService
                {
                    CloudinaryApiKey = CazonConstants.Cloudinary.CloudinaryApiKey,
                    CloudinaryApiSecret = CazonConstants.Cloudinary.CloudinaryApiSecret,
                    CloudinaryCloudName = CazonConstants.Cloudinary.CloudinaryCloudName,
                    CloudinaryUploadFolder = CazonConstants.Cloudinary.CloudinaryProductImageFolderName
                };
            }));
            container.RegisterType<ICartServiceAPI, CartServiceAPI>();

            container.RegisterType<IConfigureAuthService, ConfigureAuthService>();
            container.RegisterType<ICazonOAuthProviderService, CazonOAuthProviderService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}