﻿using Cazon.API.Infrastructure.Attribute.ActionFilter;
using Cazon.API.Infrastructure.MediaFormatters;
using Cazon.API.Model;
using Elmah.Contrib.WebApi;
using FluentValidation.WebApi;
using Microsoft.Owin.Security.OAuth;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;

namespace Cazon.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            config.Filters.Add(new NullModelAttribute());
            config.Filters.Add(new VerifyTokenAttribute());
            config.Filters.Add(new AuthorizeAttribute());

            // enable Elmah Error Logging.
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());

            FluentValidationModelValidatorProvider.Configure(config);

            // Enable Cross Origin Requests...
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ApiWithAction",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithController",
                routeTemplate: "{controller}/{id}",
                defaults: new { controller = "Error", action = "DefaultResult", id = RouteParameter.Optional }
            );

            // remove default XML handler
            var xmlFomatters = config.Formatters.OfType<XmlMediaTypeFormatter>().ToList();
            foreach (var match in xmlFomatters)
                config.Formatters.Remove(match);

            config.Formatters.Add(new FileMediaTypeBaseFormatter());
            foreach(var headerValue in CazonConstants.JsonFormatterMediaTypeHeaderValues.Split(';'))
                config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue(headerValue));
        }
    }
}
