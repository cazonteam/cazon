﻿using Cazon.API.Model;
using Cazon.API.Providers;
using Cazon.Service.Common.CazonAuth;
using Cazon.Service.Common.Contract.CazonAuth;
using Cazon.Web.Infrastructure.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;

namespace Cazon.API
{
    public partial class Startup
    {
        IConfigureAuthService AuthService;

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public Startup()
        {
            AuthService = new ConfigureAuthService();
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            AuthService.ConfigureAuth(app);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow

            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new CazonOAuthProvider(PublicClientId, new CazonOAuthProviderService()),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromSeconds("TokenExpireTimeInSeconds".GetSettingByKey<int>()),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});

            app.Use((context, next) => {
                foreach (var headerToRemove in CazonConstants.HEADERS_TO_REMOVE.Split(';'))
                    context.Response.Headers.Remove(headerToRemove);

                return next();
            });
        }
    }
}
