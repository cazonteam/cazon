﻿using Cazon.API.Infrastructure.MediaFormatters;
using Cazon.API.Model;
using Cazon.Service.API.Contract;
using Cazon.ViewModel.API;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [AllowAnonymous]
    public class ProductImageController : CazonAPIBaseCRUDController
    {
        IProductImageServiceAPI Service;

        public ProductImageController()
        {

        }

        public ProductImageController(IProductImageServiceAPI productImageService)
        {
            Service = productImageService;
            DisposableObjects.Add(Service);
        }

        public async Task<IHttpActionResult> GetAll()
        {
            await base.GetAllEntitiesService(Service.GetAll);

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetById(int productImageId)
        {
            if (CheckForNonZeroNonNegative(productImageId))
            {
                await base.GetEntityByIdService((object)productImageId, Service.FindById);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddProductImage(AddProductImageDTO model)
        {
            if (CheckForValidModel())
            {
                await base.AddEntityService(model, Service.Add);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> EditProductImage(EditProductImageDTO model)
        {
            if (CheckForValidModel())
            {
                if (await Service.IsEntityExists(model.ImageId))
                {
                    await base.UpdateEntityService(model, Service.Update);

                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> DeleteProductImage(int productImageId)
        {
            if (CheckForNonZeroNonNegative(productImageId))
            {
                if (await Service.IsEntityExists(productImageId))
                    await base.DeleteEntityService(productImageId, Service.Delete);
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE };
            }

            return Ok(ResponseBody);
        }
    }
}