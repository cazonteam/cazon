﻿using System.Web.Http;
using System.Web.Http.Results;

namespace Cazon.API.Controllers
{
    public class ErrorController : ApiController
    {
        [AcceptVerbs("Get","Post")]
        [AllowAnonymous]
        public IHttpActionResult DefaultResult()
        {
            return Ok("Sorry, We couldn't understand the resource you are looking for....");
        }
    }
}
