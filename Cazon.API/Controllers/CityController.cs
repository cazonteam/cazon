﻿using Cazon.API.Model;
using Cazon.Service.API.Contract;
using Cazon.ViewModel.API;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [AllowAnonymous]
    public class CityController : CazonAPIBaseCRUDController
    {
        private ICityServiceAPI Service;

        public CityController(ICityServiceAPI cityService)
        {
            Service = cityService;
            DisposableObjects.Add(Service);
        }

        public async Task<IHttpActionResult> GetAll()
        {
            await GetAllEntitiesService(Service.GetAll);

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetById(int cityId)
        {
            if (CheckForNonZeroNonNegative(cityId))
                await GetEntityByIdService((object)cityId, Service.GetById);

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddCity(AddCityDTO model)
        {
            if (CheckForValidModel())
                await base.AddEntityService(model, Service.Add);

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> EditCity(UpdateCityDTO model)
        {
            if (CheckForValidModel())
            {
                if(await Service.IsEntityExists(model.CityId))
                    await base.UpdateEntityService(model, Service.Update);
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteCity(int cityId)
        {
            if (CheckForNonZeroNonNegative(cityId))
            {
                if (await Service.IsEntityExists(cityId))
                    await base.DeleteEntityService(cityId, Service.Delete);
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE };
            }
            return Ok(ResponseBody);
        }
    }
}
