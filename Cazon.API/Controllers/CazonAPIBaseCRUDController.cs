﻿using Cazon.API.Model;
using Cazon.Service.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    public abstract class CazonAPIBaseCRUDController : CazonAPIBaseController
    {
        /// <summary>
        /// Find the entries from the database by calling the <paramref name="function"/> and add the result of the function to the ResponseBody and return true, or add the error/exception message to the response otherwise.
        /// </summary>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return as a list of <typeparamref name="TResult"/>.</typeparam>
        /// <param name="function">The model that will be passed to the <paramref name="function"/>.</param>
        /// <returns>return true if entities are found/not found without any error or false if any error or exception is occur.</returns>
        [NonAction]
        protected virtual async Task<bool> GetAllEntitiesService<TResult>(Func<Task<List<TResult>>> function)
        {
            try
            {
                var result = await function?.Invoke();
                ResponseBody[CazonConstants.KEY_SUCCESS] = result ?? new List<TResult>();
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return ResponseBody.ContainsKey(CazonConstants.KEY_SUCCESS);
        }

        /// <summary>
        /// Find the entity from the database by calling the <paramref name="function"/> and add the result of the function to the ResponseBody and return true, or add the error/exception message to the response otherwise.
        /// </summary>
        /// <typeparam name="TParam">The type of <paramref name="model"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return.</typeparam>
        /// <param name="entityId">The id of the entity that is being searched.</param>
        /// <param name="function">The model that will be passed to the <paramref name="function"/>.</param>
        /// <returns>return true if entity is found/not found without any error or false if any error or exception is occur.</returns>
        [NonAction]
        protected virtual async Task<bool> GetEntityByIdService<TParam, TResult>(TParam entityId, Func<TParam, Task<TResult>> function)
        {
            var isFound = false;

            try
            {
                var result = await function?.Invoke(entityId);
                if (result != null)
                {
                    ResponseBody[CazonConstants.KEY_SUCCESS] = result;
                    isFound = true;
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NO_ENTTIY_FOUND;
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isFound;
        }

        /// <summary>
        /// AddShop the entity using the service. If entity is Added successfully then add the result of the <paramref name="function"/> to the ResponseBody or add the validation exceptions errors or other exception messages to the ResponseBody otherwise.
        /// </summary>
        /// <typeparam name="TModel">The type of <paramref name="model"/> that will be passed as argument to <paramref name="function"/>.</typeparam>
        /// <typeparam name="TResult">The type of class that the <paramref name="function"/> will return.</typeparam>
        /// <param name="model">The model that will be passed to the <paramref name="function"/>.</param>
        /// <param name="function">The service class method that will be called to add the entity.</param>
        /// <returns>return true if entity is successfully added or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> AddEntityService<TModel, TResult>(TModel model, Func<TModel, Task<TResult>> function)
        {
            var isAdded = false;

            try
            {
                var result = await function?.Invoke(model);
                if (result != null)
                {
                    ResponseBody[CazonConstants.KEY_SUCCESS] = new object[] { result };
                    isAdded = true;
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = CazonConstants.KEY_ERROR;
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isAdded;
        }

        /// <summary>
        /// Updates the entity using the service. If entity is updated successfully then add success message to the ResponseBody or add the validation exceptions errors or other exception messages to the ResponseBody otherwise.
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="model">The model that will be passed as argument to the function.</param>
        /// <param name="function">The service function that will be called to the update the entity.</param>
        /// <returns>If entity is updated successfully returns true or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> UpdateEntityService<TModel>(TModel model, Func<TModel, Task<bool>> function)
        {
            var isUpdated = false;

            try
            {
                if (await function?.Invoke(model))
                {
                    ResponseBody[CazonConstants.KEY_SUCCESS] = new string[] { CazonConstants.KEY_SUCCESS };
                    isUpdated = true;
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.KEY_ERROR };
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isUpdated;
        }

        /// <summary>
        /// Delete the entity using the service. If entity is deleted successfully then add success message to the ResponseBody or add the validation exceptions errors or other exception messages to the ResponseBody otherwise.
        /// </summary>
        /// <param name="entityId">The id of the entity that is being deleted.</param>
        /// <param name="function">The service function that will be called when deleting the entity.</param>
        /// <returns>If entity is deleted successfully returns true or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> DeleteEntityService(object entityId, Func<object, Task<bool>> function)
        {
            var isDeleted = false;

            try
            {
                if (await function?.Invoke(entityId))
                {
                    ResponseBody[CazonConstants.KEY_SUCCESS] = new string[] { CazonConstants.MESSAGE_DELETED };
                    isDeleted = true;
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isDeleted;
        }

        /// <summary>
        /// Executes the service function and attach the result of the function to the ResponseBody if result is not null. if result is null then add the no entity found message to the ResponseBody.
        /// If Exception is occur then add the respective exception messages to the ResponseBody.
        /// </summary>
        /// <typeparam name="TModel">The type of the argument that the <paramref name="function"/> need.</typeparam>
        /// <typeparam name="TResult">The return type of the <paramref name="function"/></typeparam>
        /// <param name="model">The model to be passed as an argument to the <paramref name="function"/></param>
        /// <param name="function">The function that will be executed.</param>
        /// <returns>true if service function executed successfully or false otherwise.</returns>
        [NonAction]
        protected virtual async Task<bool> ExecuteSerivceFunc<TModel, TResult>(TModel model, Func<TModel, Task<TResult>> function)
        {
            var isExecuted = false;

            try
            {
                var result = await function?.Invoke(model);
                if (result != null)
                {
                    ResponseBody[CazonConstants.KEY_SUCCESS] = result;
                    isExecuted = true;
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return isExecuted;
        }

        [NonAction]
        /// <summary>
        /// Executes the service function and attach the result of the function to the ResponseBody and If Exception is occur then add the respective exception messages to the ResponseBody.
        /// </summary>
        /// <typeparam name="TResult">The CazonResult<TResult> return type of the <paramref name="function"/></typeparam>
        /// <param name="model">The model to be passed as an argument to the <paramref name="function"/></param>
        /// <param name="function">The function that will be executed.</param>
        /// <returns>true if service function executed successfully or false otherwise.</returns>
        protected virtual async Task<bool> ExecuteCazonResultSerivceFunc<TResult>(Func<Task<CazonResult<TResult>>> function)
        {
            var result = new CazonResult<TResult>();
            try
            {
                result = await function?.Invoke();
                if (result.Success)
                    ResponseBody[CazonConstants.KEY_SUCCESS] = result.Result;
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = result.Errors;
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return result.Success;
        }

        [NonAction]
        /// <summary>
        /// Executes the service function and attach the result of the function to the ResponseBody and If Exception is occur then add the respective exception messages to the ResponseBody.
        /// </summary>
        /// <typeparam name="TResult">The CazonResult<TResult> return type of the <paramref name="function"/></typeparam>
        /// <param name="model">The model to be passed as an argument to the <paramref name="function"/></param>
        /// <param name="function">The function that will be executed.</param>
        /// <returns>true if service function executed successfully or false otherwise.</returns>
        protected virtual async Task<bool> ExecuteCazonResultSerivceFuncWithModel<TModel, TResult>(TModel model, Func<TModel, Task<CazonResult<TResult>>> function)
        {
            var result = new CazonResult<TResult>();
            try
            {
                result = await function?.Invoke(model);
                if (result.Success)
                    ResponseBody[CazonConstants.KEY_SUCCESS] = result.Result;
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = result.Errors;
            }
            catch (DbEntityValidationException validationException)
            {
                base.LogError(validationException);
                ResponseBody[CazonConstants.KEY_VALIDATION_EXCEPTION] = GetValidationErrors(validationException);
            }
            catch (Exception ex)
            {
                base.LogError(ex);
                ResponseBody[CazonConstants.KEY_EXCEPTION] = new string[] { CazonConstants.MESSAGE_EXCEPTION };
            }

            return result.Success;
        }
    }
}