﻿using Cazon.API.Model;
using Cazon.Service.API.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [AllowAnonymous]
    public class CazonShopController : CazonAPIBaseCRUDController
    {
        private ICazonShopServiceAPI Service;

        public CazonShopController(ICazonShopServiceAPI service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        public async Task<IHttpActionResult> GetShopById(int shopId)
        {
            if (CheckForNonZeroNonNegative(shopId))
            {
                await base.GetEntityByIdService(shopId, Service.GetShopById);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetShopByOwner(string ownerId)
        {
            if (CheckForNonEmptyNull(ownerId))
            {
                await base.GetEntityByIdService(ownerId, Service.GetAllShopsByOwner);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddShop(AddShopApiDTO model)
        {
            if (CheckForValidModel())
            {
                await base.AddEntityService(model, Service.AddShop);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> EditShop(EditShopApiDTO model)
        {
            if (CheckForValidModel())
            {
                if (await Service.IsEntityExists(model.ShopId))
                {
                    await base.UpdateEntityService(model, Service.EditShop);
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE;
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteShop(DeleteShopDTO model)
        {
            if (CheckForValidModel())
            {
                if (await Service.IsEntityExists(model.ShopId))
                {
                    await base.ExecuteSerivceFunc(model, Service.DeleteShop);
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE };
            }

            return Ok(ResponseBody);
        }
    }
}