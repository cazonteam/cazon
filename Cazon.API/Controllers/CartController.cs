﻿using Cazon.Service.API.Contract.Shopping;
using Cazon.ViewModel.Common.Shopping.Cart;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [Authorize]
    public class CartController : CazonAPIBaseCRUDController
    {
        ICartServiceAPI Service;

        public CartController(ICartServiceAPI service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetCurrentCartDetails()
        {

            await base.ExecuteCazonResultSerivceFunc(Service.GetCurrentCartDetails);

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddProductToCart(AddProductToCartDTO model)
        {
            if (CheckForValidModel())
            {
                await base.ExecuteCazonResultSerivceFuncWithModel(model, Service.AddProductToCart);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateCartItem(UpdateCartItemDTO model)
        {
            if (CheckForValidModel())
            {
                await base.ExecuteCazonResultSerivceFuncWithModel(model, Service.UpdateCartItem);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateCart(UpdateCartDTO model)
        {
            if (CheckForValidModel())
            {
                await base.ExecuteCazonResultSerivceFuncWithModel(model, Service.UpdateCart);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteCartItem(DeleteCartItemDTO model)
        {
            if (CheckForValidModel())
            {
                await base.ExecuteCazonResultSerivceFuncWithModel(model, Service.DeleteCartItem);
            }

            return Ok(ResponseBody);
        }

    }
}
