﻿using Cazon.Service.API.Contract;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [AllowAnonymous]
    public class ProductCategoryController : CazonAPIBaseCRUDController
    {
        IProductCategoryServiceAPI Service;

        public ProductCategoryController(IProductCategoryServiceAPI service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        public async Task<IHttpActionResult> GetCategoriesWithSubCategories()
        {
            await base.GetAllEntitiesService(Service.GetCategoriesWithSubCategories);

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetSubCategories(int parentCategoryId)
        {
            if (CheckForNonZeroNonNegative(parentCategoryId))
            {
                await base.GetEntityByIdService(parentCategoryId, Service.GetSubCategories);
            }

            return Ok(ResponseBody);
        }
    }
}