﻿using Cazon.Service.API.Contract;
using Cazon.ViewModel.Common;
using System.Threading.Tasks;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    public class AccountController : CazonAPIBaseCRUDController
    {
        ICazonUserServiceAPI Service;

        public AccountController(ICazonUserServiceAPI service)
        {
            Service = service;
            DisposableObjects.Add(Service);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> Register(RegisterViewModel model)
        {
            if(CheckForValidModel())
            {
                await base.AddEntityService(model, Service.RegisterUser);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetBasicInfo(string userId)
        {
            if (CheckForNonEmptyNull(userId))
            {
                await base.GetEntityByIdService(userId, Service.GetBasicInfo);
            }

            return Ok(ResponseBody);
        }
    }
}