﻿using Cazon.API.Model;
using Cazon.Service.API.Contract;
using Cazon.Service.Common.Contract;
using Cazon.ViewModel.API;
using Cazon.ViewModel.Common;
using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;

namespace Cazon.API.Controllers
{
    [AllowAnonymous]
    public class ProductController : CazonAPIBaseCRUDController
    {
        IProductServiceAPI Service;
        ICloudinaryService CloudinaryService;

        public ProductController(IProductServiceAPI service, ICloudinaryService cloudinaryService)
        {
            Service = service;
            CloudinaryService = cloudinaryService;
            DisposableObjects.Add(Service);
            DisposableObjects.Add(CloudinaryService);

            CloudinaryService.CloudinaryUploadFolder = CazonConstants.Cloudinary.CloudinaryProductImageFolderName;
        }

        [HttpPost]
        public async Task<IHttpActionResult> FindProdcuts(FindProductModel model)
        {
            if(CheckForValidModel())
            {
                await base.ExecuteSerivceFunc(model, Service.FindProducts);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetProductsByShop(int shopId)
        {
            if (CheckForNonZeroNonNegative(shopId))
            {
                await base.GetEntityByIdService(shopId, Service.GetProductsByShop);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetProductById(int productId)
        {
            if (CheckForNonZeroNonNegative(productId))
            {
                await base.GetEntityByIdService(productId, Service.GetProductsById);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetProductsByCategoryId(int categoryId)
        {
            if (CheckForNonZeroNonNegative(categoryId))
            {
                await base.GetEntityByIdService(categoryId, Service.GetProductsByCategoryId);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetProductsByCategoryName(string categoryName)
        {
            if (CheckForNonEmptyNull(categoryName))
            {
                await base.GetEntityByIdService(categoryName, Service.GetProductsByCategoryName);
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> GetImagesForProduct(int productId)
        {
            if (CheckForNonZeroNonNegative(productId))
            {
                await base.GetEntityByIdService(productId, Service.GetProductImages);
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddProduct(AddProductApiDTO model)
        {
            if (CheckForValidModel())
            {
                if(await base.AddEntityService(model, Service.AddProduct))
                {
                    SaveFileToTempPath(model);
                    var uploadedResult = await CloudinaryService.UploadFiles(model.UploadedFiles);
                    DeleteTempFiles(model);
                    
                }
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateProduct(UpdateProductAPIDTO model)
        {
            if(CheckForValidModel())
            {
                if (await Service.IsEntityExists(model.ProductId))
                {
                    if (await base.UpdateEntityService(model, Service.UpdateProduct))
                    {
                        SaveFileToTempPath(model);
                        await CloudinaryService.UploadFiles(model.UploadedFiles);
                        DeleteTempFiles(model);
                    }
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND };
            }

            return Ok(ResponseBody);
        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteProduct(int productId)
        {
            if(CheckForNonZeroNonNegative(productId))
            {
                if(await Service.IsEntityExists(productId))
                {
                    await base.DeleteEntityService(productId, Service.DeleteProduct);
                }
                else
                    ResponseBody[CazonConstants.KEY_ERROR] = new string[] { CazonConstants.MESSAGE_NO_ENTTIY_FOUND_ON_DELETE };
            }

            return Ok(ResponseBody);
        }

        public async Task<IHttpActionResult> AddProductImages(AddProductImageTestDTO model)
        {
            if (CheckForValidModel())
            {
                SaveFileToTempPath(model);
                await base.ExecuteSerivceFunc(model.UploadedFiles, CloudinaryService.UploadFiles);
                DeleteTempFiles(model);
            }

            return Ok(ResponseBody);
        }

        [NonAction]
        private void SaveFileToTempPath(IUploadFile fileModel)
        {
            foreach (var file in fileModel.UploadedFiles)
            {
                var fileTempPath = Path.Combine(HostingEnvironment.MapPath("~/CloudinaryTempFolder"), file.FileName);
                File.WriteAllBytes(fileTempPath, file.FileData);
                file.TempUploadPath = fileTempPath;
            }

        }

        [NonAction]
        private void DeleteTempFiles(IUploadFile fileModel)
        {
            foreach (var file in fileModel.UploadedFiles)
            {
                if (File.Exists(file.TempUploadPath))
                    File.Delete(file.TempUploadPath);
            }
        }
    }
}
