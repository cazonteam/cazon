namespace Cazon.Models.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddedOrderNumberColumnToTheProductCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCategory", "OrderNumber", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCategory", "OrderNumber");
        }
    }
}
