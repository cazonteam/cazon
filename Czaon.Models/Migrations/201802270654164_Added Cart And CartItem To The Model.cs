namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCartAndCartItemToTheModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cart",
                c => new
                    {
                        CartId = c.Int(nullable: false, identity: true),
                        CreatedOn = c.DateTime(nullable: false),
                        SessionId = c.String(maxLength: 80),
                        UserId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.CartId)
                .Index(t => t.SessionId, unique: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.CartItem",
                c => new
                    {
                        CartItemId = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        AddedOn = c.DateTime(nullable: false),
                        CartID = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CartItemId)
                .ForeignKey("dbo.Cart", t => t.CartID, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.CartID)
                .Index(t => t.ProductId);
            
            AddColumn("dbo.AspNetUsers", "Cart_CartId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Cart_CartId");
            AddForeignKey("dbo.AspNetUsers", "Cart_CartId", "dbo.Cart", "CartId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Cart_CartId", "dbo.Cart");
            DropForeignKey("dbo.CartItem", "ProductId", "dbo.Product");
            DropForeignKey("dbo.CartItem", "CartId", "dbo.Cart");
            DropIndex("dbo.AspNetUsers", new[] { "Cart_CartId" });
            DropIndex("dbo.CartItem", new[] { "ProductId" });
            DropIndex("dbo.CartItem", new[] { "CartId" });
            DropIndex("dbo.Cart", new[] { "UserId" });
            DropIndex("dbo.Cart", new[] { "SessionId" });
            DropColumn("dbo.AspNetUsers", "Cart_CartId");
            DropTable("dbo.CartItem");
            DropTable("dbo.Cart");
        }
    }
}
