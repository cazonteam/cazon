﻿using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{

    public class ProductCategorySeed : ISeedData<ProductCategory>
    {
        public List<ProductCategory> GetSeedData()
        {
            var seedData = new List<ProductCategory>();

            var allCategory = new ProductCategory
            {
                CategoryName = "All",
                IconName = "fa-globe",
                OrderNumber = 1
            };
            var mobileParent = new ProductCategory
            {
                CategoryName = "Mobile",
                IconName = "fa-mobile",
                OrderNumber = 2
            };

            seedData.AddRange(GetGeneralCategories(3));
            seedData.AddRange(GetMobileSubCategories(mobileParent, 1));

            return seedData;
        }

        private List<ProductCategory> GetMobileSubCategories(ProductCategory parentCategory, int orderNumToStart)
        {
            var subCategory = new List<ProductCategory>
            {
                new ProductCategory
                {
                    CategoryName = "HTC"
                },
                new ProductCategory
                {
                    CategoryName = "Nokia"
                },
                new ProductCategory
                {
                    CategoryName = "Asus"
                },
                new ProductCategory
                {
                    CategoryName = "Black Barry"
                },
                new ProductCategory
                {
                    CategoryName = "Intex"
                },
                new ProductCategory
                {
                    CategoryName = "iPhone"
                },
                new ProductCategory
                {
                    CategoryName = "Karboon"
                },
                new ProductCategory
                {
                    CategoryName = "Lava"
                },
                new ProductCategory
                {
                    CategoryName = "Lenovo"
                },
                new ProductCategory
                {
                    CategoryName = "LG"
                },
                new ProductCategory
                {
                    CategoryName = "ML"
                },
                new ProductCategory
                {
                    CategoryName = "Micromax"
                },
                new ProductCategory
                {
                    CategoryName = "Motorola"
                },
                new ProductCategory
                {
                    CategoryName = "Samsung"
                },
                new ProductCategory
                {
                    CategoryName = "Sony"
                },
                new ProductCategory
                {
                    CategoryName = "Other Mobiles"
                }
            };

            foreach (var category in subCategory)
            {
                category.ParentCategory = parentCategory;
                category.OrderNumber = orderNumToStart++;
            }

            return subCategory;
        }

        private List<ProductCategory> GetGeneralCategories(int orderNumToStart)
        {
            var generalCategories = new List<ProductCategory>
            {
                new ProductCategory
                {
                    CategoryName = "Electronic And Appliance",
                    IconName = "fa-laptop"
                },
                new ProductCategory
                {
                    CategoryName = "Fashion Store",
                    IconName = "fa-car"
                },
                new ProductCategory
                {
                    CategoryName = "Kids Fashon & Toys",
                    IconName = "fa-book"
                },
                new ProductCategory
                {
                    CategoryName = "Home, Furniture & Patio",
                    IconName = "fa-book"
                },
                new ProductCategory
                {
                    CategoryName = "ports, Fitness & Outdoor",
                    IconName = "fa-book"
                },
                new ProductCategory
                {
                    CategoryName = "Grocery Store",
                    IconName = "fa-asterik"
                },
                new ProductCategory
                {
                    CategoryName = "Photo, Gifts & Office Supplies",
                    IconName = "fa-sheild"
                },
                new ProductCategory
                {
                    CategoryName = "Health, Beauty & Pharmacy",
                    IconName = "fa-at"
                },
                new ProductCategory
                {
                    CategoryName = "Automative",
                    IconName = "fa-home"
                },
                new ProductCategory
                {
                    CategoryName = "Books, Music & Movies",
                    IconName = "fa-home"
                }
            };

            foreach (var category in generalCategories)
                category.OrderNumber = orderNumToStart++;

            return generalCategories;
        }
    }
}