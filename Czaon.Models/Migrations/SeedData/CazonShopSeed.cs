﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cazon.Models.Migrations.SeedData
{

    public class CazonShopSeed : ISeedData<CazonShop>
    {
        public List<CazonShop> GetSeedData()
        {
            var shops = new List<CazonShop>
            {
                new CazonShop
                {
                    Name = "Zero Logics",
                    RegisteredOn = DateTime.Now,
                    AddressId = 1,
                    OwnerId = new CazonUserSeed().GetSeedData().ElementAt(0).Id
                }
            };

            return shops;
        }
    }
}