﻿using Cazon.Models.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{
    public class CazonUserSeed : ISeedData<CazonUser>
    {
        public List<CazonUser> GetSeedData()
        {
            var users = new List<CazonUser> { };

            var adminUser = new CazonUser
            {
                Id = "bd1ea359-7c7e-478d-9dc8-b373049468c4",
                FullName = "Cazon Admin",
                Email = "cazon-admin@cazon.com",
                PhoneNumber = "03332325698",
                UserName = "cazon@cazon.com",
                PasswordHash = new PasswordHasher().HashPassword("cAdmin123"),
                SecurityStamp = Guid.NewGuid().ToString()
            };

            adminUser.Roles.Add(new IdentityUserRole { RoleId = ((int)CazonUserRole.Admin).ToString(), UserId = adminUser.Id });

            users.Add(adminUser);

            return users;
        }
    }
}
