﻿using Cazon.Models.Enums;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{

    public class CazonUserRoleSeed : ISeedData<IdentityRole>
    {
        public List<IdentityRole> GetSeedData()
        {
            var roles = new List<IdentityRole>
            {
                new IdentityRole { Id = ((int)CazonUserRole.Admin).ToString(), Name = CazonUserRole.Admin.ToString() },
                new IdentityRole { Id = ((int)CazonUserRole.SalesMan).ToString(), Name = CazonUserRole.SalesMan.ToString() },
                new IdentityRole { Id = ((int)CazonUserRole.ShopKeeper).ToString(), Name = CazonUserRole.ShopKeeper.ToString() },
                new IdentityRole { Id = ((int)CazonUserRole.User).ToString(), Name = CazonUserRole.User.ToString() }
            };

            return roles;
        }
    }
}