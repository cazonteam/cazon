﻿using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{

    public interface ISeedData<T> where T : class, new()
    {
        List<T> GetSeedData();
    }
}