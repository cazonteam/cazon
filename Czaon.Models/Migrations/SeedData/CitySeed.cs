﻿using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{

    public class CitySeed : ISeedData<City>
    {
        public List<City> GetSeedData()
        {
            var seedData = new List<City>
            {
                new City
                {
                    FullName = "Islamabad",
                    ShortName = "ISB",
                    PostalCode = 54000
                },
                new City
                {
                    FullName = "Rawalpindi",
                    ShortName = "RWP",
                    PostalCode = 54000
                },
                new City
                {
                    FullName = "Lahore",
                    ShortName = "LHR",
                    PostalCode = 44000
                },
                new City
                {
                    FullName = "Khan Bella",
                    ShortName = "KHB",
                    PostalCode = 64040
                },
                new City
                {
                    FullName = "Karachi",
                    ShortName = "KCH",
                    PostalCode = 59632
                }
            };

            return seedData;
        }
    }
}