﻿using System.Collections.Generic;

namespace Cazon.Models.Migrations.SeedData
{

    public class AddressSeed : ISeedData<Address>
    {
        public List<Address> GetSeedData()
        {
            var seedData = new List<Address>
            {
                new Address { CityId = 1 }
            };

            return seedData;
        }
    }
}