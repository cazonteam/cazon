namespace Cazon.Models.Migrations
{
    using DbContexts;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<CazonDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;

            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CazonDbContext>());
        }

        protected override void Seed(CazonDbContext context)
        {
        }
    }
}
