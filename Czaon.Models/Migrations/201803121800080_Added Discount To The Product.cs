namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDiscountToTheProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "Discount", c => c.Double());
            AddColumn("dbo.Product", "DiscountStartFrom", c => c.DateTime());
            AddColumn("dbo.Product", "DiscountEndFrom", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "DiscountEndFrom");
            DropColumn("dbo.Product", "DiscountStartFrom");
            DropColumn("dbo.Product", "Discount");
        }
    }
}
