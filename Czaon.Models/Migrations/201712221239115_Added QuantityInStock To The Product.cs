namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedQuantityInStockToTheProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "QuantityInStock", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "QuantityInStock");
        }
    }
}
