namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFeaturedProductToTheModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FeaturedProduct",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartFrom = c.DateTime(nullable: false),
                        EndOn = c.DateTime(),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FeaturedProduct", "ProductId", "dbo.Product");
            DropIndex("dbo.FeaturedProduct", new[] { "ProductId" });
            DropTable("dbo.FeaturedProduct");
        }
    }
}
