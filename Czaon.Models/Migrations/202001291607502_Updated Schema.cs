namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedSchema : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Product", "CazonShopId", "dbo.CazonShop");
            AddColumn("dbo.Product", "CazonShop_Id", c => c.Int());
            AddColumn("dbo.CazonShop", "Product_Id", c => c.Int());
            CreateIndex("dbo.CazonShop", "Product_Id");
            CreateIndex("dbo.Product", "CazonShop_Id");
            AddForeignKey("dbo.CazonShop", "Product_Id", "dbo.Product", "Id");
            AddForeignKey("dbo.Product", "CazonShop_Id", "dbo.CazonShop", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "CazonShop_Id", "dbo.CazonShop");
            DropForeignKey("dbo.CazonShop", "Product_Id", "dbo.Product");
            DropIndex("dbo.Product", new[] { "CazonShop_Id" });
            DropIndex("dbo.CazonShop", new[] { "Product_Id" });
            DropColumn("dbo.CazonShop", "Product_Id");
            DropColumn("dbo.Product", "CazonShop_Id");
            AddForeignKey("dbo.Product", "CazonShopId", "dbo.CazonShop", "Id", cascadeDelete: true);
        }
    }
}
