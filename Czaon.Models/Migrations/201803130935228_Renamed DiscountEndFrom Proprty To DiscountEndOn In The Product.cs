namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedDiscountEndFromProprtyToDiscountEndOnInTheProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "DiscountEndOn", c => c.DateTime());
            DropColumn("dbo.Product", "DiscountEndFrom");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "DiscountEndFrom", c => c.DateTime());
            DropColumn("dbo.Product", "DiscountEndOn");
        }
    }
}
