namespace Cazon.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedShopPhoneNumberToTheCazonShop : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CazonShop", "ShopPhoneNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CazonShop", "ShopPhoneNumber");
        }
    }
}
