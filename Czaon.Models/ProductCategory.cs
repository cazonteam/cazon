﻿using System.ComponentModel.DataAnnotations;

namespace Cazon.Models
{
    public class ProductCategory
    {
        public int Id { get; set; }

        // The Order (Position Number) in which category will be display on the website.
        public int OrderNumber { set; get; }

        [Required(AllowEmptyStrings = false)]
        public string CategoryName { get; set; }

        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 5)]
        public string IconName { get; set; }

        public int? ParentCategoryId { get; set; }
        public virtual ProductCategory ParentCategory { get; set; }
    }
}
