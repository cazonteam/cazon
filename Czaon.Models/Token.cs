﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models
{
    public class CazonToken
    {
        public string Token { get; set; }
        public DateTime IssueTime { get; set; }
        public TimeSpan ExpiresIn { get; set; }

        [Key, ForeignKey("CazonUser"), Required(AllowEmptyStrings = false)]
        public string UserId { get; set; }
        public CazonUser CazonUser { get; set; }

        public bool? AllowRefresh { get; set; }
        public DateTimeOffset? ExpiresUtc { get; set; }
        public bool IsPersistent { get; set; }
        public DateTimeOffset? IssuedUtc { get; set; }
        public string RedirectUri { get; set; }
    }
}
