﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models.Products
{
    public class FeaturedProduct
    {
        public int Id { get; set; }

        public DateTime StartFrom { get; set; }

        public DateTime? EndOn { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
