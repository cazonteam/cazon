﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models.Products
{
    public class Product
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required]
        public double Price { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Description { get; set; }

        public int QuantityInStock { get; set; }

        public double? Discount { get; set; }

        public DateTime? DiscountStartFrom { get; set; }

        public DateTime? DiscountEndOn { get; set; }

        [Required]
        [ForeignKey("ProductCategory")]
        public int ProductCategoryId { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }

        [Required]
        [ForeignKey("CazonShop")]
        public int CazonShopId { get; set; }
        public virtual CazonShop CazonShop { get; set; }

        public virtual ICollection<ProductImage> Images { set; get; }
        public virtual ICollection<CazonShop> Shops { set; get; }
    }
}
