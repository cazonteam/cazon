﻿using Cazon.Models.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models
{
    public class CazonShop
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { set; get; }

        public string Description { get; set; }
        
        public DateTime RegisteredOn { set; get; }

        public string ShopPhoneNumber { get; set; }

        [Required(AllowEmptyStrings = false)]
        [ForeignKey("Owner")]
        public string OwnerId { get; set; }
        public virtual CazonUser Owner { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }
        public virtual Address Address { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}