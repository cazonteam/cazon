﻿using Cazon.Models.Products;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models
{
    public class ProductImage
    {
        public int Id { get; set; }

        public string ImgName { get; set; }

        public bool IsDefault { get; set; }

        [Required]
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
