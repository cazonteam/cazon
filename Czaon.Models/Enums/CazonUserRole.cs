﻿namespace Cazon.Models.Enums
{
    public enum CazonUserRole
    {
        Admin = 1,

        ShopKeeper,

        SalesMan,

        User
    }
}
