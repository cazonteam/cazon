﻿namespace Cazon.Models.DbContexts
{
    public abstract class CazonDbContextFactory
    {
        public static CazonDbContext GetDbContext(string nameOrConnectionString)
        {
            return new CazonDbContext(nameOrConnectionString);
        }
    }
}
