﻿using Cazon.Models.Products;
using Cazon.Models.Shopping.CazonCart;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Cazon.Models.DbContexts
{
    public class CazonDbContext : IdentityDbContext<CazonUser>
    {
        public CazonDbContext()
            : base("CazonDbConnection", throwIfV1Schema: false)
        {
        }

        public CazonDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString, throwIfV1Schema: false)
        {
        }

        public static CazonDbContext Create()
        {
            return new CazonDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProductCategory>()
                .HasOptional(e => e.ParentCategory)
                .WithMany()
                .HasForeignKey(m => m.ParentCategoryId);

            modelBuilder.Entity<Cart>()
                .Property(p => p.UserId)
                .IsOptional();

            modelBuilder.Entity<Cart>()
                .HasOptional(o => o.CartOwner)
                .WithOptionalPrincipal();
        }
        public DbSet<CazonToken> CazonToken { get; set; }

        public DbSet<CazonShop> CazonShop { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<FeaturedProduct> FeaturedProducts { get; set; }

        public DbSet<ProductImage> ProductImage { get; set; }

        public DbSet<ProductCategory> ProductCategory { get; set; }

        public DbSet<Address> Address { get; set; }

        public DbSet<City> City { get; set; }

        public DbSet<Cart> Cart { set; get; }

        public DbSet<CartItem> CartItem { get; set; }
    }
}
