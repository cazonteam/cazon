﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models
{
    public class Address
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Latitude { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Longitude { get; set; }

        [ForeignKey("City")]
        public int CityId { get; set; }
        public virtual City City { get; set; }

        public virtual ICollection<CazonShop> Shop { get; set; }
    }
}