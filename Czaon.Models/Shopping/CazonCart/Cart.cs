﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models.Shopping.CazonCart
{
    public class Cart
    {
        public Cart()
        {
            CreatedOn = DateTime.Now;
            CartItems = new List<CartItem>();
        }

        public int CartId { set; get; }

        public DateTime CreatedOn { protected set; get; }

        // The maximumLength of the SessionIs comes from the SessionIDManager.SessionIDMaxLength property.
        [Index(IsUnique = true)]
        [StringLength(maximumLength: 80, MinimumLength = 0)]
        public string SessionId { get; set; }

        [Index]
        [StringLength(maximumLength: 36, MinimumLength = 36)]
        [ForeignKey("CartOwner")]
        public string UserId { get; set; }
        public virtual CazonUser CartOwner { get; set; }

        public virtual ICollection<CartItem> CartItems { get; set; }
    }
}
