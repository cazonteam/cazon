﻿using Cazon.Models.Products;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cazon.Models.Shopping.CazonCart
{
    public class CartItem
    {
        public CartItem()
        {
            AddedOn = DateTime.Now;
        }

        public int CartItemId { get; set; }

        public int Quantity { get; set; }

        public DateTime AddedOn { get; protected set; }

        [Index(IsUnique = false)]
        [ForeignKey("Cart")]
        public int CartId { get; set; }
        public virtual Cart Cart { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
