﻿namespace Cazon.Models
{
    public class City
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public int PostalCode { get; set; }
    }
}